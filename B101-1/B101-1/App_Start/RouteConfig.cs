﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace B101_1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "Load",
               url: "",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ChangePass",
                url: "Admin/ChangePassword",
                defaults: new { controller = "Account", action = "ChangePassword", id = UrlParameter.Optional, subid = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "ForgotPass",
               url: "Admin/Forgot",
               defaults: new { controller = "Admin", action = "Forgot", id = UrlParameter.Optional }
           );
            routes.MapRoute(
              name: "userchangepassword",
              url: "UserChangePassword",
              defaults: new { controller = "Account", action = "UserChangePassword", id = UrlParameter.Optional }
          );
            routes.MapRoute(
                name: "Admin",
                url: "Admin/{controller}/{action}/{id}/{subid}",
                defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional, subid = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "Login",
               url: "Login",
               defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "Expire",
              url: "Expire",
              defaults: new { controller = "Account", action = "Expire", id = UrlParameter.Optional }
           );
            routes.MapRoute(
             name: "NewBuilder",
             url: "NewBuilder",
             defaults: new { controller = "Account", action = "NewBuilder", id = UrlParameter.Optional }
          );
            routes.MapRoute(
                name: "NewUser",
                url: "NewUser",
                defaults: new { controller = "Account", action = "NewUser", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Forgot",
                url: "Forgot",
                defaults: new { controller = "Account", action = "Forgot", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "signout",
               url: "SignOut",
               defaults: new { controller = "Account", action = "SignOut", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "Directery",
                url: "Directery/{*id}",
                defaults: new { controller = "Home", action = "Directery", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "Subscribe",
               url: "Newsletter/Subscribe",
               defaults: new { controller = "Newsletter", action = "Subscribe", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "View",
               url: "View/{action}/{id}",
               defaults: new { controller = "View", action = "Builder", id = UrlParameter.Optional }
           );
            routes.MapRoute(
              name: "Search",
              url: "Search/{action}/{id}",
              defaults: new { controller = "Search", action = "SearchHome", id = UrlParameter.Optional }
          );
            routes.MapRoute(
                name: "ActivateUser",
                url: "Account/ActivateUser",
                defaults: new { controller = "Account", action = "ActivateUser", id = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "ArticlebySub",
              url: "Articles/sub/{*id}",
              defaults: new { controller = "Home", action = "ArticlesBySub", id = UrlParameter.Optional }
          );
            routes.MapRoute(
               name: "Article",
               url: "Articles/{*id}",
               defaults: new { controller = "Home", action = "Articles", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
              name: "NewHomeDesign",
              url: "NewHomeDesign",
              defaults: new { controller = "Home", action = "NewHomeDesign", id = UrlParameter.Optional }
          );
            routes.MapRoute(
               name: "Content",
               url: "{*id}",
               defaults: new { controller = "Home", action = "Content", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}