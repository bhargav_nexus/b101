﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Mail;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;
using B101_1.Models;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using B101_1.App_Start;
using Facebook;

namespace B101_1.Controllers
{
    public class AccountController : Controller
    {
        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("ExternalLoginCallback");
                return uriBuilder.Uri;
            }
        }
        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();
        //
        // GET: /Account/
        BuilderRepo builderrepo = new BuilderRepo();
        public ActionResult Login()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            if (username == string.Empty || password == string.Empty)
            {
                MySession.Current.ErrorMessage= "all fields are required";
                return View();
            }
            try
            {
                if (Membership.ValidateUser(username, password))
                {
                    MembershipUser user = Membership.GetUser(username, true);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                      1,                                     // ticket version
                                     username,                        // authenticated username
                                      DateTime.Now,                          // issueDate
                                      DateTime.Now.AddMinutes(60),           // expiryDate
                                      false,                          // true to persist across browser sessions
                                      user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                      FormsAuthentication.FormsCookiePath);  // the path for the cookie
                    // Encrypt the ticket using the machine key
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    // Add the cookie to the request to save it
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    if (Roles.IsUserInRole(user.UserName, "administrator"))
                    {
                        FormsAuthentication.SignOut();
                        MySession.Current.ErrorMessage = "invalid username or password for login";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    MySession.Current.ErrorMessage = "invalid username or password";
                    return View();
                }
            }
            catch (Exception ex)
            {
                MySession.Current.ErrorMessage = ex.Message;
                return View();
            }
        }

        public ActionResult NewBuilder()
        {
            if (Request["hidden"] != null)
            {
                string hdn = Request["hidden"].ToString();
                List<DataBindModel> mdllist = new BuilderCatRepo().BindSubCatagery(Convert.ToInt64(Request["hidden"]));
                ViewBag.subcatagery = mdllist;
            }
            return View();
        }
        [HttpPost]
        public ActionResult NewBuilder(string UserName, string Password, builderModel mdl)
        {
            if(UserName == string.Empty)
            {
                MySession.Current.ErrorMessage = "Please Provide UserName";
                return View();
            }
            if (Password == string.Empty)
            {
                MySession.Current.ErrorMessage = "Please Provide Password";
                return View();
            }
            if (mdl.bm_email == string.Empty)
            {
                MySession.Current.ErrorMessage = "Please Provide Emailarddress";
                return View();
            }
            MembershipCreateStatus crtsts;
            Membership.CreateUser(UserName, Password, mdl.bm_email, "B101", "B101", false, out crtsts);
            if (crtsts == MembershipCreateStatus.Success)
            {
                MembershipUser mu = Membership.GetUser(UserName);
                Roles.AddUserToRole(mu.UserName.ToString(), "builder");
                mdl.bm_isapproved = false;
                mdl.bm_status = true;
                mdl.bm_userid = (Guid)mu.ProviderUserKey;
                mdl.bm_name = UserName;
                mdl.bm_isprofcompltd = false;

                Boolean blnSvRslt = builderrepo.SaveBuilder(mdl);
                if (blnSvRslt)
                {
                    BuildercatagerymappingRepo mappingrepo = new BuildercatagerymappingRepo();
                    buildercatmappingMaster catmdl = new buildercatmappingMaster();
                    catmdl.bcm_builderid = Convert.ToInt64(MySession.Current.AddEditId);
                    catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_catagery);
                    catmdl.bcm_isparent = true;
                    Boolean blncatSave = mappingrepo.Save(catmdl);
                    if (blncatSave)
                    {
                        if (mdl.bm_subcatagery != null)
                        {
                            catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_subcatagery);
                            catmdl.bcm_isparent = false;
                            blncatSave = mappingrepo.Save(catmdl);
                        }
                    }
                    if (!blncatSave)
                    {
                        MySession.Current.ErrorMessage = "Builder Saved SuccessFully But Unable to Save Catagery , Please Edit It ...!";
                    }

                    mail.mailto = mu.Email;
                    mail.template = tmpltrepo.gettemplatebyname("WELCOME");
                    mail.template.et_emailbody = mail.template.et_emailbody.Replace("{userName}", UserName);
                    mail.template.et_emailbody = mail.template.et_emailbody.Replace("{password}", Password);
                    mail.sendmail();
                    MySession.Current.MSGCODE = "REGBUILDSUCC";
                    return RedirectToAction("Message", "Home");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error in Insert Builder";
                    return View();
                }
            }
            else
            {
                MySession.Current.ErrorMessage = GetErrorMessage(crtsts);
                return View();
            }
        }
        public ActionResult Forgot()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Forgot(string email)
        {
            if (email != string.Empty)
            {
                if (IsEmail(email))
                {
                    string name = Membership.GetUserNameByEmail(email);
                    if (name == null)
                    {
                        MySession.Current.ErrorMessage = "Your email address is not available, please try again !!!";
                    }
                    else
                    {
                        string Password = Membership.GetUser(name).GetPassword();
                        mail.mailto = email;
                        mail.template = tmpltrepo.gettemplatebyname("FORGOTPASSWORD");
                        mail.template.et_emailbody = mail.template.et_emailbody.Replace("{password}", Password);
                        mail.sendmail();
                        MySession.Current.ErrorMessage = "Password has beent sent to you Email address please check your mail box!!!";
                    }
                    return View();
                }
                else
                {
                    MySession.Current.ErrorMessage= "Please enter valid email address !!!";
                    return View();
                }
            }
            else
            {
                MySession.Current.ErrorMessage = "Please enter email address !!!";
                return View();
            }
        }
        public ActionResult SignOut()
        {
            if (User.IsInRole("endusers"))
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                FormsAuthentication.SignOut();
                return Redirect("~/Admin");
            }
        }
        public ActionResult NewUser()
        {
            return View();
        }

        public ActionResult Expire()
        {
            string qappend=Request.QueryString[""].ToString();
            if (User.IsInRole("endusers"))
            {
                return Redirect("~/"+qappend);
            }
            else
            {
                return Redirect("~/Admin" + qappend);
            }
        }

        [HttpPost]
        public ActionResult NewUser(string UserName, string Password,string Repassword)
        {
            if(UserName == string.Empty)
            {
                MySession.Current.ErrorMessage = "Please Enter UserName";
                return View();
            }
            if(Password == string.Empty)
            {
                MySession.Current.ErrorMessage = "Please Enter Password";
                return View();
            }
            if (Repassword == string.Empty)
            {
                MySession.Current.ErrorMessage = "Please ReEnter Password";
                return View();
            }
            if(Password != Repassword)
            {
                MySession.Current.ErrorMessage = "Password do not match please insert same value";
                return View();
            }
            MembershipCreateStatus crtsts;
            Membership.CreateUser(UserName, Password, UserName, "B101", "B101",false, out crtsts);
            if (crtsts == MembershipCreateStatus.Success)
            {
                MembershipUser mu = Membership.GetUser(UserName);
                Roles.AddUserToRole(mu.UserName.ToString(), "endusers");
                string validationurl = Request.Url.GetLeftPart(UriPartial.Authority)+"/Account/ActivateUser?userid="+mu.ProviderUserKey;
                mail.mailto = mu.Email;
                mail.template = tmpltrepo.gettemplatebyname("WELCOME");
                mail.template.et_emailbody = mail.template.et_emailbody.Replace("{userName}", UserName);
                mail.template.et_emailbody = mail.template.et_emailbody.Replace("{password}", Password);
                mail.template.et_emailbody = mail.template.et_emailbody.Replace("{url}", validationurl);
                mail.sendmail();
                MySession.Current.MSGCODE = "REGSUCC";
                return RedirectToAction("Message", "Home");
            }
            else
            {
                MySession.Current.ErrorMessage = GetErrorMessage(crtsts);
                return View();
            }
        }


        [HttpPost]
        public JsonResult RegisterUser(string UserName, string Password, string Repassword)
        {
            string Message=string.Empty;
            if (UserName == string.Empty)
            {
                Message = "Please Enter UserName";
                return Json(Message);
            }
            if (Password == string.Empty)
            {
                Message = "Please Enter Password";
                return Json(Message);
            }
            if (Repassword == string.Empty)
            {
                Message = "Please ReEnter Password";
                return Json(Message);
            }
            if (Password != Repassword)
            {
                Message = "Password do not match please insert same value";
                return Json(Message);
            }
            MembershipCreateStatus crtsts;
            Membership.CreateUser(UserName, Password, UserName, "B101", "B101", false, out crtsts);
            if (crtsts == MembershipCreateStatus.Success)
            {
                MembershipUser mu = Membership.GetUser(UserName);
                Roles.AddUserToRole(mu.UserName.ToString(), "endusers");
                string validationurl = Request.Url.GetLeftPart(UriPartial.Authority) + "/Account/ActivateUser?userid=" + mu.ProviderUserKey;
                mail.mailto = mu.Email;
                mail.template = tmpltrepo.gettemplatebyname("WELCOME");
                mail.template.et_emailbody = mail.template.et_emailbody.Replace("{userName}", UserName);
                mail.template.et_emailbody = mail.template.et_emailbody.Replace("{password}", Password);
                mail.template.et_emailbody = mail.template.et_emailbody.Replace("{url}", validationurl);
                mail.sendmail();
                MySession.Current.MSGCODE = "REGSUCC";
                return Json(Message="Success");
            }
            else
            {
                Message = GetErrorMessage(crtsts);
                return Json(Message);
            }
        }
        public ActionResult ExternalLogin()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = "296673373866909",
                client_secret = "3b6bbbc97c26e42669f89af2cfb5a584",
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email" // Add other permissions as needed
            });

            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult ExternalLoginCallback(string code)
        {
            var fb = new FacebookClient(code);
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = "296673373866909",
                client_secret = "3b6bbbc97c26e42669f89af2cfb5a584",
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });
            string accessToken = result.access_token;
            if (result.access_token != null)
            {
                fb.AccessToken = accessToken;
                dynamic me = fb.Get("me?fields=first_name,last_name,id,email");
                string email = me.email;
                MembershipUser mu = Membership.GetUser(email);
                if (mu == null)
                {
                    ViewBag.fbemail = email;
                    return View();
                }
                else
                {
                    MembershipUser user = Membership.GetUser(email, true);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                          1,                                     // ticket version
                                         email ,                      // authenticated username
                                          DateTime.Now,                          // issueDate
                                          DateTime.Now.AddMinutes(60),           // expiryDate
                                          false,                          // true to persist across browser sessions
                                          user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                          FormsAuthentication.FormsCookiePath);  // the path for the cookie
                    // Encrypt the ticket using the machine key
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    // Add the cookie to the request to save it
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("Index", "Home");
                }
            }

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ExternalLoginCallback(string UserName, string Password, string Repassword)
        {
            try
            {
                if (UserName == string.Empty)
                {
                    MySession.Current.ErrorMessage = "Please Enter UserName";
                    return View();
                }
                if (Password == string.Empty)
                {
                    MySession.Current.ErrorMessage = "Please Enter Password";
                    return View();
                }
                if (Repassword == string.Empty)
                {
                    MySession.Current.ErrorMessage = "Please ReEnter Password";
                    return View();
                }
                if (Password != Repassword)
                {
                    MySession.Current.ErrorMessage = "Password do not match please insert same value";
                    return View();
                }
                MembershipCreateStatus crtsts;
                Membership.CreateUser(UserName, Password, UserName, "B101", "B101", true, out crtsts);
                if (crtsts == MembershipCreateStatus.Success)
                {
                    MembershipUser mu = Membership.GetUser(UserName);
                    Roles.AddUserToRole(mu.UserName.ToString(), "endusers");
                    MembershipUser user = Membership.GetUser(UserName, true);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                          1,                                     // ticket version
                                        UserName,                        // authenticated username
                                          DateTime.Now,                          // issueDate
                                          DateTime.Now.AddMinutes(60),           // expiryDate
                                          false,                          // true to persist across browser sessions
                                          user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                          FormsAuthentication.FormsCookiePath);  // the path for the cookie
                    // Encrypt the ticket using the machine key
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    // Add the cookie to the request to save it
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    MySession.Current.ErrorMessage = GetErrorMessage(crtsts);
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.fbemail = UserName;
                MySession.Current.ErrorMessage = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public ActionResult ActivateUser()
        {
            if (Request.QueryString["userid"] != null)
            {
                MembershipUser muser = Membership.GetUser(new Guid(Request["userid"].ToString()));
                if (muser != null)
                {
                    muser.IsApproved = true;
                    Membership.UpdateUser(muser);
                    MySession.Current.MSGCODE = "USERACTIVATED";
                }
                else
                {
                    MySession.Current.MSGCODE = "USERNOTEXIST";    
                }
            }
            else
            {
                MySession.Current.MSGCODE = "USERNOTEXIST";
            }
            return RedirectToAction("Message","Home");
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(string oldpass, string newpass, string newpassre)
        {
            MembershipUser muser = Membership.GetUser();
            string pass = muser.GetPassword();
            if (pass != oldpass)
            {
                MySession.Current.ErrorMessage = "please enter your correct current password";
            }
            else
            {
                muser.ChangePassword(pass, newpass);
                mail.mailto = muser.Email;
                mail.template = tmpltrepo.gettemplatebyname("PASSCHANGED");
                mail.sendmail();
                MySession.Current.ErrorMessage = "Password Changed Successfully";
            }
            return View();
        }

        public ActionResult UserChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserChangePassword(string oldpass, string newpass, string newpassre)
        {
            MembershipUser muser = Membership.GetUser();
            string pass = muser.GetPassword();
            if (pass != oldpass)
            {
                MySession.Current.ErrorMessage = "please enter your correct current password";
            }
            else if (newpass != newpassre)
            {
                MySession.Current.ErrorMessage = "retyped password should be matched with new password";
            }
            else
            {
                muser.ChangePassword(pass, newpass);
                mail.mailto = muser.Email;
                mail.template = tmpltrepo.gettemplatebyname("PASSCHANGED");
                mail.sendmail();
                MySession.Current.ErrorMessage = "Password Changed Successfully";
            }
            return View();
        }
        public string GetErrorMessage(MembershipCreateStatus status)
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #region Isemail
        public const string MatchEmailPattern =
                @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
         + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
         + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
         + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


        public static bool IsEmail(string email)
        {
            if (email != null)
                return Regex.IsMatch(email, MatchEmailPattern);
            else
                return false;
        }
        #endregion
    }
}
