﻿using B101_1.DataBaseRepo;
using B101_1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B101_1.Controllers
{
    public class SearchController : Controller
    {
        PrpHomeRepo prphmrepo = new PrpHomeRepo();
        ImageRepo imgrepo = new ImageRepo();
        FeatureRepo ftrrepo = new FeatureRepo();
        //
        // GET: /Search/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SearchHome(string bed, string bath, string storey, string features, string priceranage,string region)
        {
            string fmrprc = string.Empty;
            string toprc = string.Empty;

            if(priceranage != string.Empty && priceranage != null)
            {
                fmrprc = priceranage.Split('-')[0];
                toprc = priceranage.Split('-')[1];
            }

            Int64 pgn=1;
            Boolean ispaging = false;
            if (Request["pagenumber"] != null)
            {
                pgn = Convert.ToInt64(Request["pagenumber"]);
                if (Request["ispaging"] != null)
                {
                    ispaging = Convert.ToBoolean(Request["ispaging"]);
                }
            }
            String Fq = string.Empty;

            if(MySession.Current.searchresult.FilterString != string.Empty && ispaging == true)
            {
                Fq = MySession.Current.searchresult.FilterString;
            }
            if (bed != string.Empty && bed != null)
            {
                if (Fq == string.Empty)
                {
                    Fq = "hm_bed=" + bed;
                }
                else
                {
                    Fq = Fq + " and hm_bed=" + bed;
                }
            }
            if (bath != string.Empty && bath != null)
            {
                if (Fq == string.Empty)
                {
                    Fq = "hm_bath=" + bath;
                }
                else
                {
                    Fq = Fq + " and hm_bath=" + bath;
                }
            }
            if (storey != string.Empty && storey != null)
            {
                if (Fq == string.Empty)
                {
                    Fq = "hm_storey=" + storey;
                }
                else
                {
                    Fq = Fq + " and hm_storey=" + storey;
                }
            }
            if(features != string.Empty && features != null)
            {
                if (Fq == string.Empty)
                {
                    Fq = "hm_id in (select distinct(hfm_homeid) from tbl_homefeturesMapping where hfm_ftrid in ("+features+"))";
                }
                else
                {
                    Fq = Fq + " and hm_id in (select distinct(hfm_homeid) from tbl_homefeturesMapping where hfm_ftrid in (" + features + "))";
                }
            }
            if (fmrprc != string.Empty && toprc !=string.Empty)
            {
                if (Fq == string.Empty)
                {
                    Fq = "(hm_pricefrm between " + fmrprc + " and " + toprc + ") and (hm_priceto between " + fmrprc + " and " + toprc + ")";
                }
                else
                {
                    Fq = Fq + " and (hm_pricefrm between " + fmrprc + " and " + toprc + ") and (hm_priceto between " + fmrprc + " and " + toprc + ")";
                }
            }
            if (region != string.Empty && region != null)
            {
                if (Fq == string.Empty)
                {
                    Fq = "rm_region='" + region+"'";
                }
                else
                {
                    Fq = Fq + " and rm_region='" + region+"'";
                }
            }
            List<PrpHomeViewModel> homepropertylist = new List<PrpHomeViewModel>();
            List<PrpHomeViewModel> prphmlist = prphmrepo.SearchHomePaged(pgn,Fq);

            foreach (PrpHomeViewModel mdl in prphmlist)
            {
                mdl.galleryimages = imgrepo.GetHomeGalleryImages(mdl.hm_id);
                homepropertylist.Add(mdl);
            }
            return View(homepropertylist);
        }


        public ActionResult ViewHome(Int64 id)
        {
            PrpHomeViewModel homeprp = prphmrepo.SearchHomeByid(id);
            homeprp.galleryimages = imgrepo.GetHomeGalleryImages(id);
            return View(homeprp);
        }
    }
}
