﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;
using B101_1.ViewModels;

namespace B101_1.Controllers
{
    public class ViewController : Controller
    {
        //
        // GET: /View/
        ImageRepo imgrepo = new ImageRepo();
        BuilderRepo builderrepo = new BuilderRepo();
        BuildercatagerymappingRepo bcatrepo = new BuildercatagerymappingRepo();
        DisplayHomeVillageRepo dhvrepo = new DisplayHomeVillageRepo();
        BlogArticlesRepo artrepo = new BlogArticlesRepo();
        BlogcommentRepo blgcmtrepo = new BlogcommentRepo();
        BuilderGalleryRepo bldglrp = new BuilderGalleryRepo();
        public ActionResult Builder(Int64 id)
        {
            builderModel builder = builderrepo.GetBuilder(id);
            if (Convert.ToInt64(builder.bm_headerimage) != 0)
            {
                builder.bmheaderimage = imgrepo.ReadImageName(Convert.ToInt64(builder.bm_headerimage));
            }
            else
            {
                builder.bmheaderimage = string.Empty;
            }
            if (Convert.ToInt64(builder.bm_searchimage) != 0)
            {
                builder.bmsearchimage = imgrepo.ReadImageName(Convert.ToInt64(builder.bm_searchimage));
            }
            else
            {
                builder.bmsearchimage = string.Empty;
            }
            builder.bm_catagery = bcatrepo.Buildercatagery(id).ToString();

            BuilderViewModel buildvw = new BuilderViewModel();
            buildvw.buildermdl = builder;
            buildvw.galleryimages = imgrepo.GetBuilderGalleryImages(builder.bm_id);
            return View(buildvw);
        }

        public ActionResult DisplayHomeLocation(Int64 id)
        {
            displayhomevillageModel mdl = dhvrepo.GetDHVById(id);
            if (mdl.hvm_image != 0)
            {
                mdl.ImageName = imgrepo.ReadImageName(mdl.hvm_image);
            }
            else
            {
                mdl.ImageName = string.Empty;
            }
            return View(mdl);
        }

        public ActionResult LandProperty(Int64 id)
        {
            LandPropertyModel mdl = new LandPropertyRepo().GetLPById(id);
            if (mdl.lp_image != 0)
            {
                mdl.ImageName = imgrepo.ReadImageName(mdl.lp_image);
            }
            else
            {
                mdl.ImageName = string.Empty;
            }
            return View(mdl);
        }

        public ActionResult Blog(Int64 id)
        {
            BlogViewModel blogview = new BlogViewModel();
            blogview.article = artrepo.ReadArticle(id);
            blogview.comments = blgcmtrepo.ReadCommnetByBlog(id);
            foreach (string s in new BlogtagmappingRepo().SelectedTag(id))
            {
                blogview.selectedtags = blogview.selectedtags + "," + s;
            }
            blogview.selectedtags = blogview.selectedtags.TrimStart(',');
            return View(blogview);
        }
    }
}
