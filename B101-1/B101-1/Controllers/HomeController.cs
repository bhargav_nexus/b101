﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace B101_1.Controllers
{
    public class HomeController : Controller
    {
        PrpHomeRepo prphmrepo = new PrpHomeRepo();
        BlogArticlesRepo artrepo = new BlogArticlesRepo();
        BuilderRepo builderrepo = new BuilderRepo();
        ImageRepo imgrepo = new ImageRepo();
        BuildercatagerymappingRepo bcatrepo = new BuildercatagerymappingRepo();
        //
        // GET: /Home/
        public ActionResult Index()
        {
            setgeotarget();
            return View(); 
        }
        public ActionResult TermsandCondition()
        {
            return View();
        }
        public ActionResult Privacy()
        {
            return View();
        }
        public ActionResult Advertise()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult NewHomeDesign()
        {
            List<PrpHomeViewModel> prphmlist = prphmrepo.SearchHome(string.Empty);
            return View(prphmlist);
        }
        public ActionResult AdvanceSearch()
        {
            List<PrpHomeViewModel> prphmlist = prphmrepo.SearchHome(string.Empty);
            return View(prphmlist);
        }
        [HttpPost]
        public ActionResult ViewProp(Int64 id)
        {
            PrpHomeViewModel view = prphmrepo.ViewHome(id);
            return View(view);
        }
        public ActionResult Message()
        {
            return View();
        }
        public ActionResult Content(string id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                pagecontentModel page = new PageContentRepo().getpagebykey(id);
                if (page == null)
                {
                    return new HttpNotFoundResult();
                }
                else
                {
                    return View(page);
                }
            }
        }
        public ActionResult Directery(string id)
        {
            if (MySession.Current.geolocationinfo == new B101_1.Models.GeoInfoModel())
            {
                setgeotarget();
            }
            return View(id);
        }
        public ActionResult Articles(String id)
        {
            List<blogArticlesModel> articles= new BlogArticlesRepo().ReadArticlesByCatagory(id);
            return View(articles);
        }
        public ActionResult ArticlesBySub(String id)
        {
            List<blogArticlesModel> articles = new BlogArticlesRepo().ReadArticlesBySubCatagory(id);
            return View(articles);
        }
        public void setgeotarget()
        {
            //var request = (HttpWebRequest)WebRequest.Create("http://www.telize.com/geoip");
            //request.Method = "POST";
            //var response = (HttpWebResponse)request.GetResponse();
            //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            //GeoInfoModel gli = new JavaScriptSerializer().Deserialize<GeoInfoModel>(responseString.ToString());
            //MySession.Current.geolocationinfo = gli;
        }
    }
}
