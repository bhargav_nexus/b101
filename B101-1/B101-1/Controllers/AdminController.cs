﻿using B101_1.DataBaseRepo;
using B101_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace B101_1.Controllers
{
    public class AdminController : Controller
    {
        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string username, string password)
        {
            if (username == string.Empty || password == string.Empty)
            {
                MySession.Current.ErrorMessage = "all fields are required";
                return View();
            }
            try
            {
                if (Membership.ValidateUser(username, password))
                {
                    MembershipUser user = Membership.GetUser(username, true);
                    if(Roles.IsUserInRole(user.UserName,"endusers"))
                    {
                        MySession.Current.ErrorMessage = "Other User cannot login from here";
                        return View();
                    }
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                                      1,                                     // ticket version
                                     username,                        // authenticated username
                                      DateTime.Now,                          // issueDate
                                      DateTime.Now.AddMinutes(30),           // expiryDate
                                      false,                          // true to persist across browser sessions
                                      user.ProviderUserKey.ToString(),                              // can be used to store additional user data
                                      FormsAuthentication.FormsCookiePath);  // the path for the cookie
                    // Encrypt the ticket using the machine key
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    // Add the cookie to the request to save it
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("Index", "DashBoard");
                }
                else
                {
                    MySession.Current.ErrorMessage = "invalid username or password";
                    return View();
                }
            }
            catch (Exception ex)
            {
                MySession.Current.ErrorMessage = ex.Message;
                return View();
            }
        }

        public ActionResult Forgot()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Forgot(string email)
        {
            if (email != string.Empty)
            {
                if (IsEmail(email))
                {
                    string name = Membership.GetUserNameByEmail(email);
                    if (name == null)
                    {
                        MySession.Current.ErrorMessage = "Your email address is not available, please try again !!!";
                    }
                    else
                    {
                        string Password = Membership.GetUser(email).GetPassword();

                        mail.mailto = email;
                        mail.template = tmpltrepo.gettemplatebyname("FORGOTPASSWORD");
                        mail.template.et_emailbody = mail.template.et_emailbody.Replace("{password}", Password);
                        mail.sendmail();
                        MySession.Current.ErrorMessage = "Password has beent sent to you Email address please check your mail box!!!";
                    }
                    return View("Forgot");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Please enter valid email address !!!";
                    return View("Forgot");
                }
            }
            else
            {
                ViewData["message"] = "Please enter email address !!!";
                return View("Forgot");
            }
        }

        #region Isemail

        public const string MatchEmailPattern =
                @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
         + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
         + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
         + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


        public static bool IsEmail(string email)
        {
            if (email != null)
                return Regex.IsMatch(email, MatchEmailPattern);
            else
                return false;
        }
        #endregion
    }
}
