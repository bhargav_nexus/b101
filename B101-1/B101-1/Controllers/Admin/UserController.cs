﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using B101_1.ViewModels;
using System.Net.Mail;
using B101_1.DataBaseRepo;
using B101_1.Models;

namespace B101_1.Controllers.Admin
{
    public class UserController : Controller
    {

        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();
        //
        // GET: /User/

        [Authorize(Roles="administrator")]
        public ActionResult Index()
        {
            MembershipUserCollection usercollection = Membership.GetAllUsers();
            return View(usercollection);
        }

        [HttpPost]
        public JsonResult changeStatus(string id)
        {
            try
            {
                string[] usr = id.Split('%');
                MembershipUser user = Membership.GetUser(usr[0].ToString());
                if (Convert.ToInt16(usr[1].ToString()) == 0)//for deacticated user to activate
                {
                    user.IsApproved = true;
                    Membership.UpdateUser(user);
                }
                else                                                                //activated user to deactivate
                {
                    user.IsApproved = false;
                    Membership.UpdateUser(user);
                }
                return Json(1);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult ResetPassword(Guid id)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var stringChars = new char[8];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            string password = new String(stringChars);
            MembershipUser mu = Membership.GetUser(id);
            mu.ChangePassword(mu.GetPassword(), password);

            mail.mailto = mu.Email;
            mail.template = tmpltrepo.gettemplatebyname("RESETPASSWORD");
            mail.template.et_emailbody = mail.template.et_emailbody.Replace("{password}", password);
            mail.sendmail();
            MySession.Current.ErrorMessage = "Password Reseted Successfully ...!!";
            return RedirectToAction("Index","User");
        }
    }
}
