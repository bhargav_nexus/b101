﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;
using B101_1.ViewModels;
using System.Web.Security;
using System.Net.Mail;

namespace B101_1.Controllers.Admin
{
    public class BuilderController : Controller
    {
        BuilderRepo builderrepo = new BuilderRepo();
        imageMasterModel imgmster = new imageMasterModel();
        ImageRepo imgrepo = new ImageRepo();
        BuildercatagerymappingRepo bcatrepo = new BuildercatagerymappingRepo();
        EmailMessage mail = new EmailMessage();
        EmailTemplateRepo tmpltrepo = new EmailTemplateRepo();

        BuilderGalleryRepo glryrepo = new BuilderGalleryRepo();
        //
        // GET: /Builder/
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            List<builderModel> builders = builderrepo.ListBuilder();
            return View(builders);
        }
        [Authorize(Roles = "administrator")]
        public ActionResult NewRegistered()
        {
            List<builderModel> builders = builderrepo.ListNewRegisteredBuilder();
            return View(builders);
        }
        [Authorize(Roles = "administrator")]
        public ActionResult ApproveBuilder(Guid id)
        {
            MembershipUser buser = Membership.GetUser(id);
            if (buser != null)
            {
                buser.IsApproved = true;
                Membership.UpdateUser(buser);
                Boolean blnApRslt = builderrepo.ApproveBuilder(id);
                if (blnApRslt)
                {
                    return RedirectToAction("NewRegistered", "Builder");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error : " + builderrepo.GetSetError;
                    return RedirectToAction("NewRegistered", "Builder");
                }
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : NO Such User Found ..!!!!";
                return RedirectToAction("NewRegistered", "Builder");
            }
        }
        [Authorize(Roles = "administrator")]
        [HttpPost]
        public JsonResult subcatagery(Int64 id)
        {
            List<DataBindModel> mdllist = new BuilderCatRepo().BindSubCatagery(id);
            return Json(mdllist);
        }
        [Authorize(Roles = "administrator")]
        public ActionResult Add()
        {
            if (Request["hidden"] != null)
            {
                List<DataBindModel> mdllist = new BuilderCatRepo().BindSubCatagery(Convert.ToInt64(Request["hidden"]));
                ViewBag.subcatagery = mdllist;
            }
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(builderModel mdl)
        {
            MembershipCreateStatus crtsts;
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var stringChars = new char[8];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            string password = new String(stringChars);
            Membership.CreateUser(mdl.bm_name, password, mdl.bm_email, "B101", "B101", true, out crtsts);
            if (crtsts == MembershipCreateStatus.Success)
            {
                MembershipUser usr = Membership.GetUser(mdl.bm_name);
                if (mdl.headerimg != null)
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.headerimg.FileName;
                    mdl.headerimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    imgrepo.SaveImage(imgmster);
                    mdl.bm_headerimage = Convert.ToInt64(imgrepo.GetSetError);
                }
                if (mdl.searchimg != null)
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.searchimg.FileName;
                    mdl.searchimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    imgrepo.SaveImage(imgmster);
                    mdl.bm_searchimage = Convert.ToInt64(imgrepo.GetSetError);
                }
                mdl.bm_isapproved = true;
                mdl.bm_isprofcompltd = true;
                mdl.bm_userid = (Guid)usr.ProviderUserKey;
                Roles.AddUserToRole(usr.UserName, "builder");
                Boolean blnSvRslt = builderrepo.SaveBuilder(mdl);
                if (blnSvRslt)
                {
                    BuildercatagerymappingRepo mappingrepo = new BuildercatagerymappingRepo();
                    buildercatmappingMaster catmdl = new buildercatmappingMaster();
                    catmdl.bcm_builderid = Convert.ToInt64(MySession.Current.AddEditId);
                    catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_catagery);
                    catmdl.bcm_isparent = true;
                    Boolean blncatSave = mappingrepo.Save(catmdl);
                    if (blncatSave)
                    {
                        if (mdl.bm_subcatagery != null)
                        {
                            catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_subcatagery);
                            catmdl.bcm_isparent = false;
                            blncatSave = mappingrepo.Save(catmdl);
                        }
                    }
                    if (!blncatSave)
                    {
                        MySession.Current.ErrorMessage = "Builder Saved SuccessFully But Unable to Save Catagery , Please Edit It ...!";
                    }
                    mail.mailto = mdl.bm_email;
                    mail.template = tmpltrepo.gettemplatebyname("NEWBUILDER");
                    mail.template.et_emailbody = mail.template.et_emailbody + password;
                    mail.sendmail();
                    return RedirectToAction("index");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error in Insert Builder";
                    return View();
                }
            }
            else
            {
                MySession.Current.ErrorMessage = GetErrorMessage(crtsts);
                return View();
            }
        }
        [Authorize(Roles = "administrator")]
        public ActionResult Edit(Int64 id)
        {
            builderModel builder = builderrepo.GetBuilder(id);

            if (Convert.ToInt64(builder.bm_headerimage) != 0)
            {
                builder.bmheaderimage = imgrepo.ReadImageName(Convert.ToInt64(builder.bm_headerimage));
            }
            else
            {
                builder.bmheaderimage = string.Empty;
            }
            if (Convert.ToInt64(builder.bm_searchimage) != 0)
            {
                builder.bmsearchimage = imgrepo.ReadImageName(Convert.ToInt64(builder.bm_searchimage));
            }
            else
            {
                builder.bmsearchimage = string.Empty;
            }
            if (Request["hidden"] != null)
            {
                builder.bm_catagery = Request["hidden"];
            }
            else
            {
                builder.bm_catagery = bcatrepo.GetBuildercatagery(id).ToString();
                builder.bm_subcatagery = bcatrepo.GetBuildersubcatagery(id).ToString();
            }
            List<DataBindModel> mdllist = new BuilderCatRepo().BindSubCatagery(Convert.ToInt64(builder.bm_catagery));
            ViewBag.subcatagery = mdllist;
            return View(builder);
        }
        [Authorize(Roles = "administrator")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(builderModel mdl)
        {
            if (mdl.headerimg != null)
            {
                if (mdl.bmheaderimage == string.Empty || mdl.bmheaderimage == null)
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.headerimg.FileName;
                    mdl.headerimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean blnSvRslt = imgrepo.SaveImage(imgmster);
                    if (blnSvRslt)
                    {
                        mdl.bm_headerimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
                else
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.headerimg.FileName;
                    mdl.headerimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean EditRslt = imgrepo.UpdateImage(mdl.bmheaderimage, imgmster);
                    if (EditRslt)
                    {
                        mdl.bm_headerimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
            }
            if (mdl.searchimg != null)
            {
                if (mdl.bmsearchimage == string.Empty || mdl.bmsearchimage == null)
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.searchimg.FileName;
                    mdl.searchimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean blnSvRslt = imgrepo.SaveImage(imgmster);
                    if (blnSvRslt)
                    {
                        mdl.bm_searchimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
                else
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.searchimg.FileName;
                    mdl.searchimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean EditRslt = imgrepo.UpdateImage(mdl.bmsearchimage, imgmster);
                    if (EditRslt)
                    {
                        mdl.bm_searchimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
            }
            mdl.bm_isapproved = true;
            mdl.bm_isprofcompltd = true;
            Boolean blnEditRslt = builderrepo.EditBuilder(mdl);
            if (blnEditRslt)
            {
                BuildercatagerymappingRepo mappingrepo = new BuildercatagerymappingRepo();
                buildercatmappingMaster catmdl = new buildercatmappingMaster();
                catmdl.bcm_builderid = Convert.ToInt64(MySession.Current.AddEditId);
                catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_catagery);
                catmdl.bcm_isparent = true;
                Boolean blncatSave = bcatrepo.DeleteMaping(catmdl.bcm_builderid);
                blncatSave = bcatrepo.Save(catmdl);
                if (blncatSave)
                {
                    if (mdl.bm_subcatagery != null)
                    {
                        catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_subcatagery);
                        catmdl.bcm_isparent = false;
                        blncatSave = mappingrepo.Save(catmdl);
                    }
                }
                if (!blncatSave)
                {
                    MySession.Current.ErrorMessage = "Builder Updated SuccessFully But Unable to Save Catagery , Please Edit It ...!";
                }

                return RedirectToAction("index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error in Update Builder";
                return View();
            }
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean blnrslt = builderrepo.DeleteBuilder(id);
            if (blnrslt)
            {
                blnrslt = imgrepo.DeleteImage(Convert.ToInt64(builderrepo.GetSetError));
                if (blnrslt)
                {
                    MySession.Current.ErrorMessage = "Record Deleted Successfully";
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error : Deleting Image Record";
                }
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : Deleting Builder Record";
            }
            return RedirectToAction("Index");
        }

        // gallery managment

        public ActionResult Gallery(Int64 id)
        {
            List<builderGalleryModel> imagelist = glryrepo.ListGalleryByBuilder(id);
            ViewBag.itmid = id;
            return View(imagelist);
        }

        public ActionResult GalleryAdd(Int64 id)
        {
            ViewBag.itmid = id;
            ViewBag.DispOrd = glryrepo.getdisplord(id);
            return View();
        }
        [HttpPost]
        public ActionResult GalleryAdd(ImageViewModel itmimage)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            imgmdl.img_altText = itmimage.AltText;
            imgmdl.img_name = itmimage.Image.FileName;
            imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
            imgmdl.img_name = itmimage.Image.FileName;
            itmimage.Image.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmdl.img_savedname));
            bool blnSvImag = imgrepo.SaveImage(imgmdl);
            if (blnSvImag == true)
            {
                builderGalleryModel bgmdl = new builderGalleryModel();
                bgmdl.bg_displayorder = itmimage.displayOrder;
                bgmdl.bg_buildid = itmimage.itmid;
                bgmdl.bg_imageid = Convert.ToInt64(imgrepo.GetSetError);
                Boolean blnSvGlry = glryrepo.SaveBuilderImage(bgmdl);
                if (blnSvGlry)
                {
                    return RedirectToAction("Gallery", new { id = itmimage.itmid });
                }
                else
                {
                    MySession.Current.ErrorMessage = glryrepo.GetSetError;
                    return RedirectToAction("GalleryAdd", new { id = itmimage.itmid });
                }
            }
            else
            {
                MySession.Current.ErrorMessage = imgrepo.GetSetError;
                return RedirectToAction("GalleryAdd", new { id = itmimage.itmid });
            }
        }

        public ActionResult Gallerydelete(Int64 id, Int64 subid)
        {
            bool blnRsltDlt = glryrepo.DeleteBuilderImage(subid);
            if (blnRsltDlt)
            {
                bool blnimgDltRslt = imgrepo.DeleteImage(subid);
                if (blnimgDltRslt)
                {
                    return RedirectToAction("Gallery", new { id = id });
                }
                else
                {
                    MySession.Current.ErrorMessage = glryrepo.GetSetError;
                    return RedirectToAction("Gallery", new { id = id });
                }
            }
            else
            {
                MySession.Current.ErrorMessage = glryrepo.GetSetError;
                return RedirectToAction("Gallery", new { id = id });
            }
        }
        public JsonResult ArreangeImages(String order, String dir)
        {
            string[] o = order.Split('#');

            Int32 ndo = 0;
            Int32 cdo = Convert.ToInt32(o[0]);
            String Query = string.Empty;
            if (dir == "U")
            {
                ndo = cdo - 1;
            }
            else if (dir == "D")
            {
                ndo = cdo + 1;
            }
            Boolean blnRs = glryrepo.Arrange(cdo, ndo, Convert.ToInt64(o[1]), Convert.ToInt64(o[2]));
            return Json(o[2]);
        }

        public ActionResult MyProfile()
        {
            builderModel builder = builderrepo.GetBuilderByUserid((Guid)Membership.GetUser().ProviderUserKey);

            if (Convert.ToInt64(builder.bm_headerimage) != 0)
            {
                builder.bmheaderimage = imgrepo.ReadImageName(Convert.ToInt64(builder.bm_headerimage));
            }
            else
            {
                builder.bmheaderimage = string.Empty;
            }
            if (Convert.ToInt64(builder.bm_searchimage) != 0)
            {
                builder.bmsearchimage = imgrepo.ReadImageName(Convert.ToInt64(builder.bm_searchimage));
            }
            else
            {
                builder.bmsearchimage = string.Empty;
            }
            if (Request["hidden"] != null)
            {
                builder.bm_catagery = Request["hidden"];
            }
            else
            {
                builder.bm_catagery = bcatrepo.GetBuildercatagery(builder.bm_id).ToString();
                builder.bm_subcatagery = bcatrepo.GetBuildersubcatagery(builder.bm_id).ToString();
            }
            List<DataBindModel> mdllist = new BuilderCatRepo().BindSubCatagery(Convert.ToInt64(builder.bm_catagery));
            ViewBag.subcatagery = mdllist;
            return View(builder);
        }

        [Authorize(Roles="builder")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult MyProfile(builderModel mdl)
        {
            if (mdl.headerimg != null)
            {
                if (mdl.bmheaderimage == string.Empty || mdl.bmheaderimage == null)
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.headerimg.FileName;
                    mdl.headerimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean blnSvRslt = imgrepo.SaveImage(imgmster);
                    if (blnSvRslt)
                    {
                        mdl.bm_headerimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
                else
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.headerimg.FileName;
                    mdl.headerimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean EditRslt = imgrepo.UpdateImage(mdl.bmheaderimage, imgmster);
                    if (EditRslt)
                    {
                        mdl.bm_headerimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
            }
            if (mdl.searchimg != null)
            {
                if (mdl.bmsearchimage == string.Empty || mdl.bmsearchimage == null)
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.searchimg.FileName;
                    mdl.searchimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean blnSvRslt = imgrepo.SaveImage(imgmster);
                    if (blnSvRslt)
                    {
                        mdl.bm_searchimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
                else
                {
                    imgmster.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmster.img_name = mdl.searchimg.FileName;
                    mdl.searchimg.SaveAs(HttpContext.Server.MapPath("~/Content/images/builder/" + imgmster.img_savedname));
                    imgmster.img_altText = string.Empty;
                    Boolean EditRslt = imgrepo.UpdateImage(mdl.bmsearchimage, imgmster);
                    if (EditRslt)
                    {
                        mdl.bm_searchimage = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
            }
            mdl.bm_isapproved = true;
            mdl.bm_isprofcompltd = true;
            Boolean blnEditRslt = builderrepo.EditBuilder(mdl);
            if (blnEditRslt)
            {
                BuildercatagerymappingRepo mappingrepo = new BuildercatagerymappingRepo();
                buildercatmappingMaster catmdl = new buildercatmappingMaster();
                catmdl.bcm_builderid = Convert.ToInt64(MySession.Current.AddEditId);
                catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_catagery);
                catmdl.bcm_isparent = true;
                Boolean blncatSave = bcatrepo.DeleteMaping(catmdl.bcm_builderid);
                blncatSave = bcatrepo.Save(catmdl);
                if (blncatSave)
                {
                    if (mdl.bm_subcatagery != null)
                    {
                        catmdl.bcm_catageryid = Convert.ToInt64(mdl.bm_subcatagery);
                        catmdl.bcm_isparent = false;
                        blncatSave = mappingrepo.Save(catmdl);
                    }
                }
                if (!blncatSave)
                {
                    MySession.Current.ErrorMessage = "Builder Updated SuccessFully But Unable to Save Catagery , Please Edit It ...!";
                }
                else
                {
                    MySession.Current.ErrorMessage = "Builder Profile Updated SuccessFully .. !";
                }
                return RedirectToAction("MyProfile");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error in Update Builder";
                return View();
            }
        }

        public string GetErrorMessage(MembershipCreateStatus status)
        {
            switch (status)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }
}
