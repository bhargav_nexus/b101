﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator")]
    public class LandPropertyController : Controller
    {
        imageMasterModel imgmdl = new imageMasterModel();
        ImageRepo imgrepo = new ImageRepo();
        LandPropertyRepo lprepo = new LandPropertyRepo();

        public ActionResult Index()
        {
            List<LandPropertyModel> landprptlist = lprepo.ListLP();
            return View(landprptlist);
        }
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(LandPropertyModel mdl, HttpPostedFileBase file)
        {
            if (file != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = file.FileName;
                file.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                Boolean blnSvRslt = imgrepo.SaveImage(imgmdl);
                if (blnSvRslt)
                {
                    mdl.lp_image = Convert.ToInt64(imgrepo.GetSetError);
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error In Save Image";
                    return View();
                }
            }
            mdl.lp_latlang = mdl.lp_latlang.TrimStart('(');
            mdl.lp_latlang = mdl.lp_latlang.TrimEnd(')');
            Boolean blnS = lprepo.SaveLP(mdl);
            if (blnS)
            {
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error In Save Property Info ...!!";
                return View();
            }
        }
        public ActionResult Edit(Int64 id)
        {
            LandPropertyModel mdl = lprepo.GetLPById(id);
            if (mdl.lp_image != 0)
            {
                mdl.ImageName = imgrepo.ReadImageName(mdl.lp_image);
            }
            else
            {
                mdl.ImageName = string.Empty;
            }
            return View(mdl);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(LandPropertyModel mdl, HttpPostedFileBase file)
        {
            if (file != null)
            {
                if (mdl.ImageName == null)
                {
                    imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmdl.img_name = file.FileName;
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                    imgmdl.img_altText = string.Empty;
                    Boolean blnSvRslt = imgrepo.SaveImage(imgmdl);
                    if (blnSvRslt)
                    {
                        mdl.lp_image = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
                else
                {
                    imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmdl.img_name = file.FileName;
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                    imgmdl.img_altText = string.Empty;
                    Boolean EditRslt = imgrepo.UpdateImage(mdl.ImageName, imgmdl);
                    if (EditRslt)
                    {
                        mdl.lp_image = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
            }
            mdl.lp_latlang = mdl.lp_latlang.TrimStart('(');
            mdl.lp_latlang = mdl.lp_latlang.TrimEnd(')');
            Boolean blnUpdt = lprepo.UpdateLP(mdl);
            if (blnUpdt)
            {
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error In Update Land Property...!!";
                return View();
            }
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean blnDltRslt = lprepo.DeleteLP(id);
            if (blnDltRslt)
            {
                imgrepo.DeleteImage(Convert.ToInt64(lprepo.GetSetError));
                MySession.Current.ErrorMessage = "Record Deleted Successfully";
                return RedirectToAction("index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error in Delete Land Property..!!";
                return View();
            }
        }

    }
}
