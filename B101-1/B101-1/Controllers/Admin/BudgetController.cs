﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles="administrator")]
    public class BudgetController : Controller
    {
        BudgetRepo bugrepo = new BudgetRepo();
        //
        // GET: /Budget/

        public ActionResult Index()
        {
            List<budgetModel> budgetlist = bugrepo.ListBudget();
            return View(budgetlist);
        }

        [HttpPost]
        public ActionResult Save(budgetModel bmdl)
        {
            bmdl.bm_status = true;
            Boolean blnSvRslt = bugrepo.SaveBudget(bmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Budget Saved Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + bugrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = bugrepo.DeleteBudget(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Budget Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + bugrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
