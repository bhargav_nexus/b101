﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;
namespace B101_1.Controllers.Admin
{
    public class AdditionalContentController : Controller
    {
        AdditionalContentRepo ac = new AdditionalContentRepo();
        //
        // GET: /AdditionalContent/

        public ActionResult Index()
        {
            List<additionalContent> cntList = ac.ReadAll();
            return View(cntList);
        }
        [HttpGet]
        public ActionResult AddEditContent(int id)
        {
            additionalContent acPrp = ac.ReadById(id);
            return View(acPrp);

        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditContent(additionalContent acPrp)
        {
            Boolean blnRslt = false;
            if (acPrp.CntId < 0)
                blnRslt = ac.AddContent(acPrp);
            else
                blnRslt = ac.UpdateContent(acPrp);

            if (blnRslt)
                return RedirectToAction("Index", "AdditionalContent");
            else
                return View(acPrp);

        }

    }
}
