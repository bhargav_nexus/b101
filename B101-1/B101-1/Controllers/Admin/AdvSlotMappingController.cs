﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator")]
    public class AdvSlotMappingController : Controller
    {
        AdvSlotMappingRepo advsltmaprepo = new AdvSlotMappingRepo();
        //
        // GET: /AdvSlotMapping/

        public ActionResult Index()
        {
            List<AdvSlotMappingViewModel> mappinglist = advsltmaprepo.AdvSlotMapping();
            return View(mappinglist);
        }
        [HttpPost]
        public ActionResult Save(advsltmappingModel mdl)
        {
            if (mdl.asm_advit == 0)
            {
                MySession.Current.ErrorMessage = "Please Select Advertisement";
                return RedirectToAction("Index");
            }
            if (mdl.asm_slotid == 0)
            {
                MySession.Current.ErrorMessage = "Please Select Slot";
                return RedirectToAction("Index");
            }
            Boolean BlnRslt = advsltmaprepo.SaveMapping(mdl);
            if (BlnRslt)
            {
                MySession.Current.ErrorMessage = "Mapping Saved Sucessfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : " + advsltmaprepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean BlnRslt = advsltmaprepo.DeleteMapping(id);
            if (BlnRslt)
            {
                MySession.Current.ErrorMessage = "Mapping Deleted Sucessfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : " + advsltmaprepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
