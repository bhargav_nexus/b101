﻿using B101_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    public class BlogSubCatageryController : Controller
    {
        BlogsubcatRepo subcatrepo = new BlogsubcatRepo();
        //
        // GET: /BlogSubCatagery/

        public ActionResult Index()
        {
            List<blogsubcatageryModel> subcatlist = subcatrepo.ReadCatagery();
            return View(subcatlist);
        }

        [HttpPost]
        public ActionResult Add(blogsubcatageryModel catmdl)
        {
            Boolean blnSvRslt = subcatrepo.SaveCatagery(catmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Catagery Added Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error:" + subcatrepo.GetSetError;
                return View();
            }
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnReslt = subcatrepo.DeleteByCatagery(id);
            if (blnReslt)
            {
                MySession.Current.ErrorMessage = "Catagory deleted successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :"+subcatrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
