﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator")]
    public class TagController : Controller
    {
        //
        // GET: /Tag/

        TagRepo tagrepo = new TagRepo();
        //
        // GET: /Budget/

        public ActionResult Index()
        {
            List<tagModel> budgetlist = tagrepo.ListTag();
            return View(budgetlist);
        }

        [HttpPost]
        public ActionResult Save(tagModel bmdl)
        {
            Boolean blnSvRslt = tagrepo.SaveTag(bmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Tag Saved Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + tagrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = tagrepo.DeleteTag(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Tag Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + tagrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
