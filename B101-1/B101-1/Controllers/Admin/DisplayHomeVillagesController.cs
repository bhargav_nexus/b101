﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator")]
    public class DisplayHomeVillagesController : Controller
    {
        imageMasterModel imgmdl = new imageMasterModel();
        ImageRepo imgrepo = new ImageRepo();
        DisplayHomeVillageRepo dhvrepo = new DisplayHomeVillageRepo();
        //
        // GET: /DisplayHomeVillages/

        public ActionResult Index()
        {
            List<displayhomevillageModel> villagelist = dhvrepo.ListDHV();
            return View(villagelist);
        }
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(displayhomevillageModel mdl, HttpPostedFileBase file)
        {
            if (file != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = file.FileName;
                file.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                Boolean blnSvRslt = imgrepo.SaveImage(imgmdl);
                if (blnSvRslt)
                {
                    mdl.hvm_image = Convert.ToInt64(imgrepo.GetSetError);
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error In Save Image";
                    return View();
                }
            }
            mdl.hvm_latlang = mdl.hvm_latlang.TrimStart('(');
            mdl.hvm_latlang = mdl.hvm_latlang.TrimEnd(')');
            Boolean blnS = dhvrepo.SaveDHV(mdl);
            if (blnS)
            {
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error In Save Village Info ...!!";
                return View();
            }
        }
        public ActionResult Edit(Int64 id)
        {
            displayhomevillageModel mdl = dhvrepo.GetDHVById(id);
            if (mdl.hvm_image != 0)
            {
                mdl.ImageName = imgrepo.ReadImageName(mdl.hvm_image);
            }
            else
            {
                mdl.ImageName = string.Empty;
            }
            return View(mdl);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(displayhomevillageModel mdl, HttpPostedFileBase file)
        {
            if (file != null)
            {
                if (mdl.ImageName == null)
                {
                    imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmdl.img_name = file.FileName;
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                    imgmdl.img_altText = string.Empty;
                    Boolean blnSvRslt = imgrepo.SaveImage(imgmdl);
                    if (blnSvRslt)
                    {
                        mdl.hvm_image = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
                else
                {
                    imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmdl.img_name = file.FileName;
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                    imgmdl.img_altText = string.Empty;
                    Boolean EditRslt = imgrepo.UpdateImage(mdl.ImageName, imgmdl);
                    if (EditRslt)
                    {
                        mdl.hvm_image = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    else
                    {
                        MySession.Current.ErrorMessage = "Error In Save Image";
                        return View();
                    }
                }
            }
            mdl.hvm_latlang = mdl.hvm_latlang.TrimStart('(');
            mdl.hvm_latlang = mdl.hvm_latlang.TrimEnd(')');
            Boolean blnUpdt = dhvrepo.UpdateDHV(mdl);
            if (blnUpdt)
            {
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error In Update Village Info ...!!";
                return View();
            }
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean blnDltRslt = dhvrepo.DeleteDHV(id);
            if (blnDltRslt)
            {
                imgrepo.DeleteImage(Convert.ToInt64(dhvrepo.GetSetError));
                MySession.Current.ErrorMessage = "Record Deleted Successfully";
                return RedirectToAction("index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error in Delete Display Home Village ..!!";
                return View();
            }
        }
    }
}
