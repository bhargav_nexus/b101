﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator")]
    public class FeatureController : Controller
    {
        FeatureRepo fetrepo = new FeatureRepo();
        //
        // GET: /Budget/

        public ActionResult Index()
        {
            List<featureModel> list = fetrepo.ListFeature();
            return View(list);
        }

        [HttpPost]
        public ActionResult Save(featureModel fmdl)
        {
            fmdl.fm_status = true;
            Boolean blnSvRslt = fetrepo.SaveFeature(fmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Feature Saved Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + fetrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = fetrepo.DeleteFeature(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Feature Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + fetrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
