﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    public class AdvertisementController : Controller
    {
        ImageRepo imgrepo = new ImageRepo();
        AdvertisementRepo advrepo = new AdvertisementRepo();
        //
        // GET: /Advertisement/
        public ActionResult Index()
        {
            List<advertisementModel> advlist = advrepo.ReadallAd();
            return View(advlist);
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(advertisementModel mdl)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            if (mdl.advimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = mdl.advimage.FileName;
                mdl.advimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/advertisement/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                imgrepo.SaveImage(imgmdl);
                mdl.adv_image = Convert.ToInt64(imgrepo.GetSetError);
            }
            Boolean BlnSvresult = advrepo.SaveAd(mdl);
            if (BlnSvresult)
            {
                MySession.Current.ErrorMessage = "Advertisement Saved Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : "+advrepo.GetSetError;
                return View();
            }
        }

        public ActionResult Edit(Int64 id)
        {
            advertisementModel advmdl = advrepo.ReadaAdbyId(id);
            return View(advmdl);
        }
        [HttpPost]
        public ActionResult Edit(advertisementModel mdl)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            if (mdl.advimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = mdl.advimage.FileName;
                mdl.advimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/advertisement/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                if (mdl.advimagenm != null)
                {
                    imgrepo.UpdateImage(mdl.advimagenm, imgmdl);
                }
                else
                {
                    imgrepo.SaveImage(imgmdl);
                }
                mdl.adv_image = Convert.ToInt64(imgrepo.GetSetError);
            }

            Boolean BlnUdresult = advrepo.EditAd(mdl);
            if (BlnUdresult)
            {
                MySession.Current.ErrorMessage = "Advertisement Updated Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : " + advrepo.GetSetError;
                return RedirectToAction("Index");
            }
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean dltAd = advrepo.DeleteAd(id);
            if (dltAd)
            {
                dltAd = imgrepo.DeleteImage(Convert.ToInt64(advrepo.GetSetError));
                if (dltAd)
                {
                    MySession.Current.ErrorMessage = "Advertisement Deleted Successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Advertisement Deleted Successfully but unable to delete Image";
                    return RedirectToAction("Index");
                }
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :"+advrepo.GetSetError;
                return RedirectToAction("Index");
            }
        }
    }
}
