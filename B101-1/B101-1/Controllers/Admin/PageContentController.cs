﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers
{
    [Authorize(Roles = "administrator")]
    public class PageContentController : Controller
    {
        PageContentRepo pgcntntrepo = new PageContentRepo();
        //
        // GET: /PageContent/
        public ActionResult Index()
        {
            List<pagecontentModel> pages = pgcntntrepo.listpages();
            return View(pages);
        }
        public ActionResult Add()
        {
            return View();
        }
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(pagecontentModel pgmdl)
        {
            if(pgmdl.cp_pagecontent != string.Empty)
            {
                MySession.Current.ErrorMessage = "Page Content Required";
                return View();
            }
            pgmdl.cp_url = "/" + pgmdl.cp_key;
            Boolean blnRslt = pgcntntrepo.SavePage(pgmdl);
            if (blnRslt)
            {
                MySession.Current.ErrorMessage = "Page Saved Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + pgcntntrepo.GetSetError;
                return View();
            }
        }

        public ActionResult Edit(String id)
        {
            pagecontentModel page = pgcntntrepo.getpagebykey(id);
            return View(page);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(pagecontentModel pgmdl)
        {
            Boolean blnRslt = pgcntntrepo.EditPage(pgmdl);
            if (blnRslt)
            {
                MySession.Current.ErrorMessage = "Page Edited Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + pgcntntrepo.GetSetError;
                return View();
            }
        }
    }
}
