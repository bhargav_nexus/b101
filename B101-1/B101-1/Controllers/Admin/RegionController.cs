﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles="administrator")]
    public class RegionController : Controller
    {
        RegionRepo rgnrepo = new RegionRepo();
        //
        // GET: /Region/
        public ActionResult Index()
        {
            List<regionModel> regionlist = rgnrepo.ListRegion();
            return View(regionlist);
        }

        [HttpPost]
        public ActionResult Save(regionModel mdl)
        {
            mdl.rm_status = true;
            Boolean blnSvRslt = rgnrepo.SaveRegion(mdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Region Saved Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + rgnrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = rgnrepo.DeleteRegion(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Region Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + rgnrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
