﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    public class BlogCatageryController : Controller
    {
        BlogcatageryRepo blgcatrepo = new BlogcatageryRepo();
        ImageRepo imgrepo = new ImageRepo();
        //
        // GET: /BlogCatagery/

        public ActionResult Index()
        {
            List<blogcatageryModel> catlist = blgcatrepo.ReadCatagery();
            return View(catlist);
        }
        [HttpPost]
        public ActionResult Add(blogcatageryModel catmdl)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            if (catmdl.mainimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = catmdl.mainimage.FileName;
                catmdl.mainimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/other/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                imgrepo.SaveImage(imgmdl);
                catmdl.bc_mainimage = Convert.ToInt64(imgrepo.GetSetError);
            }
            catmdl.bc_isstatic = false;
            Boolean blnSvRslt = blgcatrepo.SaveCatagery(catmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Catagery Added Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error:" + blgcatrepo.GetSetError;
                return View();
            }
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = blgcatrepo.DeleteByCatagery(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Catagery Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + blgcatrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
