﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.DataBaseRepo;
using B101_1.Models;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles="administrator")]
    public class LocationController : Controller
    {
        LocationRepo lcnrepo = new LocationRepo();
        //
        // GET: /Location/
        public ActionResult Index()
        {
            List<locationModel> locationlist = lcnrepo.ListLocation();
            return View(locationlist);
        }
        [HttpPost]
        public ActionResult Save(locationModel lmdl)
        {
            lmdl.lct_status = true;
            Boolean blnSvRslt = lcnrepo.SaveLocation(lmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Location Saved Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + lcnrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = lcnrepo.DeleteLocation(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Location Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + lcnrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
