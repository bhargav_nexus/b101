﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.DataBaseRepo;
using B101_1.Models;
using B101_1.ViewModels;
using System.Web.Security;

namespace B101_1.Controllers.Admin
{
    public class PrpHomeController : Controller
    {
        PrpHomeRepo hmrepo = new PrpHomeRepo();
        BuilderRepo bldrepo = new BuilderRepo();
        BuildercatagerymappingRepo bcatrepo = new BuildercatagerymappingRepo();
        HomeGalleryRepo glryrepo = new HomeGalleryRepo();
        ImageRepo imgrepo = new ImageRepo();
        HomefeatureMappingRepo hfmrepo = new HomefeatureMappingRepo();
        //
        // GET: /Home/
        public ActionResult Index()
        {
            List<prpHomeModel> prphmlist;
            if (User.IsInRole("administrator"))
            {
                prphmlist = hmrepo.ListHomes();
            }
            else
            {
                prphmlist = hmrepo.ListHomesByBuilder((Guid)Membership.GetUser().ProviderUserKey);
            }
            return View(prphmlist);
        }

        public ActionResult Add()
        {
            if (User.IsInRole("builder"))
            {
                Int64 bid = bldrepo.GetBuilderByUserid((Guid)Membership.GetUser().ProviderUserKey).bm_id;
                ViewBag.bid = bid;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Preview(prpHomeModel hmdl)
        {
            return View(hmdl);
        }
        [ValidateInput(false)]
        [HttpPost]

        public ActionResult Add(prpHomeModel hmdl)
        {
            if (hmdl.hm_id == -1)
            {
                hmdl = (prpHomeModel)Session["hmdl"];
                hmdl.hm_id = -1;
                hmdl.hmmainimage = null;
                hmdl.hmflorplanimage = null;
            }

            hmdl.hm_catagery = bcatrepo.GetBuildercatagery(hmdl.hm_builder);
            hmdl.hm_subcatagery = bcatrepo.GetBuildersubcatagery(hmdl.hm_builder);

            imageMasterModel imgmdl = new imageMasterModel();
            if (hmdl.hmmainimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                hmdl.mainimage = imgmdl.img_savedname;
                imgmdl.img_name = hmdl.hmmainimage.FileName;
                hmdl.hmmainimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/property/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                imgrepo.SaveImage(imgmdl);
                hmdl.hm_mainimage = Convert.ToInt64(imgrepo.GetSetError);
            }
            if (hmdl.hmflorplanimage != null)
            {
                imgmdl.img_savedname = (DateTime.Now.Ticks) + 1.ToString() + ".jpg";
                hmdl.floreplanimage = imgmdl.img_savedname;
                imgmdl.img_name = hmdl.hmflorplanimage.FileName;
                hmdl.hmflorplanimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/property/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                imgrepo.SaveImage(imgmdl);
                hmdl.hm_florplanimage = Convert.ToInt64(imgrepo.GetSetError);
            }
            if (hmdl.hm_id == 0)
            {
                builderModel bldr = bldrepo.GetBuilder(hmdl.hm_builder);
                hmdl.bldrname = bldr.bm_name;
                hmdl.storey = hmrepo.GetStoreyById(hmdl.hm_storey);
                hmdl.locationname = hmrepo.GetLocationById(hmdl.hm_location);
                hmdl.vilagename = hmrepo.GetVIllagesById(hmdl.hm_village);
                hmdl.features = hmrepo.GetFeaturesById(hmdl.hmfeatures);
                Session["hmdl"] = hmdl;
                return View("Preview", hmdl);
            }
            Boolean blnSvRslt = hmrepo.SaveHome(hmdl);
            if (blnSvRslt)
            {
                homefeatureMappingModel hfm = new homefeatureMappingModel();
                hfm.hfm_homeid = Convert.ToInt64(hmrepo.GetSetError);
                foreach (string f in hmdl.hmfeatures.Split(','))
                {
                    hfm.hfm_ftrid = Convert.ToInt64(f);
                    hfmrepo.SaveMapping(hfm);
                }
                MySession.Current.ErrorMessage = "Record Saved Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error:" + hmrepo.GetSetError;
                return View(hmdl);
            }
        }

        public ActionResult Cancle()
        {
            if (Session["hmdl"] != null)
            {
                prpHomeModel hmmdl = (prpHomeModel)Session["hmdl"];
                Boolean blnrslt = imgrepo.DeleteImage(hmmdl.hm_mainimage);
                if(blnrslt)
                {
                    blnrslt = imgrepo.DeleteImage(hmmdl.hm_florplanimage);
                }
                Session.Abandon();
            }
            return RedirectToAction("Add", "PrpHome");
        }

        public ActionResult Edit(Int64 id)
        {
            prpHomeModel hmdl = hmrepo.GetHome(id);
            if (hmdl.hm_mainimage != 0)
            {
                hmdl.mainimage = imgrepo.ReadImageName(hmdl.hm_mainimage);
            }
            else
            {
                hmdl.mainimage = string.Empty;
            }
            if (hmdl.hm_florplanimage != 0)
            {
                hmdl.floreplanimage = imgrepo.ReadImageName(hmdl.hm_florplanimage);
            }
            else
            {
                hmdl.floreplanimage = string.Empty;
            }
            foreach (Int64 fi in hfmrepo.HomeFeatures(id))
            {
                hmdl.hmfeatures = hmdl.hmfeatures + fi.ToString() + ",";
                hmdl.hmfeatures = hmdl.hmfeatures.TrimEnd(',');
            }
            return View(hmdl);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(prpHomeModel hmdl)
        {
            hmdl.hm_catagery = bcatrepo.GetBuildercatagery(hmdl.hm_builder);
            hmdl.hm_subcatagery = bcatrepo.GetBuildersubcatagery(hmdl.hm_builder);

            imageMasterModel imgmdl = new imageMasterModel();
            if (hmdl.hmmainimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = hmdl.hmmainimage.FileName;
                hmdl.hmmainimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/property/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                if (hmdl.mainimage != null)
                {
                    imgrepo.UpdateImage(hmdl.mainimage, imgmdl);
                }
                else
                {
                    imgrepo.SaveImage(imgmdl);
                }
                hmdl.hm_mainimage = Convert.ToInt64(imgrepo.GetSetError);
            }
            if (hmdl.hmflorplanimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = hmdl.hmflorplanimage.FileName;
                hmdl.hmflorplanimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/property/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                if (hmdl.floreplanimage != null)
                {
                    imgrepo.UpdateImage(hmdl.floreplanimage, imgmdl);
                }
                else
                {
                    imgrepo.SaveImage(imgmdl);
                }
                hmdl.hm_florplanimage = Convert.ToInt64(imgrepo.GetSetError);
            }
            Boolean blnSvRslt = hmrepo.UpdateHome(hmdl);
            if (blnSvRslt)
            {
                hfmrepo.DeleteMapping(hmdl.hm_id);
                homefeatureMappingModel hfm = new homefeatureMappingModel();
                hfm.hfm_homeid = Convert.ToInt64(hmdl.hm_id);
                foreach (string f in hmdl.hmfeatures.Split(','))
                {
                    hfm.hfm_ftrid = Convert.ToInt64(f);
                    hfmrepo.SaveMapping(hfm);
                }
                MySession.Current.ErrorMessage = "Record Saved Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error:" + hmrepo.GetSetError;
                return View();
            }
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean blnrslt = hmrepo.DeleteHome(id);
            if (blnrslt)
            {
                hfmrepo.DeleteMapping(id);
                blnrslt = glryrepo.DeleteHomeGlry(id);
                if (blnrslt)
                {
                    MySession.Current.ErrorMessage = "Record Deleted Successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error : Deleting Gallery Record";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult Gallery(Int64 id)
        {
            List<homegalleryModel> imagelist = glryrepo.ListGalleryByHome(id);
            ViewBag.itmid = id;
            return View(imagelist);
        }
        public ActionResult GalleryAdd(Int64 id)
        {
            ViewBag.itmid = id;
            ViewBag.DispOrd = glryrepo.getdisplord(id);
            return View();
        }
        [HttpPost]
        public ActionResult GalleryAdd(ImageViewModel itmimage)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            imgmdl.img_altText = itmimage.AltText;
            imgmdl.img_name = itmimage.Image.FileName;
            imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
            imgmdl.img_name = itmimage.Image.FileName;
            itmimage.Image.SaveAs(HttpContext.Server.MapPath("~/Content/images/property/" + imgmdl.img_savedname));
            bool blnSvImag = imgrepo.SaveImage(imgmdl);
            if (blnSvImag == true)
            {
                homegalleryModel hgmdl = new homegalleryModel();
                hgmdl.hg_displayorder = itmimage.displayOrder;
                hgmdl.hg_hmid = itmimage.itmid;
                hgmdl.hg_imageid = Convert.ToInt64(imgrepo.GetSetError);
                Boolean blnSvGlry = glryrepo.SaveHomeImage(hgmdl);
                if (blnSvGlry)
                {
                    return RedirectToAction("Gallery", new { id = itmimage.itmid });
                }
                else
                {
                    MySession.Current.ErrorMessage = glryrepo.GetSetError;
                    return RedirectToAction("GalleryAdd", new { id = itmimage.itmid });
                }
            }
            else
            {
                MySession.Current.ErrorMessage = imgrepo.GetSetError;
                return RedirectToAction("GalleryAdd", new { id = itmimage.itmid });
            }
        }
        [HttpGet]
        public ActionResult Gallerydelete(Int64 id, Int64 subid)
        {
            bool blnRsltDlt = glryrepo.DeleteHomeImage(subid);
            if (blnRsltDlt)
            {
                bool blnimgDltRslt = imgrepo.DeleteImage(subid);
                if (blnimgDltRslt)
                {
                    return RedirectToAction("Gallery", new { id = id });
                }
                else
                {
                    MySession.Current.ErrorMessage = glryrepo.GetSetError;
                    return RedirectToAction("Gallery", new { id = id });
                }
            }
            else
            {
                MySession.Current.ErrorMessage = glryrepo.GetSetError;
                return RedirectToAction("Gallery", new { id = id });
            }
        }
        public JsonResult ArreangeImages(String order, String dir)
        {
            string[] o = order.Split('#');

            Int32 ndo = 0;
            Int32 cdo = Convert.ToInt32(o[0]);
            String Query = string.Empty;
            if (dir == "U")
            {
                ndo = cdo - 1;
            }
            else if (dir == "D")
            {
                ndo = cdo + 1;
            }
            Boolean blnRs = glryrepo.Arrange(cdo, ndo, Convert.ToInt64(o[1]), Convert.ToInt64(o[2]));
            return Json(o[2]);
        }
    }
}
