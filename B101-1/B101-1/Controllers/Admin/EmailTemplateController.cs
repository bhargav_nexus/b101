﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles="administrator")]
    public class EmailTemplateController : Controller
    {
        EmailTemplateRepo templaterepo = new EmailTemplateRepo();
        //
        // GET: /EmailTemplate/

        public ActionResult Index()
        {
            List<emailtemplateModel> templates = templaterepo.TemplateList();
            return View(templates);
        }

        public ActionResult Edit(Int64 id)
        {
            emailtemplateModel emdl = templaterepo.gettemplatebyid(id);
            return View(emdl);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(emailtemplateModel etmplt)
        {
            Boolean blnRslt = templaterepo.EditTemplate(etmplt);
            if (blnRslt)
            {
                MySession.Current.ErrorMessage = "Template Saved Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = templaterepo.GetSetError;
                return View();
            }
        }
    }
}
