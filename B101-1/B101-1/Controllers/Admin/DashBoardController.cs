﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator,builder")]
    public class DashBoardController : Controller
    {
        BuilderRepo bldrrpo = new BuilderRepo();
        //
        // GET: /DashBoard/
        public ActionResult Index()
        {
            if(User.IsInRole("builder"))
            {
                builderModel bldMdl = bldrrpo.GetBuilderByUserid((Guid)Membership.GetUser().ProviderUserKey);
                if (!bldMdl.bm_isprofcompltd)
                {
                    ViewBag.ispc = false;
                }
                else
                {
                    ViewBag.ispc = true;
                }
            }
            return View();
        }
    }
}
