﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;
using B101_1.ViewModels;

namespace B101_1.Controllers
{
    [Authorize(Roles = "administrator")]
    public class BuildercatageryController : Controller
    {
        BuilderCatRepo catrepo = new BuilderCatRepo();
        //
        // GET: /buildercatagery/

        public ActionResult Index()
        {
            List<buildercatageryModel> catlist = catrepo.ListCatagery();
            return View(catlist);
        }

        [HttpPost]
        public ActionResult Save(buildercatageryModel bmdl)
        {
            Boolean blnSvRslt = catrepo.SaveCatagery(bmdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Catageory Saved Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + catrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(Int64 id)
        {
            Boolean blnSvRslt = catrepo.DeleteBCatagery(id);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Catagery Delete Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + catrepo.GetSetError;
            }
            return RedirectToAction("Index");
        }
    }
}
