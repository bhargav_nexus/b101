﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{

    public class SliderController : Controller
    {
        SliderRepo sliderrepo = new SliderRepo();
        ImageRepo imagerepo = new ImageRepo();

        //
        // GET: /Slider/

        public ActionResult Index()
        {
            List<SliderViewModel> slides = sliderrepo.GetSliders();
            return View(slides);
        }

        [HttpGet]
        public ActionResult AddNew()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddNew(SliderViewModel slider, HttpPostedFileBase ImagePath)
        {
            imageMasterModel imgmstr = new imageMasterModel();
            // all validation for image goes here
            imgmstr.img_altText = slider.AltText;
            imgmstr.img_savedname = DateTime.Now.Ticks.ToString()+".jpg";
            imgmstr.img_name = ImagePath.FileName;
            ImagePath.SaveAs(HttpContext.Server.MapPath("~/Content/images/slider/" + imgmstr.img_savedname));
            bool BlnRsltImgSave= imagerepo.SaveImage(imgmstr);
            if (BlnRsltImgSave)
            {
                sliderModel sldmdl = new sliderModel();
                sldmdl.sld_dsplorder = slider.displayOrder;
                sldmdl.sld_imageid = Convert.ToInt64(imagerepo.GetSetError);
                bool BlnRsltSldSave = sliderrepo.SaveSlider(sldmdl);
                if (BlnRsltSldSave)
                {
                    return RedirectToAction("Index", "Slider");
                }
                else
                {
                    MySession.Current.ErrorMessage = sliderrepo.GetSetError;
                    return View();
                }
            }
            else
            {
                MySession.Current.ErrorMessage = imagerepo.GetSetError;
                return View();
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Boolean blnDltSld= sliderrepo.DeleteSlider(id);
            if (blnDltSld)
            {
                Boolean blnDltImg = imagerepo.DeleteImage(Convert.ToInt32(sliderrepo.GetSetError));
                if (blnDltImg)
                {
                    return RedirectToAction("Index", "Slider");
                }
                else
                {
                    MySession.Current.ErrorMessage = imagerepo.GetSetError;
                    return RedirectToAction("Index", "Slider");
                }
            }
            else
            {
                MySession.Current.ErrorMessage = sliderrepo.GetSetError;
                return RedirectToAction("Index","Slider");
            }
        }
    }
}
