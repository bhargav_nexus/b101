﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    public class AdvSlotController : Controller
    {
        AdvslotsRepo sltrepo = new AdvslotsRepo();
        //
        // GET: /AdvSlot/

        public ActionResult Index()
        {
            List<advslotsModel> sltlst = sltrepo.ReadallSlot();
            return View(sltlst);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(advslotsModel mdl)
        {
            //Boolean slta = sltrepo.CheckSlotAvailability(mdl.slt_startdate,mdl.slt_enddate);
            //if (slta)
            //{
                Boolean blnSvRslt = sltrepo.SaveSlot(mdl);
                if (blnSvRslt)
                {
                    MySession.Current.ErrorMessage = "Slot Created Successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    MySession.Current.ErrorMessage = "Error : " + sltrepo.GetSetError;
                    return View();
                }
            //}
            //else
            //{
            //    MySession.Current.ErrorMessage = "Error : Date Range Is Already Assigned";
            //    return View();
            //}
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            advslotsModel slt = sltrepo.ReadaSlotbyId(id);
            return View(slt);
        }

        [HttpPost]
        public ActionResult Edit(advslotsModel mdl)
        {
            Boolean blnSvRslt = sltrepo.UpdateSlot(mdl);
            if (blnSvRslt)
            {
                MySession.Current.ErrorMessage = "Slot Updated Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : " + sltrepo.GetSetError;
                return View();
            }
        }
        public ActionResult Delete(Int64 id)
        {
            Boolean blnRslt = sltrepo.DeleteSlot(id);
            if (blnRslt)
            {
                MySession.Current.ErrorMessage = "Slot Deleted Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : " + sltrepo.GetSetError;
                return View();
            }
        }
    }
}
