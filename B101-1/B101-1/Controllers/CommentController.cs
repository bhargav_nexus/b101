﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    public class CommentController : Controller
    {
        BlogcommentRepo bcr = new BlogcommentRepo();
        //
        // GET: /Comment/
        [Authorize(Roles="administrator")]
        public ActionResult Index()
        {
            List<blogcommentModel> comments = bcr.ReadAllComment();
            return View(comments);
        }
        [Authorize(Roles="administrator")]
        public ActionResult Delete(Int64  id)
        {
            Boolean blnDltRslt = bcr.DeleteComment(id);
            if (blnDltRslt)
            {
                MySession.Current.ErrorMessage = "Comment Deleted Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Erro : "+bcr.GetSetError;
                return RedirectToAction("Index");   
            }
        }

        [Authorize(Roles = "administrator")]
        public JsonResult View(Int64 id)
        {
            blogcommentModel comment = bcr.ReadCommnetById(id);
            return Json(comment);
        }
        public ActionResult Add(blogcommentModel blgcmt)
        {
            blgcmt.cmt_datetime = DateTime.Now;
            blgcmt.cmt_aprvsts = true;
            Boolean blnSvRslt = bcr.SaveComment(blgcmt);
            if (blnSvRslt)
            {
                return RedirectToAction("View", "BlogArticle", new { id = blgcmt.cmt_blog_id });
            }
            else
            {
                MySession.Current.ErrorMessage = "Error : " +bcr.GetSetError;
                return RedirectToAction("View", "BlogArticle", new { id = blgcmt.cmt_blog_id });
            }
        }
    }
}
