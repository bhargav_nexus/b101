﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;
using System.Web.Security;

namespace B101_1.Controllers.Admin
{
    public class BlogArticleController : Controller
    {
        BlogArticlesRepo artrepo = new BlogArticlesRepo();
        ImageRepo imgrepo = new ImageRepo();
        BlogtagmappingRepo btmrepo = new BlogtagmappingRepo();
        BlogcommentRepo blgcmtrepo = new BlogcommentRepo();
        //
        // GET: /BlogArticle/
        [Authorize(Roles = "administrator")]
        public ActionResult View(Int64 id)
        {
            blogArticlesModel blg = artrepo.ReadArticle(id);
            return View(blg);
        }

        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            List<blogArticlesModel> Articles = artrepo.ReadAllArticles();
            return View(Articles);
        }
        [Authorize(Roles = "administrator")]
        public ActionResult Add()
        {
            return View();
        }
        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(blogArticlesModel blgmdl)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            if (blgmdl.mainimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = blgmdl.mainimage.FileName;
                blgmdl.mainimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/blog/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                imgrepo.SaveImage(imgmdl);
                blgmdl.ba_mainimage = Convert.ToInt64(imgrepo.GetSetError);
            }
            blgmdl.ba_createddate = DateTime.Now;
            blgmdl.ba_status = true;
            blgmdl.ba_author = (Guid)Membership.GetUser().ProviderUserKey;

            Boolean blnSvRslt = artrepo.SaveArticle(blgmdl);
            if (blnSvRslt)
            {
                blogtagmappingModel btm = new blogtagmappingModel();
                btm.btm_blgid = Convert.ToInt64(artrepo.GetSetError);
                string[] tags = blgmdl.BlogTags.Split(',');
                foreach (string tg in tags)
                {
                    btm.btm_tagid = Convert.ToInt64(tg);
                    btmrepo.SaveTagMapping(btm);
                }
                MySession.Current.ErrorMessage = "Article Submitted Successfully ...!!!";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = artrepo.GetSetError;
                return View();
            }
        }
        [Authorize(Roles = "administrator")]
        public ActionResult Edit(Int64 id)
        {
            BlogViewModel bvmdl = new BlogViewModel();

            bvmdl.article = artrepo.ReadArticle(id);
            foreach (string s in btmrepo.SelectedTaglist(bvmdl.article.ba_id))
            {
                bvmdl.selectedtags = bvmdl.selectedtags + "," + s;
            }
            bvmdl.selectedtags.TrimStart(',');
            return View(bvmdl);
        }
        [Authorize(Roles = "administrator")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(blogArticlesModel blgmdl)
        {
            imageMasterModel imgmdl = new imageMasterModel();
            if (blgmdl.mainimage != null)
            {
                imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                imgmdl.img_name = blgmdl.mainimage.FileName;
                blgmdl.mainimage.SaveAs(HttpContext.Server.MapPath("~/Content/images/blog/" + imgmdl.img_savedname));
                imgmdl.img_altText = string.Empty;
                if (blgmdl.image != null)
                {
                    imgrepo.UpdateImage(blgmdl.image, imgmdl);
                }
                else
                {
                    imgrepo.SaveImage(imgmdl);
                }
                blgmdl.ba_mainimage = Convert.ToInt64(imgrepo.GetSetError);
            }

            blgmdl.ba_createddate = DateTime.Now;
            blgmdl.ba_status = true;
            blgmdl.ba_author = (Guid)Membership.GetUser().ProviderUserKey;

            Boolean blnUpdtRslt = artrepo.UpdateArticle(blgmdl);
            if (blnUpdtRslt)
            {
                blogtagmappingModel btm = new blogtagmappingModel();
                Boolean dltrslt = btmrepo.DeleteTagMappingByArticle(blgmdl.ba_id);
                if (dltrslt)
                {
                    btm.btm_blgid = blgmdl.ba_id;
                    string[] tags = blgmdl.BlogTags.Split(',');
                    foreach (string tg in tags)
                    {
                        if (tg != string.Empty)
                        {
                            btm.btm_tagid = Convert.ToInt64(tg);
                            btmrepo.SaveTagMapping(btm);
                        }
                    }
                }
                MySession.Current.ErrorMessage = "Article Updated Successfully ...!!!";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = artrepo.GetSetError;
                return View();
            }
        }
        [Authorize(Roles = "administrator")]
        public ActionResult Delete(Int64 id)
        {
            Boolean blnRslt = artrepo.DeleteArticle(id);
            if (blnRslt)
            {
                MySession.Current.ErrorMessage = "Article Deleted Successfully";
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :" + artrepo.GetSetError;
            }
            return RedirectToAction("index");
        }
    }
}
