﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles="administrator")]
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAttributeValue(int id)
        {
            List<attributevalueModel> vallist = new AttributeRepo().ReadValuebyAttribute(id);
            return Json(vallist);
        }
    }
}
