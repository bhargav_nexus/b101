﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;

namespace B101_1.Controllers.Admin
{
    [Authorize(Roles = "administrator")]
    public class ItemController : Controller
    {
        CatageryRepo catrepo = new CatageryRepo();
        ProductRepo prdctrepo = new ProductRepo();
        ImageRepo imgrepo = new ImageRepo();
        //
        // GET: /Kitchen/

        public ActionResult Index()
        {
            List<ProductViewModel> productlist = prdctrepo.AllReadProduct();
            return View(productlist);
        }
        [HttpGet]
        public ActionResult AddNew()
        {
            return View();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddNew(ProductViewModel prdview, List<HttpPostedFileBase> files)
        {
            productModel prdmdl = new productModel();
            imageMasterModel imgmdl = new imageMasterModel();
            for (int i = 0; i < files.Count; i++)
            {
                var fl = files[i];
                if (fl != null)
                {
                    imgmdl.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmdl.img_name = fl.FileName;
                    fl.SaveAs(HttpContext.Server.MapPath("~/Content/images/products/" + imgmdl.img_savedname));
                    if (i == 0)
                    {
                        imgmdl.img_altText = string.Empty;
                        imgrepo.SaveImage(imgmdl);
                        prdmdl.prd_defaultimg = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    if (i == 1)
                    {
                        imgmdl.img_altText = string.Empty;
                        imgrepo.SaveImage(imgmdl);
                        prdmdl.prd_smallimg = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    if (i == 2)
                    {
                        imgmdl.img_altText = string.Empty;
                        imgrepo.SaveImage(imgmdl);
                        prdmdl.prd_midimg = Convert.ToInt64(imgrepo.GetSetError);
                    }
                    if (i == 3)
                    {
                        imgmdl.img_altText = string.Empty;
                        imgrepo.SaveImage(imgmdl);
                        prdmdl.prd_bigimg = Convert.ToInt64(imgrepo.GetSetError);
                    }
                }
            }
            prdmdl.prd_catageryid = Convert.ToInt64(prdview.Catagery);
            prdmdl.prd_description = prdview.Description;
            prdmdl.prd_price = prdview.Price;
            prdmdl.prd_productname = prdview.ProductName;
            prdmdl.prd_shortdescription = prdview.ShortDescription;
            prdmdl.prd_status = prdview.Status;
            prdmdl.prd_parentcat = catrepo.GetParetCatID(prdmdl.prd_catageryid);
            bool blnSVRslt = prdctrepo.SaveProduct(prdmdl);
            if (blnSVRslt)
            {
                return RedirectToAction("Index", "Item");
            }
            else
            {
                MySession.Current.ErrorMessage = prdctrepo.GetSetError;
                return View();
            }
        }

        public ActionResult Edit(Int64 id)
        {
            ProductViewModel product = prdctrepo.ReadProductById(id);
            if (product.DefaultImage != "0")
            {
                imageMasterModel DefaultImage = imgrepo.ReadImage(Convert.ToInt64(product.DefaultImage));
                product.DefaultImage = DefaultImage.img_savedname;
                product.DIAlt = DefaultImage.img_altText;
            }
            if (product.SmallImage != "0")
            {
                imageMasterModel SmallImage = imgrepo.ReadImage(Convert.ToInt64(product.SmallImage));
                product.SmallImage = SmallImage.img_savedname;
                product.SIAlt = SmallImage.img_altText;
            }
            else
            {
                product.SmallImage = string.Empty;
            }
            if (product.Midimage != "0")
            {
                imageMasterModel MidImage = imgrepo.ReadImage(Convert.ToInt64(product.Midimage));
                product.Midimage = MidImage.img_savedname;
                product.MIAlt = MidImage.img_altText;
            }
            else
            {
                product.Midimage = string.Empty;
            }
            if (product.BigImaage != "0")
            {
                imageMasterModel BigImage = imgrepo.ReadImage(Convert.ToInt64(product.BigImaage));
                product.BigImaage = BigImage.img_savedname;
                product.BIAlt = BigImage.img_altText;
            }
            else
            {
                product.BigImaage = string.Empty;
            }
            return View(product);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(ProductViewModel prdview, List<HttpPostedFileBase> files)
        {
            for(int i=0;i<files.Count;i++)
            {
                imageMasterModel imgmstr = new imageMasterModel();
                var file = files[i];
                if (file != null)
                {
                    imgmstr.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
                    imgmstr.img_name = file.FileName;
                    file.SaveAs(HttpContext.Server.MapPath("~/Content/images/products/" + imgmstr.img_savedname));
                    if(i == 0)
                    {
                        if (prdview.DefaultImage != string.Empty)
                        {
                            imgrepo.UpdateImage(prdview.DefaultImage,imgmstr);
                        }
                        else
                        {
                            imgrepo.SaveImage(imgmstr);
                            prdctrepo.UpdateProductImage("prd_defaultimg", imgrepo.GetSetError, prdview.Id);
                        }
                    }
                    else if(i == 1)
                    {
                        if (prdview.SmallImage != null)
                        {
                            imgrepo.UpdateImage(prdview.SmallImage, imgmstr);
                        }
                        else
                        {
                            imgrepo.SaveImage(imgmstr);
                            prdctrepo.UpdateProductImage("prd_smallimg", imgrepo.GetSetError, prdview.Id);
                        }
                    }
                    else if (i == 2)
                    {
                        if (prdview.Midimage != null)
                        {
                            imgrepo.UpdateImage(prdview.Midimage, imgmstr);
                        }
                        else
                        {
                            imgrepo.SaveImage(imgmstr);
                            prdctrepo.UpdateProductImage("prd_midimg", imgrepo.GetSetError, prdview.Id);
                        }
                    }
                    else if (i == 3)
                    {
                        if (prdview.BigImaage != null)
                        {
                            imgrepo.UpdateImage(prdview.BigImaage, imgmstr);
                        }
                        else
                        {
                            imgrepo.SaveImage(imgmstr);
                            prdctrepo.UpdateProductImage("prd_bigimg", imgrepo.GetSetError, prdview.Id);
                        }
                    }
                }
            }
            productModel prdmdl = new productModel();
            bool blnUpRslt = prdctrepo.UpdateProduct(prdview);
            if (blnUpRslt)
            {
                return RedirectToAction("Index", "Item");
            }
            else
            {
                MySession.Current.ErrorMessage = prdctrepo.GetSetError;
                return View();
            }
        }

        #region ItemAttributes
        public ActionResult ItemAttributes(Int64 id)
        {
            ViewBag.prdct = id;
            List<ProductAtributeViewModel> attributelist = prdctrepo.ReadAttributeByProduct(id);
            return View(attributelist);
            return View();
        }
        [HttpPost]
        public ActionResult ItemAttributes(productattributesModel prdctattrmdl)
        {
            bool blnSvReslt = prdctrepo.SaveProductAttribute(prdctattrmdl);
            if (blnSvReslt)
            {
                return RedirectToAction("ItemAttributes", new { id = prdctattrmdl.prat_productid });
            }
            else
            {
                MySession.Current.ErrorMessage = prdctrepo.GetSetError;
                return RedirectToAction("ItemAttributes", new { id = prdctattrmdl.prat_productid });
            }
        }

        public ActionResult DeleteAttributes(Int64 id, Int64 subid)
        {
            bool blnRsltDlt = prdctrepo.DeleteAttributeValue(subid);
            if (blnRsltDlt)
            {
                return RedirectToAction("ItemAttributes", new { id = id });
            }
            else
            {
                MySession.Current.ErrorMessage = prdctrepo.GetSetError;
                return RedirectToAction("ItemAttributes", new { id = id });
            }
        }
        #endregion

        #region Item Gallery
        public ActionResult ItemImage(Int64 id)
        {
            ViewBag.Pi = id;
            List<ImageViewModel> attributelist = prdctrepo.ListProductImages(id);
            return View(attributelist);
        }
        [HttpGet]
        public ActionResult NewImage(Int64 id)
        {
            ViewBag.Pi = id;
            return View();
        }
        [HttpPost]
        //public ActionResult NewImage(ImageViewModel itmimage, HttpPostedFileBase ImagePath)
        //{
        //    imageMasterModel imgmstr = new imageMasterModel();
        //    // all validation for image goes here
        //    imgmstr.img_altText = itmimage.AltText;
        //    imgmstr.img_savedname = DateTime.Now.Ticks.ToString() + ".jpg";
        //    imgmstr.img_name = ImagePath.FileName;
        //    ImagePath.SaveAs(HttpContext.Server.MapPath("~/Content/images/products/" + imgmstr.img_savedname));
        //    bool blnSvImag = imgrepo.SaveImage(imgmstr);
        //    if (blnSvImag == true)
        //    {
        //        productimageModel prdimgmdl = new productimageModel();
        //        prdimgmdl.pi_imageid = Convert.ToInt64(imgrepo.GetSetError);
        //        prdimgmdl.pi_prodid = itmimage.ProductId;
        //        prdimgmdl.pi_displayoreder = itmimage.displayOrder;
        //        bool blnSvRslt = prdctrepo.SaveProductImage(prdimgmdl);
        //        if (blnSvRslt)
        //        {
        //            return RedirectToAction("ItemImage", new { id = itmimage.ProductId });
        //        }
        //        else
        //        {
        //            MySession.Current.ErrorMessage = prdctrepo.GetSetError;
        //            return RedirectToAction("NewImage", new { id = itmimage.ProductId });
        //        }
        //    }
        //    else
        //    {
        //        MySession.Current.ErrorMessage = prdctrepo.GetSetError;
        //        return RedirectToAction("NewImage", new { id = itmimage.ProductId });
        //    }
        //}
        public JsonResult ArreangeImages(String order, String dir)
        {
            string[] o = order.Split('#');

            Int32 ndo = 0;
            Int32 cdo = Convert.ToInt32(o[0]);
            String Query = string.Empty;
            if (dir == "U")
            {
                ndo = cdo - 1;
            }
            else if (dir == "D")
            {
                ndo = cdo + 1;
            }
            Boolean blnRs = prdctrepo.Arrange(cdo, ndo, Convert.ToInt64(o[1]), Convert.ToInt64(o[2]));
            return Json(o[2]);
        }
        public ActionResult DeleteItemImage(Int64 id, Int64 subid)
        {
            bool blnRsltDlt = prdctrepo.DeleteProductImage(subid);
            if (blnRsltDlt)
            {
                bool blnimgDltRslt = imgrepo.DeleteImage(subid);
                if (blnimgDltRslt)
                {
                    return RedirectToAction("ItemImage", new { id = id });
                }
                else
                {
                    MySession.Current.ErrorMessage = prdctrepo.GetSetError;
                    return RedirectToAction("ItemImage", new { id = id });
                }
            }
            else
            {
                MySession.Current.ErrorMessage = prdctrepo.GetSetError;
                return RedirectToAction("ItemImage", new { id = id });
            }
        }

        public JsonResult RemoveImage(String ImagePrp)
        {
            string[] prp = ImagePrp.Split('#');
            imgrepo.DeleteImageByName(prp[0]);
            string flag = string.Empty;
            switch (Convert.ToInt32(prp[1]))
            {
                case 1:
                    flag = "prd_defaultimg";
                    break;
                case 2:
                    flag = "prd_smallimg";
                    break;
                case 3:
                    flag = "prd_midimg";
                    break;
                case 4:
                    flag = "prd_bigimg";
                    break;
            }
            prdctrepo.UpdateProductImage(flag, "0",Convert.ToInt64(prp[2]));
            return Json(Convert.ToInt64(prp[2]));
        }
        #endregion

        public JsonResult GetAttributeValue(int id)
        {
            List<attributevalueModel> vallist = new AttributeRepo().ReadValuebyAttribute(id);
            return Json(vallist);
        }
    }
}
