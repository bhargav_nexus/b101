﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using B101_1.Models;
using B101_1.DataBaseRepo;
using MailChimp;
using MailChimp.Lists;
using MailChimp.Helper;

namespace B101_1.Controllers
{
    public class NewsletterController : Controller
    {
        //
        // GET: /Newsletter/
        NewsletterRepo nwsrepo = new NewsletterRepo();

        [Authorize(Roles="administrator")]
        public ActionResult Index()
        {
            List<newsletterModel> subscriberlist = nwsrepo.ListSubscriber();
            return View(subscriberlist);
        }
        public ActionResult Subscribe()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Subscribe(newsletterModel mdl)
        {
            MailChimpManager mm = new MailChimpManager("af5773c3af8fd293a546c6ffc4edc220-us8");
            EmailParameter email = new EmailParameter()
            {
                Email = mdl.ns_email
            };
            EmailParameter results = mm.Subscribe("fd105e6486", email,null,"html",true,true,true,true);
            
            //mdl.ns_status = true;
            //Boolean blnrslt= nwsrepo.SaveSubscriber(mdl);
            //if (blnrslt)
            //{
                MySession.Current.MSGCODE = "SUBSUCC";
                return RedirectToAction("Message", "Home");
            //}
            //else
            //{
            //    MySession.Current.ErrorMessage = "Error :" + nwsrepo.GetSetError;
            //    return View();
            //}
        }

        public ActionResult Changests(Int64 id)
        {
            Boolean blnRslt = nwsrepo.ChangeStatus(id);
            if (blnRslt)
            {
                MySession.Current.ErrorMessage = "Status Changed Successfully";
                return RedirectToAction("Index");
            }
            else
            {
                MySession.Current.ErrorMessage = "Error :"+nwsrepo.GetSetError;
                return RedirectToAction("Index");
            }
        }
    }
}
