﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;
using B101_1.ViewModels;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_BuilderMaster"), PetaPoco.PrimaryKey("bm_id")]
    public class BuilderViewModel
    {
        public builderModel buildermdl { get; set; }
        public List<ImageViewModel> galleryimages { get; set; }
    }
}