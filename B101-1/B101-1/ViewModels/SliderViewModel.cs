﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.ViewModels
{
    public class SliderViewModel
    {
        [PetaPoco.Column("sld_id")]
        public int sliderId { get; set; }

        [PetaPoco.Column("img_savedname")]
        public string ImagePath { get; set; }

        [PetaPoco.Column("img_altText")]
        public string AltText { get; set; }

        [PetaPoco.Column("sld_dsplorder")]
        public int displayOrder { get; set; }
    }
}