﻿using System;
using System.Collections.Generic;using System.Linq;
using System.Web;

namespace B101_1.ViewModels
{
    public class ImageViewModel
    {
        public Int64 ImageId { get; set; }
        public String ImagePath { get; set; }
        public Int64 displayOrder { get; set; }
        public Int64 itmid { get; set; }
        public String AltText { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}