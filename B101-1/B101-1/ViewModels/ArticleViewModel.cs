﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;

namespace B101_1.ViewModels
{
    public class ArticleViewModel
    {
        public blogArticlesModel article { get; set; }
        public List<blogcommentModel> comments { get; set; }
    }
}