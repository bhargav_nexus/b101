﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;

namespace B101_1.ViewModels
{
    public class AdvSlotMappingViewModel
    {
        public Int64 asm_id{ get; set; }
        public String adv_name { get; set; }
        public String adv_url { get; set; }
        public DateTime slt_startdate { get; set; }
        public DateTime slt_enddate { get; set; }
        public String location { get; set; }
        public String adv_image { get; set; }
    }
}