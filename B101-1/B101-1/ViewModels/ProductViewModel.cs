﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;


namespace B101_1.ViewModels
{
    public class ProductViewModel
    {
        [PetaPoco.Column("prd_id")]
        public Int64 Id { get; set; }
        [PetaPoco.Column("prd_productname")]
        public String ProductName { get; set; }
        [PetaPoco.Column("Catagery")]
        public String Catagery { get; set; }
        [PetaPoco.Column("Type")]
        public String Type { get; set; }
        [PetaPoco.Column("prd_price")]
        public Double Price { get; set; }
        [PetaPoco.Column("prd_shortdescription")]
        public String ShortDescription { get; set; }
        [PetaPoco.Column("prd_description")]
        public String Description { get; set; }
        [PetaPoco.Column("prd_smallimg")]
        public String SmallImage { get; set; }
        [PetaPoco.Ignore]
        public String SIAlt { get; set; }
        [PetaPoco.Column("prd_bigimg")]
        public String BigImaage { get; set; }
        [PetaPoco.Ignore]
        public String BIAlt { get; set; }
        [PetaPoco.Column("prd_midimg")]
        public String Midimage { get; set; }
        [PetaPoco.Ignore]
        public String MIAlt { get; set; }
        [PetaPoco.Column("prd_defaultimg")]
        public String DefaultImage { get; set; }
        [PetaPoco.Ignore]
        public String DIAlt { get; set; }
        [PetaPoco.Column("prd_status")]
        public Boolean Status { get; set; }
        [PetaPoco.Column("prd_catageryid")]
        public Int64 CatId { get; set; }
    }
}