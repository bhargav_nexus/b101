﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.ViewModels
{
    public class PrpHomeViewModel
    {
        public int ROWNUM { get; set; }
        public Int64 hm_id { get; set; }
        public String hmbuilder { get; set; }
        public String hmbuilderlogo { get; set; }
        public Int64 hm_catagery { get; set; }
        public Int64 hm_subcatagery { get; set; }
        public String hm_name { get; set; }
        public Double hm_pricefrm { get; set; }
        public Double hm_priceto { get; set; }
        public Int64 hm_pricerange { get; set; }
        public Int64 hm_bed { get; set; }
        public Int64 hm_bath { get; set; }
        public Int64 hm_storey { get; set; }
        public Double hm_area_sm { get; set; }
        public Double hm_area_s { get; set; }
        public Double hm_blocksize { get; set; }
        public Double hm_frontage { get; set; }
        public Int64 hm_location { get; set; }
        public String hm_address { get; set; }
        public Int64 hm_village { get; set; }
        public String hm_shortdescription { get; set; }
        public String hm_description { get; set; }
        public String hm_pagetitle { get; set; }
        public String hm_metakey { get; set; }
        public String hm_metadescription { get; set; }
        public Int64 hm_mainimage { get; set; }
        public Int64 hm_florplanimage { get; set; }
        public Boolean hm_status { get; set; }
        public Boolean hm_suitnb { get; set; }
        public String bm_budget { get; set; }
        public String bm_name { get; set; }
        public String mainimage { get; set; }
        public String floreplanimage { get; set; }
        public List<String> otherfeatures { get; set; }
        public List<ImageViewModel> galleryimages { get; set; }
    }
}