﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.ViewModels
{
    public class UserViewModel
    {
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public string UserName { get; set; }
        public bool IsAnonymous { get; set; }
        public string Email { get; set; }
        public DateTime LastActivityDate { get; set; }
    }
}