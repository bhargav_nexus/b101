﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.ViewModels
{
    public class ProductAtributeViewModel
    {
        public Int64 prat_id { get; set; }
        public String Attribute { get; set; }
        public String Value { get; set; }
    }

    public class ProductAtributeInsertModel
    {
        public Int64 ProductId { get; set; }
        public Int64 AttributeId { get; set; }
        public Int64 ValueId { get; set; }
        public String CustomValue { get; set; }
    }
}