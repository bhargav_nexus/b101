﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MySession
/// </summary>
public class MySession
{
    // private constructor
    private MySession()
    {
        ErrorMessage = string.Empty;
        AddEditId = 0;
        MSGCODE = string.Empty;
        model = null;

        B101_1.Models.SearchModel sm = new B101_1.Models.SearchModel();
        sm.FilterString = string.Empty;
        searchresult = sm;

        geolocationinfo = new B101_1.Models.GeoInfoModel();
    }

    // Gets the current session.
    public static MySession Current
    {
        get
        {
            MySession session =
              (MySession)HttpContext.Current.Session["__MySession__"];
            if (session == null)
            {
                session = new MySession();
                HttpContext.Current.Session["__MySession__"] = session;
            }
            return session;
        }
    }
    // **** add your session properties here, e.g like this:
    public String ErrorMessage { get; set; }
    public Int64 AddEditId { get; set; }
    public String MSGCODE { get; set; }
    public object model { get; set; }
    public B101_1.Models.SearchModel searchresult { get; set; }

    public B101_1.Models.GeoInfoModel geolocationinfo { get; set;}
}