﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_propimages"), PetaPoco.PrimaryKey("pri_id")]
    public class propimageModel
    {
        public Int64 pri_id { get; set; }
        public Int64 pri_propid { get; set; }
        public Int64 pri_imageid { get; set; }
        public Int64 pri_displayorder { get; set; }
    }
}