﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_advertisementMaster"), PetaPoco.PrimaryKey("adv_id")]
    public class advertisementModel
    {
        public Int64 adv_id { get; set; }
        public String adv_name { get; set; }
        public Int64 adv_image { get; set; }
        public String adv_url { get; set; }
        public String adv_alttext { get; set; }
        [ResultColumn]
        public HttpPostedFileBase advimage { get; set; }
        [ResultColumn]
        public String advimagenm { get; set; }
    }
}