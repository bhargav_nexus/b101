﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_regionMaster"), PetaPoco.PrimaryKey("rm_id")]
    public class regionModel
    {
        public Int64 rm_id { get; set; }
        public String rm_region { get; set; }
        public String rm_country { get; set; }
        public Boolean rm_status { get; set; }
    }
}