﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using B101_1.DataBaseRepo;

namespace B101_1.Models
{
    public class EmailMessage
    {
        public emailtemplateModel template { get; set; }
        public String mailto { get; set; }

        public String GetsetError { get; set; }

        public void sendmail()
        {
            // getting account associted with template
            template.emaccount = new EmailAccountRepo().getaccount(template.et_eaid);

            // configuring mail
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(template.emaccount.ea_email);
            mail.To.Add(this.mailto);
            mail.Subject = template.et_subject;
            mail.Body = template.et_emailbody;
            mail.IsBodyHtml = true;
            // configuring smtp server
            SmtpClient SmtpServer = new SmtpClient();
            SmtpServer.Host = template.emaccount.ea_smtpserver;
            SmtpServer.Port = 587;
            SmtpServer.EnableSsl = template.emaccount.ea_enablessl;
            SmtpServer.Credentials = new NetworkCredential(template.emaccount.ea_email, template.emaccount.ea_password);
            // sending mail
            try
            {
                SmtpServer.Send(mail);
            }
            catch(Exception ex)
            {
                GetsetError = ex.Message;
            }
        }
    }
}