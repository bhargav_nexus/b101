﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_AdditionalContent"), PetaPoco.PrimaryKey("CntId")]
    public class additionalContent
    {
        public int CntId { get; set; }
        public string CntKey { get; set; }
        public string CntContent { get; set; }
    }
}