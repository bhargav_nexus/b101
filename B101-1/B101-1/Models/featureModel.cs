﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_featureMaster"), PetaPoco.PrimaryKey("fm_id")]
    public class featureModel
    {
        public Int64 fm_id { get; set; }
        public String fm_feature { get; set; }
        public Boolean fm_status { get; set; }
    }
}