﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_attributeValueMaster"), PetaPoco.PrimaryKey("atrval_id")]
    public class attributevalueModel
    {
        public Int64 atrval_id { get; set; }
        public Int64 atrval_attrid { get; set; }
        public String atrval_value { get; set; }
    }
}