﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_productAttributes"), PetaPoco.PrimaryKey("prat_id")]
    public class productattributesModel
    {
        public Int64 prat_id { get; set; }
        public Int64 prat_productid { get; set; }
        public Int64 prat_attrid { get; set; }
        public Int64 prat_valueid { get; set; }
        public String prat_customvalue { get; set; }
    }
}