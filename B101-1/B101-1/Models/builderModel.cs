﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;
using B101_1.ViewModels;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_BuilderMaster"), PetaPoco.PrimaryKey("bm_id")]
    public class builderModel
    {
        public Int64 bm_id { get; set; }
        [PetaPoco.ResultColumn]
        public String bm_catagery { get; set; }
        public Guid bm_userid { get; set; }
        [PetaPoco.ResultColumn]
        public String bm_subcatagery { get; set; }
        public String bm_name { get; set; }
        public String bm_address { get; set; }
        public Int64 bm_region { get; set; }
        public String bm_phone { get; set; }
        public String bm_fax { get; set; }
        public String bm_email { get; set; }
        public String bm_website { get; set; }
        public String bm_shortdescription { get; set; }
        public String bm_description { get; set; }
        public String bm_pagetitle { get; set; }
        public String bm_metakey { get; set; }
        public String bm_metadescription { get; set; }
        public Int64 bm_headerimage { get; set; }
        public Int64 bm_searchimage { get; set; }
        public Boolean bm_status { get; set; }
        public Boolean bm_isprofcompltd { get; set; }
        public Boolean bm_isapproved { get; set; }
        [PetaPoco.ResultColumn]
        public String bmheaderimage { get; set; }
        [PetaPoco.ResultColumn]
        public String bmsearchimage { get; set; }
        [PetaPoco.ResultColumn]
        public HttpPostedFileBase headerimg { get; set; }
        [PetaPoco.ResultColumn]
        public HttpPostedFileBase searchimg { get; set; }
    }
}