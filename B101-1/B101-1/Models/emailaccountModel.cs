﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_emailaccountMaster"), PetaPoco.PrimaryKey("ea_id")]
    public class emailaccountModel
    {
        public Int64 ea_id { get; set; }
        public String ea_name { get; set; }
        public String ea_email { get; set; }
        public String ea_password { get; set; }
        public String ea_smtpserver { get; set; }
        public Boolean ea_enablessl { get; set; }
    }
}