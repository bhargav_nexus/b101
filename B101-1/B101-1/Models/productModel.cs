﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_productMaster"), PetaPoco.PrimaryKey("prd_id")]
    public class productModel
    {
        public Int64 prd_id { get; set; }
        public String prd_productname { get; set; }
        public Int64 prd_parentcat { get; set; }
        public Int64 prd_catageryid { get; set; }
        public String prd_shortdescription { get; set; }
        public String prd_description { get; set; }
        public Int64 prd_defaultimg { get; set; }
        public Int64 prd_bigimg { get; set; }
        public Int64 prd_midimg { get; set; }
        public Int64 prd_smallimg { get; set; }
        public Double prd_price { get; set; }
        public Boolean prd_status { get; set; }
    }
}