﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_productimages"), PetaPoco.PrimaryKey("pi_id")]
    public class productimageModel
    {
        public Int64 pi_id { get; set; }
        public Int64 pi_prodid { get; set; }
        public Int64 pi_imageid { get; set; }
        public Int64 pi_displayoreder { get; set; }
    }
}