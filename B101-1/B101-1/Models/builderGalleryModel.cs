﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_buildergallery"), PetaPoco.PrimaryKey("bg_id")]
    public class builderGalleryModel
    {
        public Int64 bg_id { get; set; }
        public Int64 bg_buildid { get; set; }
        public Int64 bg_imageid { get; set; }
        public Int64 bg_displayorder { get; set; }
        [PetaPoco.ResultColumn]
        public String ImagePath { get; set; }
    }
}