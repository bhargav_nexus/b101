﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_budgetMaster"), PetaPoco.PrimaryKey("bm_id")]
    public class budgetModel
    {
        public Int64 bm_id { get; set; }
        public Double bm_frmamnt { get; set; }
        public Double bm_toamnt { get; set; }
        public Boolean bm_status { get; set; }
    }
}