﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_tagMaster"), PetaPoco.PrimaryKey("bt_id")]
    public class tagModel
    {
        public Int64 bt_id { get; set; }
        public String bt_tag { get; set; }
    }
}