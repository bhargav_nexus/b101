﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.Models
{
    public class attributeModel
    {
        public int attr_id { get; set; }
        public string attr_attribute { get; set; }
        public Boolean attr_status { get; set; }
    }
}