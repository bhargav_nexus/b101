﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_homefeturesMapping"), PetaPoco.PrimaryKey("hfm_id")]
    public class homefeatureMappingModel
    {
        public Int64 hfm_id { get; set; }
        public Int64 hfm_homeid { get; set; }
        public Int64 hfm_ftrid { get; set; }
    }
}