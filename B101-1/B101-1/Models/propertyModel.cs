﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_property"), PetaPoco.PrimaryKey("prp_id")]
    public class propertyModel
    {
        public Int64 prp_id { get; set; }
        public String prp_name { get; set; }
        public Int64 prp_typeId { get; set; }
        public Int64 prp_bed { get; set; }
        public Int64 prp_bath { get; set; }
        public Double prp_price { get; set; }
        public String prp_description { get; set; }
        public Int64 prp_landsize { get; set; }
        public Int64 prp_buildingsize { get; set; }
        public String prp_otherFeatures { get; set;}
        public String prp_address { get; set; }
        public decimal prp_latitude { get; set; }
        public decimal prp_longitude { get; set; }
        public Boolean prp_isact { get; set; }
        public Int64 prp_storey { get; set; }
        public Int64 prp_defaultimage { get; set; }
        public Int64 prp_smallmage { get; set; }
        public Int64 prp_mdmimage { get; set; }
        public Int64 prp_bigimage { get; set; }
    }
}