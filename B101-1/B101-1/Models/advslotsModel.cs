﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_advslotMaster"), PetaPoco.PrimaryKey("slt_id")]
    public class advslotsModel
    {
        public Int64 slt_id { get; set; }
        public Int64 slt_location { get; set; }
        public DateTime slt_startdate { get; set; }
        public DateTime slt_enddate { get; set; }
        [ResultColumn]
        public String location { get; set; }
    }
}