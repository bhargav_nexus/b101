﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_imageMaster"), PetaPoco.PrimaryKey("img_id")]
    public class imageMasterModel
    {
        public long img_id { get; set; }
        public string img_altText { get; set; }
        public string img_name { get; set; }
        public string img_savedname { get; set; }
        public string img_path { get; set; }
    }
}