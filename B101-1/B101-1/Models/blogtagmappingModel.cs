﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_blogtagmapping"), PetaPoco.PrimaryKey("btm_id")]
    public class blogtagmappingModel
    {
        public Int64 btm_id { get; set; }
        public Int64 btm_blgid { get; set; }
        public Int64 btm_tagid { get; set; }
    }
}