﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_slider"), PetaPoco.PrimaryKey("sld_id")]
    public class sliderModel
    {
        public long sld_id { get; set; }
        public long sld_imageid { get; set; }
        public long sld_dsplorder { get; set; }
    }
}