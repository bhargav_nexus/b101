﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.Models
{
     [PetaPoco.TableName("tbl_productAttributes"), PetaPoco.PrimaryKey("prat_id")]
    public class productAttributeValueModel
    {
         public Int64 pav_id { get; set; }
        public Int64 pav_productid { get; set; }
        public Int64 pav_atrvalid { get; set; }
        public String pav_custom { get; set; }
    }
}