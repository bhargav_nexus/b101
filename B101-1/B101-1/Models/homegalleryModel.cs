﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_homegallery"), PetaPoco.PrimaryKey("hi_id")]
    public class homegalleryModel
    {
        public Int64 hg_id { get; set; }
        public Int64 hg_hmid { get; set; }
        public Int64 hg_imageid { get; set; }
        public Int64 hg_displayorder { get; set; }
        [PetaPoco.ResultColumn]
        public String ImagePath { get; set; }
    }
}