﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using System.ComponentModel.DataAnnotations;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_AdvSlotMapping"), PetaPoco.PrimaryKey("asm_id")]
    public class advsltmappingModel
    {
        public Int64 asm_id { get; set; }

        [Required]
        public Int64 asm_advit { get; set; }
        [Required]
        public Int64 asm_slotid { get; set; }
    }
}