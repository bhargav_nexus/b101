﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_blogcatageryMaster"), PetaPoco.PrimaryKey("bc_id")]
    public class blogcatageryModel
    {
        public Int64 bc_id { get; set; }
        public String bc_catagery { get; set; }
        public Int64 bc_mainimage { get; set; }
        public Boolean bc_isstatic { get; set; }
        [ResultColumn]
        public HttpPostedFileBase mainimage { get; set; }
        [ResultColumn]
        public String mainimagenm { get; set; }
    }
}