﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("PageContentMaster"), PetaPoco.PrimaryKey("cp_id")]
    public class pagecontentModel
    {
        public Int64 cp_id { get; set; }
        public String cp_key { get; set; }
        public String cp_url { get; set; }
        public String cp_pagetitle { get; set; }
        public String cp_pagecontent { get; set; }
        public String cp_pagemeta { get; set; }
        public String cp_pagemetadescription { get; set; }
        public Boolean cp_isdefault { get; set; }
        public Boolean cp_isdisplay { get; set; }
    }
}