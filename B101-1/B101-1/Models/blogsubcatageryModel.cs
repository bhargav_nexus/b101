﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_blogsubcatageryMaster"), PetaPoco.PrimaryKey("bsc_id")]
    public class blogsubcatageryModel
    {
        public Int64 bsc_id { get; set; }
        public Int64 bsc_parentcat { get; set; }
        public String bsc_subcatname { get; set; }
        
        [PetaPoco.ResultColumn]
        public String catageryname { get; set; }
    }
}