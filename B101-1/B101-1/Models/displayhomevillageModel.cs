﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_DIsplayHomeVillageMaster"), PetaPoco.PrimaryKey("hvm_id")]
    public class displayhomevillageModel
    {
        public Int64 hvm_id {get;set;}
        public String hvm_name {get;set;}
        public Int64 hvm_image {get;set;}
        public String hvm_address {get;set;}
        public String hvm_externalurl {get;set;}
        public String hvm_shortdescription { get; set; }
        public String hvm_description {get;set;}
        public String hvm_title {get;set;}
        public String hvm_metakey {get;set;}
        public String hvm_meatdescription {get;set;}
        public String hvm_latlang { get; set; }
        public Boolean hvm_status { get; set; }
        [PetaPoco.ResultColumn]
        public String  ImageName { get; set; }
    }
}