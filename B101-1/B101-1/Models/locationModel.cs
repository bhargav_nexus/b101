﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_locationMaster"), PetaPoco.PrimaryKey("lct_id")]
    public class locationModel
    {
        public Int64 lct_id { get; set; }
        public String lct_location { get; set; }
        public Boolean lct_status { get; set; }
    }
}