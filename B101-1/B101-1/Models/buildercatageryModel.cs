﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_BuildercatageryMaster"), PetaPoco.PrimaryKey("bcat_id")]
    public class buildercatageryModel
    {
        public Int64 bcat_id {get;set;}
        public String bcat_catagery {get;set;}
        public Int64 bcat_parent { get; set; }
    }
}