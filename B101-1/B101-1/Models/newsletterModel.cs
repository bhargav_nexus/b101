﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_newslatterSubscriber"), PetaPoco.PrimaryKey("ns_id")]
    public class newsletterModel
    {
        public Int64 ns_id { get; set; }
        public String ns_name { get; set; }
        public String ns_email { get; set; }
        public Boolean ns_status { get; set; }
    }
}