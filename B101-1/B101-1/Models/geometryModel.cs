﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_propertyGeometryMaster"), PetaPoco.PrimaryKey("geo_id")]
    public class geometryModel
    {
        public int geo_id { get; set; }
        public decimal geo_latitude { get; set; }
        public decimal geo_longitude { get; set; }
    }
}