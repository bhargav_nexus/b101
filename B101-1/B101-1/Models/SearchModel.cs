﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.Models
{
    public class SearchModel
    {
        public String FilterString { get; set; }
        public Int64 TotalRecord { get; set; }
        public Int64 RecordperPage { get; set; }
        public Int64 CurrentPage { get; set; }
        public Int64 TotalPage { get; set; }
        public Int64 ps { get; set; }
        public Int64 pe { get; set; }
    }
}