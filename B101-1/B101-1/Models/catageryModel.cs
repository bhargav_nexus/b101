﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_productcatagery"), PetaPoco.PrimaryKey("prdct_catageryid")]
    public class catageryModel
    {
        public Int64 prdct_catageryid { get; set; }
        public String prdct_catagery { get; set; }
        public Int64 prdct_parentid { get; set; }
        public String prdct_fulcatagery { get; set; }
        public Boolean prdct_status { get; set; }
    }
}