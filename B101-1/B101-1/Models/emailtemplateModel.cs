﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_emailtemplateMaster"), PetaPoco.PrimaryKey("et_id")]
    public class emailtemplateModel
    {
        public Int64 et_id { get; set; }
        public Int64 et_eaid { get; set; }
        public String et_name { get; set; }
        public String et_subject { get; set; }
        public String et_emailbody { get; set; }
        public String et_bccemail { get; set; }
        public String et_ccemail { get; set; }
        public Boolean et_status { get; set; }
        [ResultColumn]
        public emailaccountModel emaccount { get; set; }
    }
}