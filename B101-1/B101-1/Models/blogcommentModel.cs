﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_blogcomment"), PetaPoco.PrimaryKey("cmt_id")]
    public class blogcommentModel
    {
        public Int64 cmt_id { get; set; }
        public Int64 cmt_blog_id { get; set; }
        public String cmt_name { get; set; }
        public String cmt_email { get; set; }
        public String cmt_text { get; set; }
        public DateTime cmt_datetime { get; set; }
        public Boolean cmt_aprvsts { get; set; }
        [ResultColumn]
        public String blogname { get; set; }
    }
}