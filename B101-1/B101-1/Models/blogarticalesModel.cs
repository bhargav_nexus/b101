﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_blogarticales"), PetaPoco.PrimaryKey("ba_id")]
    public class blogArticlesModel
    {
        public Int64 ba_id { get; set; }
        public Guid ba_author { get; set; }
        public Int64 ba_catagery { get; set; }
        public String ba_title { get; set; }
        public String ba_content { get; set; }
        public Int64 ba_mainimage { get; set; }
        public DateTime ba_createddate { get; set; }
        public String ba_shortcontent { get; set; }
        public Boolean ba_status { get; set; }
        [PetaPoco.ResultColumn]
        public String UserName { get; set; }
        [PetaPoco.ResultColumn]
        public String BlogTags { get; set; }
        [PetaPoco.ResultColumn]
        public HttpPostedFileBase mainimage { get; set; }
        [PetaPoco.ResultColumn]
        public String image { get; set; }
    }
}