﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_LandPropertyMaster"), PetaPoco.PrimaryKey("lp_id")]
    public class LandPropertyModel
    {
        public Int64 lp_id { get; set; }
        public String lp_name { get; set; }
        public Int64 lp_image { get; set; }
        public String lp_address { get; set; }
        public String lp_latlang { get; set; }
        public String lp_externalurl { get; set; }
        public String lp_shortdescription { get; set; }
        public String lp_description { get; set; }
        public String lp_title { get; set; }
        public String lp_metakey { get; set; }
        public String lp_meatdescription { get; set; }
        public Boolean lp_status { get; set; }
        [PetaPoco.ResultColumn]
        public String ImageName { get; set; }
    }
}