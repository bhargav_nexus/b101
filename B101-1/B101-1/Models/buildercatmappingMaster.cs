﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace B101_1.Models
{
    [PetaPoco.TableName("tbl_BuilderCatageryMapping"), PetaPoco.PrimaryKey("bcm_id")]
    public class buildercatmappingMaster
    {
        public Int64 bcm_id { get; set; }
        public Int64 bcm_builderid { get; set; }
        public Int64 bcm_catageryid { get; set; }
        public Boolean bcm_isparent { get; set; }
    }
}