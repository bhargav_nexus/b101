﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class FeatureRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveFeature(featureModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_featureMaster", "fm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Feature";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<featureModel> ListFeature()
        {
            List<featureModel> locations = new List<featureModel>();
            Sql query = Sql.Builder.Append("");
            locations = Connection.Fetch<featureModel>(query);
            return locations;
        }

        public List<string> ListPropFeature(Int64 hmid)
        {
            List<string> features = new List<string>();
            Sql query = Sql.Builder.Append("select fm_feature from tbl_featureMaster where fm_id in (select hfm_ftrid from tbl_homefeturesMapping where hfm_homeid=@0)",hmid);
            features = Connection.Fetch<string>(query);
            return features;
        }

        public Boolean DeleteFeature(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<featureModel>("Where fm_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Feature";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}