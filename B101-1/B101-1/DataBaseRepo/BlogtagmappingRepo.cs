﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class BlogtagmappingRepo :BaseConnection
    {

        public string GetSetError { get; set; }
        public Boolean SaveTagMapping(blogtagmappingModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_blogtagmapping", "btm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding TagMapping";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<tagModel> ListTagMapping(Int64 artid)
        {
            List<tagModel> tags = new List<tagModel>();
            Sql query = Sql.Builder.Append("select bt_id,bt_tag from tbl_blogtagmapping inner join tbl_tagMaster on btm_tagid=bt_id where btm_blgid=@0", artid);
            tags = Connection.Fetch<tagModel>(query);
            return tags;
        }
        public List<String> SelectedTaglist(Int64 artid)
        {
            List<String> tags = new List<String>();
            Sql query = Sql.Builder.Append("select btm_tagid from tbl_blogtagmapping where btm_blgid=@0", artid);
            tags = Connection.Fetch<String>(query);
            return tags;
        }
        public List<String> SelectedTag(Int64 artid)
        {
            List<String> tags = new List<String>();
            Sql query = Sql.Builder.Append("select bt_tag from tbl_blogtagmapping");
            query.Append("inner join tbl_tagMaster on tbl_blogtagmapping .btm_tagid=tbl_tagMaster.bt_id");
            query.Append("where btm_blgid=@0",artid);
            tags = Connection.Fetch<String>(query);
            return tags;
        }

        public Boolean DeleteTagMapping(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<blogtagmappingModel>("Where btm_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Deleting TagMapping";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean DeleteTagMappingByArticle(Int64 artid)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<blogtagmappingModel>("Where btm_blgid=@0", artid);
            if (Id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Deleting TagMapping";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}