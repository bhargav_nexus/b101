﻿using B101_1.Models;
using B101_1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.ViewModels;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class PrpHomeRepo : BaseConnection
    {
        public string GetSetError { get; set; }

        #region masterdata

        public List<DataBindModel> GetBathroom()
        {
            List<DataBindModel> baths = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bam_id as datavalue,bam_bathroom as datatext from tbl_bathMaster");
            baths = data.ToList<DataBindModel>();
            return baths;
        }

        public List<DataBindModel> GetBedRoom()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bem_id as datavalue,bem_bedroom as datatext from tbl_bedMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public List<DataBindModel> GetType()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select prptype_id as datavalue,prptype_name as datatext from tbl_hometypeMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public List<DataBindModel> GetStorey()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select strm_id as datavalue,strm_storeys as datatext from tbl_storeysMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }
        public string GetStoreyById(long id)
        {
            var data = Connection.Single<String>("select strm_storeys as datatext from tbl_storeysMaster where strm_id=@0", id);
            return data.ToString();
        }

        public List<DataBindModel> GetBuilder()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bm_id as datavalue,bm_name as datatext from tbl_builderMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public List<DataBindModel> GetLocation()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select lct_id as datavalue,lct_location as datatext from tbl_locationMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public string GetLocationById(long id)
        {
            var data = Connection.Single<String>("select lct_location as datatext from tbl_locationMaster where lct_id =@0", id);
            return data.ToString();
        }
        public List<DataBindModel> GetFeatures()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select fm_id as datavalue,fm_feature as datatext from tbl_featureMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }
        public string GetFeaturesById(string id)
        {
            string ftrs = "";
            List<String> items = new List<String>();
            items = Connection.Fetch<String>("select fm_feature from tbl_featureMaster where fm_id in (" + id + ")");
            foreach (String s in items)
            {
                ftrs += s.ToString() + " ,";
            }
            return ftrs.TrimEnd(','); ;
        }
        public List<DataBindModel> GetPriceRange()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bm_id as datavalue,'$ ' +(convert(varchar(30), bm_frmamnt, 1) + '-' +'$ '+convert(varchar(30), bm_toamnt, 1)) as datatext from tbl_budgetMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public List<DataBindModel> GetVIllages()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select hvm_id as datavalue,hvm_name as datatext from tbl_displayhomevillageMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }
        public string GetVIllagesById(long id)
        {

            var data = Connection.Single<String>("select hvm_name from tbl_displayhomevillageMaster where hvm_id =@0", id);

            return data.ToString();
        }

        #endregion

        public Boolean SaveHome(prpHomeModel hmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_homeMaster", "hm_id", hmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                GetSetError = inserted.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Home";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean UpdateHome(prpHomeModel hmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_homeMaster", "hm_id", hmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updating Home";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean DeleteHome(Int64 id)
        {
            Boolean isdel = false;
            int dltid = Connection.Delete<prpHomeModel>("Where hm_id=@0", id);
            if (dltid > 0)
            {
                isdel = true;
            }
            else
            {
                isdel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return isdel;
        }
        public List<prpHomeModel> ListHomes()
        {
            List<prpHomeModel> list = new List<prpHomeModel>();
            Sql query = Sql.Builder.Append("select tbl_homeMaster.*,tbl_builderMaster.bm_name from tbl_homeMaster");
            query.Append("inner join tbl_builderMaster on hm_builder=bm_id");
            list = Connection.Fetch<prpHomeModel>(query);
            return list;
        }
        public List<prpHomeModel> ListHomesByBuilder(Guid Bldrid)
        {
            List<prpHomeModel> list = new List<prpHomeModel>();
            Sql query = Sql.Builder.Append("select tbl_homeMaster.*,tbl_builderMaster.bm_name from tbl_homeMaster");
            query.Append("inner join tbl_builderMaster on hm_builder=bm_id");
            query.Append("where bm_userid=@0", Bldrid);
            list = Connection.Fetch<prpHomeModel>(query);
            return list;
        }
        public prpHomeModel GetHome(Int64 hmid)
        {
            prpHomeModel home = new prpHomeModel();
            Sql query = Sql.Builder.Append("select tbl_homeMaster.*,tbl_builderMaster.bm_name from tbl_homeMaster");
            query.Append("inner join tbl_builderMaster on hm_builder=bm_id");
            query.Append("where hm_id=@0", hmid);
            home = Connection.SingleOrDefault<prpHomeModel>(query);
            return home;
        }
        public List<PrpHomeViewModel> SearchHome(string Fq)
        {
            List<PrpHomeViewModel> proplist = new List<PrpHomeViewModel>();
            //("select tbl_homeMaster.*,mi.img_savedname as mainimage , mf.img_savedname as floreplanimage,bm_name as hm_builder from tbl_homeMaster");
            Sql query = Sql.Builder.Append("select distinct(hm_id), tbl_homeMaster.*,mi.img_savedname as mainimage , mf.img_savedname as floreplanimage,bm_name as hm_builder from tbl_homeMaster");
            query.Append("inner join tbl_imageMaster mi on tbl_homeMaster.hm_mainimage=mi.img_id");
            query.Append("inner join tbl_imageMaster mf on tbl_homeMaster.hm_florplanimage=mf.img_id");
            query.Append("inner join tbl_builderMaster bm on tbl_homeMaster.hm_builder=bm.bm_id");
            query.Append("inner join tbl_homefeturesMapping on tbl_homeMaster.hm_id=tbl_homefeturesMapping.hfm_homeid");
            if (Fq != string.Empty)
            {
                query.Append("where " + Fq);
            }
            var data = Connection.Query<PrpHomeViewModel>(query);
            proplist = data.ToList<PrpHomeViewModel>();
            return proplist;
        }
        public List<PrpHomeViewModel> SearchHomePaged(long page, string Fq)
        {
            int pagesize = 3;
            List<PrpHomeViewModel> proplist = new List<PrpHomeViewModel>();
            Sql query = Sql.Builder.Append("select ROW_NUMBER() OVER (ORDER BY hm_id) AS ROWNUM,tbl_homeMaster.*,mi.img_savedname as mainimage , mf.img_savedname as floreplanimage ,bm_name as hmbuilder,bi.img_savedname as hmbuilderlogo from tbl_homeMaster");
            query.Append("inner join tbl_imageMaster mi on tbl_homeMaster.hm_mainimage=mi.img_id");
            query.Append("inner join tbl_imageMaster mf on tbl_homeMaster.hm_florplanimage=mf.img_id");
            query.Append("left join tbl_builderMaster bm on tbl_homeMaster.hm_builder=bm.bm_id");
            query.Append("left join tbl_imageMaster bi on bm.bm_headerimage=bi.img_id");
            query.Append("left join tbl_regionMaster rm on bm.bm_region=rm.rm_id");
            if (Fq != string.Empty)
            {
                query.Append("where " + Fq);
            }
            var data = Connection.Page<PrpHomeViewModel>(page, pagesize, query);
            proplist = data.Items.ToList<PrpHomeViewModel>();

            // managing searchng string and pager
            SearchModel sm = new SearchModel();
            sm.FilterString = Fq;
            sm.TotalRecord = Convert.ToInt64(data.TotalItems);
            sm.TotalPage = Convert.ToInt64(data.TotalPages);
            sm.RecordperPage = Convert.ToInt64(data.ItemsPerPage);
            sm.CurrentPage = Convert.ToInt64(data.CurrentPage);

            if (data.Items.Count != 0)
            {
                sm.ps = (((page - 1) * pagesize)) + 1;
            }

            if (sm.RecordperPage > data.Items.Count)
            {
                sm.pe = (page - 1 * pagesize) + (sm.TotalRecord - (page - 1 * pagesize));
            }
            else
            {
                sm.pe = page * pagesize;
            }
            MySession.Current.searchresult = sm;
            return proplist;
        }
        public PrpHomeViewModel SearchHomeByid(Int64 hmid)
        {
            PrpHomeViewModel property = new PrpHomeViewModel();
            Sql query = Sql.Builder.Append("select tbl_homeMaster.*,mi.img_savedname as mainimage , mf.img_savedname as floreplanimage ,bm_name as hmbuilder,bi.img_savedname as hmbuilderlogo from tbl_homeMaster");
            query.Append("inner join tbl_imageMaster mi on tbl_homeMaster.hm_mainimage=mi.img_id");
            query.Append("inner join tbl_imageMaster mf on tbl_homeMaster.hm_florplanimage=mf.img_id");
            query.Append("inner join tbl_builderMaster bm on tbl_homeMaster.hm_builder=bm.bm_id");
            query.Append("inner join tbl_imageMaster bi on bm.bm_headerimage=bi.img_id");
            query.Append("where hm_id=@0", hmid);
            property = Connection.SingleOrDefault<PrpHomeViewModel>(query);
            return property;
        }
        public PrpHomeViewModel ViewHome(Int64 hmid)
        {
            PrpHomeViewModel hmview = new PrpHomeViewModel();
            Sql query = Sql.Builder.Append("select *,mi.img_savedname as mainimage , mf.img_savedname as floreplanimage from tbl_homeMaster");
            query.Append("inner join tbl_imageMaster mi on tbl_homeMaster.hm_mainimage=mi.img_id");
            query.Append("inner join tbl_imageMaster mf on tbl_homeMaster.hm_florplanimage=mf.img_id");
            query.Append("where hm_id=@0", hmid);
            var data = Connection.SingleOrDefault<PrpHomeViewModel>(query);
            hmview = data;
            return hmview;
        }
    }
}