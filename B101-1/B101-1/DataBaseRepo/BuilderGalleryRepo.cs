﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class BuilderGalleryRepo :BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveBuilderImage(builderGalleryModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_buildergallery", "bg_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Gallery Image";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<builderGalleryModel> ListGalleryByBuilder(Int64 bid)
        {
            List<builderGalleryModel> images = new List<builderGalleryModel>();
            Sql query = Sql.Builder.Append("select tbl_buildergallery.*,tbl_imageMaster.img_savedname as ImagePath from tbl_buildergallery");
            query.Append("inner join tbl_imageMaster on bg_imageid=img_id");
            query.Append("where bg_buildid=@0", bid);
            query.Append("order by bg_displayorder asc");
            images = Connection.Fetch<builderGalleryModel>(query);
            return images;
        }
        public Boolean DeleteBuilderImage(Int64 hmimg)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<builderGalleryModel>("Where bg_imageid=@0", hmimg);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return IsDel;
        }
        public Boolean DeleteBuilderGlry(Int64 hmid)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<homegalleryModel>("Where bg_id=@0", hmid);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return IsDel;
        }
        public Boolean Arrange(Int32 cdo, Int32 ndo, Int64 Imageid, Int64 bid)
        {
            Boolean BlnRslt = false;
            Sql query = Sql.Builder.Append("update tbl_buildergallery set bg_displayorder=" + ndo + " where bg_imageid=" + Imageid + " and bg_buildid=" + bid);
            var rslt = Connection.Execute(query);
            query = Sql.Builder.Append("update tbl_buildergallery set bg_displayorder=" + cdo + " where bg_imageid !=" + Imageid + " and bg_displayorder=" + ndo + " and bg_buildid=" + bid);
            rslt = Connection.Execute(query);
            BlnRslt = true;
            return BlnRslt;
        }
        public Int64 getdisplord(Int64 hmid)
        {
            Int64 disord = 0;
            try
            {
                disord = Connection.Single<Int64>("select max(bg_displayorder) from tbl_buildergallery where bg_buildid=@0", hmid);
            }
            catch (Exception ex)
            {
                disord = 0;
            }

            return disord = disord + 1;
        }
    }
}