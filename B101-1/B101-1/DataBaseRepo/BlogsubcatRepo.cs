﻿using B101_1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class BlogsubcatRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public List<DataBindModel> Bindblgsubcatagery()
        {
            List<DataBindModel> catlist = new List<DataBindModel>();
            try
            {
                var data = Connection.Query<DataBindModel>("select bsc_id as datavalue,bsc_subcatname as datatext from tbl_blogsubcatageryMaster");
                catlist = data.ToList<DataBindModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                catlist = null;
            }
            return catlist;
        }
        public Boolean SaveCatagery(blogsubcatageryModel catmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_blogsubcatageryMaster", "bsc_id", catmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                GetSetError = inserted.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<blogsubcatageryModel> ReadCatagery()
        {
            List<blogsubcatageryModel> catlst = new List<blogsubcatageryModel>();
            try
            {
                var data = Connection.Query<blogsubcatageryModel>("select tbl_blogsubcatageryMaster.*,tbl_blogcatageryMaster.bc_catagery as catageryname from tbl_blogsubcatageryMaster inner join tbl_blogcatageryMaster on bsc_parentcat=bc_id");
                catlst = data.ToList<blogsubcatageryModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                catlst = null;
            }
            return catlst;
        }
        public Boolean DeleteByCatagery(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<blogsubcatageryModel>("Where bsc_id = @0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<DataBindModel> BindblgsubcatageryByParent(Int64 prntid)
        {
            List<DataBindModel> catlist = new List<DataBindModel>();
            try
            {
                var data = Connection.Query<DataBindModel>("select bsc_id as datavalue,bsc_subcatname as datatext from tbl_blogsubcatageryMaster where bsc_parentcat=@0", prntid);
                catlist = data.ToList<DataBindModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                catlist = null;
            }
            return catlist;
        }
    }
}