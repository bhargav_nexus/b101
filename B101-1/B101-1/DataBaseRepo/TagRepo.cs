﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class TagRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveTag(tagModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_tagMaster", "bt_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Tag";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<tagModel> ListTag()
        {
            List<tagModel> tags = new List<tagModel>();
            Sql query = Sql.Builder.Append("");
            tags = Connection.Fetch<tagModel>(query);
            return tags;
        }
        public Boolean DeleteTag(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<tagModel>("Where bt_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Tag";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}