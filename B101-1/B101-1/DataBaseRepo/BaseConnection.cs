﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.DataBaseRepo
{
    public class BaseConnection
    {
        private Database _Connection;
        public Database Connection
        {
            get
            {
                if (_Connection == null)
                {
                    _Connection = new Database("DefaultConnection");
                }
                return _Connection;
            }
        }
    }
}