﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class AttributeRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public List<DataBindModel> BindAttributes()
        {
            List<DataBindModel> catlist = new List<DataBindModel>();
            try
            {
                var data = Connection.Query<DataBindModel>("select attr_id as datavalue,attr_attribute as datatext from tbl_attributeMaster where attr_status=1");
                catlist = data.ToList<DataBindModel>();
            }
            catch(Exception ex)
            {
                GetSetError = ex.Message;
                catlist = null;
            }
            return catlist;
        }
        public Boolean SaveAttribute(attributeModel attrmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_attributeMaster", "attr_id", attrmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Attribute";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<attributeModel> ReadallAtributes()
        {
            List<attributeModel> attrlst = new List<attributeModel>();
            try
            {
                var data = Connection.Query<attributeModel>("select * from tbl_attributeMaster");
                attrlst = data.ToList<attributeModel>();
            }
            catch(Exception ex)
            {
                GetSetError = ex.Message;
                attrlst = null;
            }
            return attrlst;
        }

        #region AttributeValue

        public Boolean SaveAttributeValue(attributevalueModel attrmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_attributeValueMaster", "atrval_id", attrmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Attribute";
                blnRslt = false;
            }
            return blnRslt;
        }

        public List<attributevalueModel> ReadValuebyAttribute(Int64 attrid)
        {
            List<attributevalueModel> vallist = new List<attributevalueModel>();
            var data = Connection.Query<attributevalueModel>("select * from tbl_attributeValueMaster where atrval_attrid=@0", attrid);
            vallist = data.ToList<attributevalueModel>();
            return vallist;
        }

        public Boolean DeleteAttributeValue(Int64 attid)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<attributevalueModel>("Where atrval_id=@0", attid);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Value Delete";
            }
            return IsDel;
        }

        #endregion
    }
}