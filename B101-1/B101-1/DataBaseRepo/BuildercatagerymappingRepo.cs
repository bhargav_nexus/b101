﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class BuildercatagerymappingRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean Save(buildercatmappingMaster mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_BuilderCatageryMapping", "bcm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add BuildingCatageryMapping";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean DeleteMaping(Int64 builderid)
        {
            Boolean blnRslt = false;
            var del = Connection.Execute("delete from tbl_BuilderCatageryMapping where bcm_builderid=@0", builderid);
            if (Convert.ToInt64(del) > 0)
            {
                blnRslt = true;
            }
            return blnRslt;
        }
        public List<buildercatageryModel> ListCatagery()
        {
            List<buildercatageryModel> cataeries = new List<buildercatageryModel>();
            Sql query = Sql.Builder.Append("select * from tbl_BuildercatageryMaster where bcat_parent !=0");
            cataeries = Connection.Fetch<buildercatageryModel>(query);
            return cataeries;
        }
        public Boolean DeleteBuilderCatagery(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<buildercatageryModel>("Where bcat_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Int64 GetBuildercatagery(Int64 builderid)
        {
            Int64 cat = 0;
            var c = Connection.SingleOrDefault<buildercatmappingMaster>("select * from tbl_BuilderCatageryMapping where bcm_builderid=@0 and bcm_isparent ='true'", builderid);
            cat = c.bcm_catageryid;
            return cat;
        }

        public String Buildercatagery(Int64 builderid)
        {
            String cat = string.Empty;
            Sql sql = Sql.Builder.Append("select bcat_catagery from tbl_BuilderCatageryMapping");
            sql.Append("inner join tbl_buildercatageryMaster on tbl_BuilderCatageryMapping.bcm_catageryid=tbl_buildercatageryMaster.bcat_id");
            sql.Append("where bcm_builderid=@0 and bcm_isparent ='true'",builderid);
            var c = Connection.SingleOrDefault<String>(sql);
            cat = c;
            return cat;
        }

        public Int64 GetBuildersubcatagery(Int64 builderid)
        {
            Int64 cat = 0;
            var c = Connection.SingleOrDefault<buildercatmappingMaster>("select * from tbl_BuilderCatageryMapping where bcm_builderid=@0 and bcm_isparent ='false'", builderid);
            if (c != null)
            {
                cat = c.bcm_catageryid;
            }
            return cat;
        }
    }
}