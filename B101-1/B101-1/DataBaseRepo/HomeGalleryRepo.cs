﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class HomeGalleryRepo : BaseConnection
    {
         public string GetSetError { get; set; }
        public Boolean SaveHomeImage(homegalleryModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_homegallery", "hg_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Budget";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<homegalleryModel> ListGalleryByHome(Int64 hmid)
        {
            List<homegalleryModel> images = new List<homegalleryModel>();
            Sql query = Sql.Builder.Append("select tbl_homegallery.*,tbl_imageMaster.img_savedname as ImagePath from tbl_homegallery");
            query.Append("inner join tbl_imageMaster on hg_imageid=img_id");
            query.Append("where hg_hmid=@0",hmid);
            query.Append("order by hg_displayorder asc");
            images = Connection.Fetch<homegalleryModel>(query);
            return images;
        }
        public Boolean DeleteHomeImage(Int64 hmimg)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<homegalleryModel>("Where hg_imageid=@0", hmimg);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return IsDel;
        }
        public Boolean DeleteHomeGlry(Int64 hmid)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<homegalleryModel>("Where hg_hmid=@0", hmid);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return IsDel;
        }
        public Boolean Arrange(Int32 cdo, Int32 ndo, Int64 Imageid, Int64 hmid)
        {
            Boolean BlnRslt = false;
            Sql query = Sql.Builder.Append("update tbl_homegallery set hg_displayorder=" + ndo + " where hg_imageid=" + Imageid + " and hg_hmid=" + hmid);
            var rslt = Connection.Execute(query);
            query = Sql.Builder.Append("update tbl_homegallery set hg_displayorder=" + cdo + " where hg_imageid !=" + Imageid + " and hg_displayorder=" + ndo + " and hg_hmid=" + hmid);
            rslt = Connection.Execute(query);
            BlnRslt = true;
            return BlnRslt;
        }
        public Int64 getdisplord(Int64 hmid)
        {
            Int64 disord = 0;
            try
            {
                disord = Connection.Single<Int64>("select max(hg_displayorder) from tbl_homegallery where hg_hmid=@0", hmid);
            }
            catch(Exception ex)
            {
                disord = 0;
            }

            return disord=disord+1;
        }
    }
}