﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class BuilderRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveBuilder(builderModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_BuilderMaster", "bm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(inserted);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Budget";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean EditBuilder(builderModel mdl)
        {
            Boolean blnRslt = false;
            var Updated = 0 ;
            try
            {
                Updated = Connection.Update("tbl_BuilderMaster", "bm_id", mdl);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            if (Convert.ToInt32(Updated) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(Updated);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updtae Builder";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<builderModel> ListBuilder()
        {
            List<builderModel> builders = new List<builderModel>();
            Sql query = Sql.Builder.Append("select tbl_BuilderMaster.*,tbl_BuildercatageryMaster.bcat_catagery as bm_catagery ,Memberships.IsApproved as bm_isapproved from tbl_BuilderMaster");
            query.Append("inner join tbl_BuilderCatageryMapping on tbl_BuilderMaster.bm_id=tbl_BuilderCatageryMapping.bcm_builderid");
            query.Append("inner join tbl_BuildercatageryMaster on tbl_BuilderCatageryMapping.bcm_catageryid=tbl_BuildercatageryMaster.bcat_id");
            query.Append("inner join Memberships on tbl_BuilderMaster.bm_userid = Memberships.UserId");
            query.Append("where tbl_BuilderMaster.bm_isapproved = 'true' and tbl_BuilderCatageryMapping.bcm_isparent='true'");
            builders = Connection.Fetch<builderModel>(query);
            return builders;
        }
        public List<builderModel> ListDeveloper()
        {
            String region = MySession.Current.geolocationinfo.region;
            String country = MySession.Current.geolocationinfo.country;

            List<builderModel> builders = new List<builderModel>();
            Sql query = Sql.Builder.Append("select tbl_BuilderMaster.*,tbl_BuildercatageryMaster.bcat_catagery as bm_catagery ,Memberships.IsApproved as bm_isapproved,tbl_imageMaster.img_savedname as bmheaderimage from tbl_BuilderMaster");
            query.Append("inner join tbl_BuilderCatageryMapping on tbl_BuilderMaster.bm_id=tbl_BuilderCatageryMapping.bcm_builderid");
            query.Append("inner join tbl_BuildercatageryMaster on tbl_BuilderCatageryMapping.bcm_catageryid=tbl_BuildercatageryMaster.bcat_id");
            query.Append("inner join Memberships on tbl_BuilderMaster.bm_userid = Memberships.UserId");
            query.Append("inner join tbl_regionMaster on tbl_BuilderMaster.bm_region = rm_id");
            query.Append("left join tbl_imageMaster on bm_headerimage=img_id");
            if (country.ToLower() != "australia")
            {
                query.Append("where tbl_BuilderMaster.bm_isapproved = 'true' and tbl_BuilderCatageryMapping.bcm_isparent='true' and tbl_BuildercatageryMaster.bcat_catagery='Property Developers Pearth'");
            }
            else
            {
                query.Append("where tbl_BuilderMaster.bm_isapproved = 'true' and tbl_BuilderCatageryMapping.bcm_isparent='true' and tbl_BuildercatageryMaster.bcat_catagery='Property Developers Pearth' and rm_region=@0", region);
            }
            builders = Connection.Fetch<builderModel>(query);
            return builders;
        }

        public List<builderModel> ListNewHomeDeveloper()
        {
            List<builderModel> builders = new List<builderModel>();
            Sql query = Sql.Builder.Append("select tbl_BuilderMaster.*,tbl_BuildercatageryMaster.bcat_catagery as bm_catagery ,Memberships.IsApproved as bm_isapproved,tbl_imageMaster.img_savedname as bmheaderimage from tbl_BuilderMaster");
            query.Append("inner join tbl_BuilderCatageryMapping on tbl_BuilderMaster.bm_id=tbl_BuilderCatageryMapping.bcm_builderid");
            query.Append("inner join tbl_BuildercatageryMaster on tbl_BuilderCatageryMapping.bcm_catageryid=tbl_BuildercatageryMaster.bcat_id");
            query.Append("inner join Memberships on tbl_BuilderMaster.bm_userid = Memberships.UserId");
            query.Append("left join tbl_imageMaster on bm_headerimage=img_id");
            query.Append("where tbl_BuilderMaster.bm_isapproved = 'true' and tbl_BuilderCatageryMapping.bcm_isparent='true' and tbl_BuildercatageryMaster.bcat_catagery='New Home Builders'");
            builders = Connection.Fetch<builderModel>(query);
            return builders;
        }

        public List<builderModel> ListCommercial()
        {
            String region = MySession.Current.geolocationinfo.region;
            String country = MySession.Current.geolocationinfo.country;

            List<builderModel> builders = new List<builderModel>();

            Sql query = Sql.Builder.Append("select tbl_BuilderMaster.*,tbl_BuildercatageryMaster.bcat_catagery as bm_catagery ,Memberships.IsApproved as bm_isapproved,tbl_imageMaster.img_savedname as bmheaderimage from tbl_BuilderMaster");
            query.Append("inner join tbl_BuilderCatageryMapping on tbl_BuilderMaster.bm_id=tbl_BuilderCatageryMapping.bcm_builderid");
            query.Append("inner join tbl_BuildercatageryMaster on tbl_BuilderCatageryMapping.bcm_catageryid=tbl_BuildercatageryMaster.bcat_id");
            query.Append("inner join Memberships on tbl_BuilderMaster.bm_userid = Memberships.UserId");
            query.Append("inner join tbl_regionMaster on tbl_BuilderMaster.bm_region = rm_id");
            query.Append("left join tbl_imageMaster on bm_headerimage=img_id");
            if (country.ToLower() != "australia")
            {
                query.Append("where tbl_BuilderMaster.bm_isapproved = 'true' and tbl_BuilderCatageryMapping.bcm_isparent='true' and tbl_BuildercatageryMaster.bcat_catagery='Commercial'");
            }
            else
            {
                query.Append("where tbl_BuilderMaster.bm_isapproved = 'true' and tbl_BuilderCatageryMapping.bcm_isparent='true' and tbl_BuildercatageryMaster.bcat_catagery='Commercial' and rm_region=@0", region);
            }
            builders = Connection.Fetch<builderModel>(query);
            return builders;
        }
        public List<builderModel> ListNewRegisteredBuilder()
        {
            List<builderModel> builders = new List<builderModel>();
            Sql query = Sql.Builder.Append("select tbl_BuilderMaster.*,tbl_BuildercatageryMaster.bcat_catagery as bm_catagery ,Memberships.IsApproved as bm_isapproved from tbl_BuilderMaster");
            query.Append("inner join tbl_BuilderCatageryMapping on tbl_BuilderMaster.bm_id=tbl_BuilderCatageryMapping.bcm_builderid");
            query.Append("inner join tbl_BuildercatageryMaster on tbl_BuilderCatageryMapping.bcm_catageryid=tbl_BuildercatageryMaster.bcat_id");
            query.Append("inner join Memberships on tbl_BuilderMaster.bm_userid = Memberships.UserId");
            query.Append("where tbl_BuilderMaster.bm_isapproved = 'false' and tbl_BuilderCatageryMapping.bcm_isparent='true'");
            builders = Connection.Fetch<builderModel>(query);
            return builders;
        }
        public builderModel GetBuilder(Int64 BldrId)
        {
            builderModel builder = new builderModel();
            Sql query = Sql.Builder.Append("select tbl_BuilderMaster.* from tbl_BuilderMaster where bm_id =@0", BldrId);
            builder = Connection.SingleOrDefault<builderModel>(query);
            return builder;
        }
        public builderModel GetBuilderByUserid(Guid buid)
        {
            builderModel bmdl = new builderModel();
            Sql query = Sql.Builder.Append("select * from tbl_BuilderMaster where bm_userid =@0", buid);
            bmdl = Connection.SingleOrDefault<builderModel>(query);
            return bmdl;
        }
        public Boolean DeleteBuilder(Int64 id)
        {
            Boolean blnRslt = false;
            var imageid = Connection.ExecuteScalar<Int64>("select bm_headerimage from tbl_builderMaster where bm_id=@0",id);
            var Id = Connection.Delete<builderModel>("Where bm_id=@0", id);
            if (id > 0)
            {
                GetSetError = imageid.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Builder";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean ApproveBuilder(Guid builderid)
        {
            Boolean blnRslt = false;
            var aprov = Connection.Execute("update tbl_BuilderMaster set bm_isapproved='true' where bm_userid=@0",builderid);
            if (Convert.ToInt64(aprov) > 0)
            {
                blnRslt = true;
            }
            else
            {
                this.GetSetError = "Unknown Error Occured in Approve Builder ...!!!";
            }
            return blnRslt;
        }
    }
}