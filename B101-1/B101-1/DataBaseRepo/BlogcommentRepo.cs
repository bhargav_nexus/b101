﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class BlogcommentRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveComment(blogcommentModel cmtmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_blogcomment", "cmt_id", cmtmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                GetSetError = inserted.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Articles";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<blogcommentModel> ReadCommnetByBlog(Int64 blgid)
        {
            List<blogcommentModel> cmntlst = new List<blogcommentModel>();
            try
            {
                var data = Connection.Fetch<blogcommentModel>("select * from tbl_blogcomment where cmt_blog_id=@0",blgid);
                cmntlst = data;
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                cmntlst = null;
            }
            return cmntlst;
        }

        public blogcommentModel ReadCommnetById(Int64 cmtid)
        {
            blogcommentModel cmnt = new blogcommentModel();
            try
            {
                var data = Connection.Single<blogcommentModel>("select * from tbl_blogcomment where cmt_id=@0", cmtid);
                cmnt = data;
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                cmnt = null;
            }
            return cmnt;
        }
        public List<blogcommentModel> ReadAllComment()
        {
            List<blogcommentModel> cmntlst = new List<blogcommentModel>();
            try
            {
                var data = Connection.Fetch<blogcommentModel>("select tbl_blogcomment.*,tbl_blogarticales.ba_title as blogname from tbl_blogcomment inner join tbl_blogarticales on tbl_blogcomment.cmt_blog_id = tbl_blogarticales.ba_id");
                cmntlst = data;
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                cmntlst = null;
            }
            return cmntlst;
        }
        public Boolean DeleteComment(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<blogcommentModel>("Where cmt_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}