﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class NewsletterRepo : BaseConnection
    {
        public String GetSetError { get; set; }
        public Boolean SaveSubscriber(newsletterModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_newslatterSubscriber", "ns_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Product";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<newsletterModel> ListSubscriber()
        {
            List<newsletterModel> locations = new List<newsletterModel>();
            Sql query = Sql.Builder.Append("select * from tbl_newslatterSubscriber ");
            locations = Connection.Fetch<newsletterModel>(query);
            return locations;
        }

        public Boolean ChangeStatus(Int64 subid)
        {
            Boolean blnRslt = false;
            var s = Connection.SingleOrDefault<newsletterModel>("select * from tbl_newslatterSubscriber where ns_id=@0",subid);
            if (s.ns_status == true)
            {
                s.ns_status = false;
            }
            else
            {
                s.ns_status = true;
            }
            int rslt = Connection.Update(s);
            if (rslt > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updating Data";
            }
            return blnRslt;
        }
    }
}