﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class LandPropertyRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveLP(LandPropertyModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_LandPropertyMaster", "lp_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Land Property";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean UpdateLP(LandPropertyModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_LandPropertyMaster", "lp_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updtae LandProperty";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<LandPropertyModel> ListLP()
        {
            List<LandPropertyModel> landprprt = new List<LandPropertyModel>();
            Sql query = Sql.Builder.Append("select lp_id,lp_name,lp_image,lp_address,lp_externalurl,lp_shortdescription,lp_description,lp_title,lp_metakey,lp_meatdescription,lp_status,lp_latlang,img_savedname as ImageName from tbl_LandPropertyMaster");
            query.Append("left join tbl_imageMaster on lp_image=img_id");
            landprprt = Connection.Fetch<LandPropertyModel>(query);
            return landprprt;
        }

        public LandPropertyModel GetLPById(Int64 id)
        {
            LandPropertyModel landprpt = new LandPropertyModel();
            Sql query = Sql.Builder.Append("select lp_id,lp_name,lp_image,lp_address,lp_externalurl,lp_shortdescription,lp_description,lp_title,lp_metakey,lp_meatdescription,lp_status,lp_latlang,img_savedname as ImageName from tbl_LandPropertyMaster");
            query.Append("left join tbl_imageMaster on lp_image=img_id");
            query.Append("where lp_id=@0", id);
            landprpt = Connection.SingleOrDefault<LandPropertyModel>(query);
            return landprpt;
        }
        public Boolean DeleteLP(Int64 id)
        {
            Int32 ImgId = Connection.Single<Int32>("select lp_image from tbl_LandPropertyMaster where lp_id=@0", id);
            Boolean blnRslt = false;
            var Id = Connection.Delete<LandPropertyModel>("Where lp_id=@0", id);
            if (id > 0)
            {
                GetSetError = ImgId.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Land Property";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}