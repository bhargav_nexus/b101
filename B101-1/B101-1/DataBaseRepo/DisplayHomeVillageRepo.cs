﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class DisplayHomeVillageRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveDHV(displayhomevillageModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_DIsplayHomeVillageMaster", "hvm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Village";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean UpdateDHV(displayhomevillageModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_DIsplayHomeVillageMaster", "hvm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updtae Village";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<displayhomevillageModel> ListDHV()
        {
            List<displayhomevillageModel> locations = new List<displayhomevillageModel>();
            Sql query = Sql.Builder.Append("select hvm_id,hvm_name,hvm_image,hvm_address,hvm_externalurl,hvm_shortdescription,hvm_description,hvm_title,hvm_metakey,hvm_meatdescription,hvm_status,hvm_latlang,img_savedname as ImageName from tbl_DIsplayHomeVillageMaster");
            query.Append("left join tbl_imageMaster on hvm_image=img_id");
            locations = Connection.Fetch<displayhomevillageModel>(query);
            return locations;
        }

        public displayhomevillageModel GetDHVById(Int64 id)
        {
            displayhomevillageModel locations = new displayhomevillageModel();
            Sql query = Sql.Builder.Append("select hvm_id,hvm_name,hvm_image,hvm_address,hvm_externalurl,hvm_shortdescription,hvm_description,hvm_title,hvm_metakey,hvm_meatdescription,hvm_status,hvm_latlang,img_savedname as ImageName from tbl_DIsplayHomeVillageMaster");
            query.Append("left join tbl_imageMaster on hvm_image=img_id");
            query.Append("where hvm_id=@0",id);
            locations = Connection.SingleOrDefault<displayhomevillageModel>(query);
            return locations;
        }
        public Boolean DeleteDHV(Int64 id)
        {
            Int32 ImgId = Connection.Single<Int32>("select hvm_image from tbl_DIsplayHomeVillageMaster where hvm_id=@0", id);
            Boolean blnRslt = false;
            var Id = Connection.Delete<displayhomevillageModel>("Where hvm_id=@0", id);
            if (id > 0)
            {
                GetSetError = ImgId.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Village";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}