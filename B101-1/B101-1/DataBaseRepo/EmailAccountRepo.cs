﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class EmailAccountRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public emailaccountModel getaccount(Int64 acid)
        {
            emailaccountModel account = new emailaccountModel();
            Sql query = Sql.Builder.Append("select * from tbl_emailaccountMaster where ea_id=@0",acid);
            account = Connection.SingleOrDefault<emailaccountModel>(query);
            return account;
        }
    }
}