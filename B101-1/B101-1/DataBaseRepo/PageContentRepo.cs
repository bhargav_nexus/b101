﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class PageContentRepo : BaseConnection
    {
        public String GetSetError { get; set; }
        public Boolean SavePage(pagecontentModel mdl)
        {
            Boolean blnRslt = false;

            var rec = Connection.ExecuteScalar<Int64>("select count(*) from PageContentMaster where cp_key=@0", mdl.cp_key);
            if (Convert.ToInt64(rec) == 0)
            {
                var inserted = Connection.Insert("PageContentMaster", "cp_id", mdl);
                if (Convert.ToInt32(inserted) > 0)
                {
                    MySession.Current.AddEditId = Convert.ToInt64(inserted);
                    blnRslt = true;
                }
                else
                {
                    GetSetError = "Error in Adding New Page";
                    blnRslt = false;
                }
            }
            else
            {
                GetSetError = "Key Already Exist";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean EditPage(pagecontentModel mdl)
        {
            Boolean blnRslt = false;
            var updated = Connection.Update("PageContentMaster", "cp_id", mdl);
            if (Convert.ToInt32(updated) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(updated);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updating Page";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<pagecontentModel> listpages()
        {
            List<pagecontentModel> pages = new List<pagecontentModel>();
            pages = Connection.Fetch<pagecontentModel>("select * from PageContentMaster");
            return pages;
        }

        public pagecontentModel getpagebykey(String pagekey)
        {
            pagecontentModel page = new pagecontentModel();
            try
            {
                page = Connection.SingleOrDefault<pagecontentModel>("select * from PageContentMaster where cp_key=@0", pagekey);
            }
            catch(Exception ex)
            {
                this.GetSetError = ex.Message;
                page = null;
            }
            return page;
        }
    }
}