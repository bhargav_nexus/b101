﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using B101_1.ViewModels;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class AdditionalContentRepo : BaseConnection
    {
        public string GetSetError { get; set; }

        public Boolean AddContent(additionalContent mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_AdditionalContent", "CntId", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Additional Content";
                blnRslt = false;
            }
            return blnRslt;
        }

        public List<additionalContent> ReadAll()
        {
            List<additionalContent> CntList = new List<additionalContent>();
            CntList = Connection.Fetch<additionalContent>("SELECT * FROM tbl_AdditionalContent");
            return CntList;
        }

        public additionalContent ReadById(int CntId)
        {
            additionalContent cntmdl = new additionalContent();
            cntmdl = Connection.Single<additionalContent>("SELECT * FROM tbl_AdditionalContent WHERE CntId = @0", CntId);
            return cntmdl;
        }

        public additionalContent ReadByKey(string Key)
        {
            additionalContent cntmdl = new additionalContent();
            cntmdl = Connection.Single<additionalContent>("SELECT * FROM tbl_AdditionalContent WHERE CntKey = @0", Key);
            return cntmdl;
        }
        public Boolean UpdateContent(additionalContent cntmdl)
        {
            Boolean blnRslt = false;
            var updated = Connection.Update("tbl_AdditionalContent", "CntId", cntmdl);
            if (Convert.ToInt32(updated) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(updated);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updating Content";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}