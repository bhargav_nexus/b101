﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class BuilderCatRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public List<DataBindModel> BindParentCatagery()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bcat_id as datavalue,bcat_catagery as datatext from tbl_BuildercatageryMaster where bcat_parent=0");
            items = data.ToList<DataBindModel>();
            return items;
        }
        public List<DataBindModel> BindSubCatagery(Int64 id)
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bcat_id as datavalue,bcat_catagery as datatext from tbl_BuildercatageryMaster where bcat_parent="+id);
            items = data.ToList<DataBindModel>();
            return items;
        }
        public Boolean SaveCatagery(buildercatageryModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_BuildercatageryMaster", "bcat_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Building Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<buildercatageryModel> ListCatagery()
        {
            List<buildercatageryModel> cataeries = new List<buildercatageryModel>();
            Sql query = Sql.Builder.Append("select * from tbl_BuildercatageryMaster");
            cataeries = Connection.Fetch<buildercatageryModel>(query);
            return cataeries;
        }
        public Boolean DeleteBCatagery(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<buildercatageryModel>("Where bcat_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}