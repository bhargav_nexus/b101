﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class AdvslotsRepo : BaseConnection
    {
        public string GetSetError { get; set; }

        public Boolean SaveSlot(advslotsModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_advslotMaster", "slt_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(inserted);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Advertisement Slot";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean CheckSlotAvailability(DateTime strtdt,DateTime enddt)
        {
            Boolean blnRslt = false;
            Sql query = Sql.Builder.Append("select * from tbl_advslotMaster where slt_location=6");
            query.Append("and (slt_startdate between '"+strtdt+"' AND '"+enddt+"' or slt_enddate between '"+strtdt+"' AND '"+enddt+"')");
            var r = Connection.Fetch<advslotsModel>(query);
            if(r.Count == 0)
            {
                blnRslt = true;
            }
            return blnRslt;
        }
        public Boolean UpdateSlot(advslotsModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_advslotMaster", "slt_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(inserted);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updating Advertisement Slot";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<advslotsModel> ReadallSlot()
        {
            List<advslotsModel> advslt = new List<advslotsModel>();
            Sql query = Sql.Builder.Append("select tbl_advslotMaster.*,tbl_advspacelocationMaster.asl_location as location from tbl_advslotMaster");
            query.Append("inner join tbl_advspacelocationMaster on tbl_advslotMaster.slt_location= tbl_advspacelocationMaster.asl_id");
            advslt = Connection.Fetch<advslotsModel>(query);
            return advslt;
        }
        public List<advslotsModel> BindSlot()
        {
            List<advslotsModel> advslt = new List<advslotsModel>();
            Sql query = Sql.Builder.Append("select tbl_advslotMaster.*,tbl_advspacelocationMaster.asl_location as location from tbl_advslotMaster");
            query.Append("inner join tbl_advspacelocationMaster on tbl_advslotMaster.slt_location= tbl_advspacelocationMaster.asl_id");
            query.Append("where slt_id not in (select asm_slotid from tbl_AdvSlotMapping)");
            advslt = Connection.Fetch<advslotsModel>(query);
            return advslt;
        }
        public advslotsModel ReadaSlotbyId(Int64 sltid)
        {
            advslotsModel slt = new advslotsModel();
            Sql query = Sql.Builder.Append("select tbl_advslotMaster.*,tbl_advspacelocationMaster.asl_location as location from tbl_advslotMaster");
            query.Append("inner join tbl_advspacelocationMaster on tbl_advslotMaster.slt_location= tbl_advspacelocationMaster.asl_id");
            query.Append("where slt_id=@0", sltid);
            slt = Connection.SingleOrDefault<advslotsModel>(query);
            return slt;
        }
        public Boolean DeleteSlot(Int64 id)
        {
            Boolean isdel = false;
            int dltid = Connection.Delete<advslotsModel>("Where slt_id=@0", id);
            if (dltid > 0)
            {
                isdel = true;
            }
            else
            {
                isdel = false;
                GetSetError = "Error Occured In Slot Delete";
            }
            return isdel;
        }
    }
}