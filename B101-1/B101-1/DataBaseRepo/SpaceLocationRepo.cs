﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class SpaceLocationRepo :BaseConnection
    {
        public List<DataBindModel> BindLocation()
        {
            List<DataBindModel> catlist = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select asl_id as datavalue,asl_location as datatext from tbl_advspacelocationMaster");
            catlist = data.ToList<DataBindModel>();
            return catlist;
        }
    }
}