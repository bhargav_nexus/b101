﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class LocationRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveLocation(locationModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_locationMaster", "lct_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Product";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<locationModel> ListLocation()
        {
            List<locationModel> locations = new List<locationModel>();
            Sql query = Sql.Builder.Append("");
            locations = Connection.Fetch<locationModel>(query);
            return locations;
        }
        public Boolean DeleteLocation(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<locationModel>("Where lct_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Product";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}