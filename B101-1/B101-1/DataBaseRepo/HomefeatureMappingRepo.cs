﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class HomefeatureMappingRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveMapping(homefeatureMappingModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_homefeturesMapping", "hfm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(inserted);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Mapping Feature";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean DeleteMapping(Int64 hmid)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<homefeatureMappingModel>("Where hfm_homeid=@0", hmid);
            if (Id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Mapping";
                blnRslt = false;
            }
            return blnRslt;
        }

        public List<Int64> HomeFeatures(Int64 hmid)
        {
            List<Int64> ftrs = new List<Int64>();
            ftrs = Connection.Fetch<Int64>("select hfm_ftrid from tbl_homefeturesMapping where hfm_homeid=@0", hmid);
            return ftrs;
        }
    }
}