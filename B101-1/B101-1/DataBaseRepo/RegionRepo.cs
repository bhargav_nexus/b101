﻿using B101_1.Models;
using B101_1.ViewModels;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B101_1.DataBaseRepo
{
    public class RegionRepo :BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveRegion(regionModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_regionMaster", "rm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Region";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<regionModel> ListRegion()
        {
            List<regionModel> locations = new List<regionModel>();
            Sql query = Sql.Builder.Append("");
            locations = Connection.Fetch<regionModel>(query);
            return locations;
        }
        public Boolean DeleteRegion(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<regionModel>("Where rm_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Region";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<DataBindModel> GetRegion()
        {
            List<DataBindModel> baths = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select rm_id as datavalue,rm_region as datatext from tbl_regionMaster");
            baths = data.ToList<DataBindModel>();
            return baths;
        }
        public List<DataBindModel> GetCountry()
        {
            List<DataBindModel> baths = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select cm_id as datavalue,cm_countryname as datatext from tbl_countryMaster");
            baths = data.ToList<DataBindModel>();
            return baths;
        }
    }
}