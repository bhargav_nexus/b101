﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using B101_1.ViewModels;
using PetaPoco;

namespace B101_1.DataBaseRepo
{
    public class AdvSlotMappingRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveMapping(advsltmappingModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_AdvSlotMapping", "asm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Mapping";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean DeleteMapping(Int64 id)
        {
            Boolean blnRslt = false;
            var dltid = Connection.Delete<advsltmappingModel>("Where asm_id = @0", id);
            if (dltid > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Deleteing Mapping";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<AdvSlotMappingViewModel> AdvSlotMapping()
        {
            List<AdvSlotMappingViewModel> adsltmappinglist = new List<AdvSlotMappingViewModel>();
            Sql query = Sql.Builder.Append("select asm_id,tbl_advertisementMaster.adv_name,tbl_advertisementMaster.adv_url,tbl_advslotMaster.slt_enddate,tbl_advslotMaster.slt_startdate,asl_location as location from tbl_AdvSlotMapping");
            query.Append("inner join tbl_advertisementMaster on asm_advit=adv_id");
            query.Append("inner join tbl_advslotMaster on asm_slotid=slt_id");
            query.Append("inner join tbl_advspacelocationMaster on slt_location=asl_id");
            adsltmappinglist = Connection.Fetch<AdvSlotMappingViewModel>(query);
            return adsltmappinglist;
        }

        public List<AdvSlotMappingViewModel> ReadDaytoDayAdv()
        {
            List<AdvSlotMappingViewModel> adsltmappinglist = new List<AdvSlotMappingViewModel>();
            Sql query = Sql.Builder.Append("select asm_id,tbl_advertisementMaster.adv_name,tbl_advertisementMaster.adv_url,tbl_advslotMaster.slt_enddate,tbl_advslotMaster.slt_startdate,asl_location as location ,img_savedname as adv_image  from tbl_AdvSlotMapping");
            query.Append("inner join tbl_advertisementMaster on asm_advit=adv_id");
            query.Append("inner join tbl_imageMaster on tbl_advertisementMaster.adv_image=img_id");
            query.Append("inner join tbl_advslotMaster on asm_slotid=slt_id");
            query.Append("inner join tbl_advspacelocationMaster on slt_location=asl_id");
            adsltmappinglist = Connection.Fetch<AdvSlotMappingViewModel>(query);
            return adsltmappinglist;
        }
    }
}