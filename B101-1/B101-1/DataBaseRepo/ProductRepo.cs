﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class ProductRepo : BaseConnection
    {
        public string GetSetError { get; set; }   
        public Boolean SaveProduct(productModel prdmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_productMaster", "prd_id", prdmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Product";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean UpdateProduct(ProductViewModel prdv)
        {
            Boolean blnRslt = false;
            var prd = Connection.SingleOrDefault<productModel>("WHERE prd_id=@0", prdv.Id);
            prd.prd_description = prdv.Description;
            prd.prd_shortdescription = prdv.ShortDescription;
            prd.prd_productname = prdv.ProductName;
            prd.prd_price = prdv.Price;
            prd.prd_status = prdv.Status;

            int udid = Connection.Update("tbl_productMaster", "prd_id", prd);
            if(udid > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Update Product";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean UpdateProductImage(string flag,string imageid,Int64 productid)
        {
            Boolean isUpdt = false;
            Sql sql = Sql.Builder.Append("update tbl_productMaster set "+flag+" = "+imageid+" where prd_id="+productid);
            var res = Connection.Execute(sql);
            isUpdt = true;
            return isUpdt;
        }
        public List<ProductViewModel> ReadProductByCatagory(Int64 catId)
        {
            List<ProductViewModel> productlist = new List<ProductViewModel>();
            var data = Connection.Query<ProductViewModel>("select tbl_productMaster.*,tbl_productcatagery.prdct_fulcatagery As Catagery from  tbl_productMaster join tbl_productcatagery on prd_catageryid=prdct_catageryid where prd_parentcat=@0", catId);
            productlist = data.ToList<ProductViewModel>();
            return productlist;
        }
        public List<ProductViewModel> AllReadProduct()
        {
            List<ProductViewModel> productlist = new List<ProductViewModel>();
            Sql query = Sql.Builder.Append("select rslt.*,pt.prdct_catagery As Type from");
            query.Append("(select tbl_productMaster.*,pc.prdct_catagery As Catagery");
            query.Append("from  tbl_productMaster");
            query.Append("inner join tbl_productcatagery pc on tbl_productMaster.prd_catageryid = pc.prdct_catageryid) as rslt");
            query.Append("inner join tbl_productcatagery pt on rslt.prd_parentcat = pt.prdct_catageryid");
            var data = Connection.Query<ProductViewModel>(query);
            productlist = data.ToList<ProductViewModel>();
            return productlist;
        }
        public ProductViewModel ReadProductById(Int64 Id)
        {
            ProductViewModel productlist = new ProductViewModel();
            Sql query = Sql.Builder.Append("select rslt.*,pt.prdct_catagery As Type from");
            query.Append("(select tbl_productMaster.*,pc.prdct_catagery As Catagery");
            query.Append("from  tbl_productMaster");
            query.Append("inner join tbl_productcatagery pc on tbl_productMaster.prd_catageryid = pc.prdct_catageryid) as rslt");
            query.Append("inner join tbl_productcatagery pt on rslt.prd_parentcat = pt.prdct_catageryid");
            query.Append("where rslt.prd_id=@0",Id);
            var data = Connection.Single<ProductViewModel>(query);
            productlist = data;
            return productlist;
        }

        #region ProductAttributes
        public Boolean SaveProductAttribute(productattributesModel prdattrmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_productAttributes", "prat_id", prdattrmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Attribute";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Boolean SaveProductAttributeValue(productAttributeValueModel valmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_productAttributesValue", "pav_id", valmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add AttributeValue";
                blnRslt = false;
            }
            return blnRslt;
        }

        public List<ProductAtributeViewModel> ReadAttributeByProduct(Int64 prdctid) 
        {
            List<ProductAtributeViewModel> list = new List<ProductAtributeViewModel>();
            Sql sql = Sql.Builder.Append("select prat_id, attr_attribute As Attribute,atrval_value As Value from tbl_productAttributes");
            sql.Append("left join tbl_attributeMaster on tbl_productAttributes.prat_attrid=tbl_attributeMaster.attr_id");
            sql.Append("left join tbl_attributeValueMaster on tbl_productAttributes.prat_valueid=tbl_attributeValueMaster.atrval_id");
            sql.Append("where atrval_value is not null and tbl_productAttributes.prat_productid=@0", prdctid);
            var data = Connection.Query<ProductAtributeViewModel>(sql);
            list = data.ToList<ProductAtributeViewModel>();

            sql=Sql.Builder.Append("select prat_id,attr_attribute As Attribute,prat_customvalue As Value from tbl_productAttributes");
            sql.Append("left join tbl_attributeMaster on tbl_productAttributes.prat_attrid=tbl_attributeMaster.attr_id where prat_customvalue is not null");
            sql.Append("and tbl_productAttributes.prat_productid=@0", prdctid);
            data = Connection.Query<ProductAtributeViewModel>(sql);
            foreach(ProductAtributeViewModel i in data.ToList<ProductAtributeViewModel>())
            {
                list.Add(i);
            }
            return list;
        }

        public Boolean DeleteAttributeValue(Int64 pratid)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<productattributesModel>("Where prat_id=@0", pratid);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Attribute Delete";
            }
            return IsDel;
        }

        #endregion

        #region ProductImages
        public Boolean SaveProductImage(productimageModel imgmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_productimages", "pi_id", imgmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Image";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<ImageViewModel> ListProductImages(Int64 prdctid)
        {
            List<ImageViewModel> list = new List<ImageViewModel>();
            var data = Connection.Query<ImageViewModel>("select pi_id  ,img_altText as AltText,img_savedname as ImagePath,pi_displayoreder as displayOrder,pi_imageid As ImageId from tbl_productimages join tbl_imageMaster on pi_imageid=img_id where pi_prodid=@0 order by pi_displayoreder asc", prdctid);
            list = data.ToList<ImageViewModel>();
            return list;
        }
        public Boolean DeleteProductImage(Int64 pratid)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<productimageModel>("Where pi_imageid=@0", pratid);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return IsDel;
        }

        public Boolean Arrange(Int32 cdo ,Int32 ndo,Int64 Imageid,Int64 pradid)
        {
            Boolean BlnRslt = false;
            Sql query = Sql.Builder.Append("update tbl_productimages set pi_displayoreder=" + ndo + " where pi_imageid=" + Imageid + " and pi_prodid=" + pradid);
            var rslt = Connection.Execute(query);
            query = Sql.Builder.Append("update tbl_productimages set pi_displayoreder=" + cdo + " where pi_imageid !=" + Imageid + " and pi_displayoreder=" + ndo + " and pi_prodid=" + pradid);
            rslt = Connection.Execute(query);
            BlnRslt = true;
            return BlnRslt;
        }
        #endregion
    }
}