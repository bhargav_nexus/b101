﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class ImageRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveImage(imageMasterModel imgmdl)
        {
            Boolean Issaved = false;
            var inserted = Connection.Insert("tbl_imageMaster", "img_id", imgmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                GetSetError = inserted.ToString();
                Issaved = true;
            }
            else
            {
                GetSetError = "Error in Insert Image";
                Issaved = false;
            }
            return Issaved;
        }
        public Boolean UpdateImage(String ImageName,imageMasterModel imgmdl)
        {
            Boolean IsUpdate = false;
            var img = Connection.SingleOrDefault<imageMasterModel>("WHERE img_savedname=@0", ImageName);
            img.img_savedname = imgmdl.img_savedname;
            img.img_name = imgmdl.img_name;
            img.img_altText = imgmdl.img_altText;
            Connection.Update("tbl_imageMaster", "img_id", img);
            this.GetSetError = img.img_id.ToString();
            IsUpdate = true;
            return IsUpdate;
        }

        public Boolean DeleteImage(Int64 id)
        {
            Boolean blnRslt = false;
            int dltid = Connection.Delete<imageMasterModel>("Where img_id=@0", id);
            if (dltid > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Insert Image";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean DeleteImageByName(String  imgname)
        {
            Boolean blnRslt = false;
            imageMasterModel img = Connection.SingleOrDefault<imageMasterModel>("Where img_savedname=@0", imgname);
            int dltid = Connection.Delete<imageMasterModel>("Where img_savedname=@0", imgname);
            if (dltid > 0)
            {
                GetSetError = img.img_id.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Insert Image";
                blnRslt = false;
            }
            return blnRslt;
        }
        public imageMasterModel ReadImage(Int64 id)
        {
            var data = Connection.Single<imageMasterModel>("select * from tbl_imageMaster where img_id=@0", id);
            return data;
        }
        public List<ImageViewModel> GetHomeGalleryImages(Int64 id)
        {
            Sql query = Sql.Builder.Append("select tbl_homegallery.hg_imageid as ImageId,tbl_homegallery.hg_hmid as itmid,tbl_homegallery.hg_displayorder as displayOrder,img_savedname as ImagePath from tbl_homegallery");
            query.Append("inner join tbl_imageMaster on hg_imageid=img_id");
            query.Append("where hg_hmid=@0", id);
            var data = Connection.Fetch<ImageViewModel>(query);
            return data;
        }

        public List<ImageViewModel> GetBuilderGalleryImages(Int64 id)
        {
            Sql query = Sql.Builder.Append("select tbl_buildergallery.bg_imageid as ImageId,tbl_buildergallery.bg_buildid as itmid,tbl_buildergallery.bg_displayorder as displayOrder,img_savedname as ImagePath from tbl_buildergallery");
            query.Append("inner join tbl_imageMaster on bg_imageid=img_id");
            query.Append("where bg_buildid=@0", id);
            var data = Connection.Fetch<ImageViewModel>(query);
            return data;
        }
        public string ReadImageName(Int64 id)
        {
            string imgname = string.Empty;
            imgname = Connection.Single<string>("select img_savedname from tbl_imageMaster where img_id=@0", id);
            return imgname;
        }
    }
}