﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class BudgetRepo :BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveBudget(budgetModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_budgetMaster", "bm_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Budget";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<budgetModel> ListBudget()
        {
            List<budgetModel> locations = new List<budgetModel>();
            Sql query = Sql.Builder.Append("select * from tbl_budgetMaster order by bm_frmamnt asc");
            locations = Connection.Fetch<budgetModel>(query);
            return locations;
        }
        public Boolean DeleteBudget(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<budgetModel>("Where bm_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Budget";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}