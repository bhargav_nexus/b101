﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.ViewModels;
using B101_1.DataBaseRepo;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class PropertyRepo : BaseConnection
    {
        public string GetSetError { get; set; }

        #region masterdata

        public List<DataBindModel> GetBathroom()
        {
            List<DataBindModel> baths = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bam_id as datavalue,bam_bathroom as datatext from tbl_prpBathMaster");
            baths = data.ToList<DataBindModel>();
            return baths;
        }

        public List<DataBindModel> GetBedRoom()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select bem_id as datavalue,bem_bedroom as datatext from tbl_prpBedMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public List<DataBindModel> GetType()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select prptype_id as datavalue,prptype_name as datatext from tbl_prpTypeMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public List<DataBindModel> GetStorey()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select strm_id as datavalue,strm_storeys as datatext from tbl_prpStoreysMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        #endregion

        public Boolean SaveProperty(propertyModel prpmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_property", "prp_id", prpmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Property";
                blnRslt = false;
            }
            return blnRslt;
        }

        //public Boolean EditProperty(PropetyViewModel prpView)
        //{
        //    Boolean blnRslt = false;
        //    var prop = Connection.SingleOrDefault<propertyModel>("WHERE prp_id=@0", prpView.prp_id);
        //    prop.prp_address = prpView.prp_address;
        //    prop.prp_bath = prpView.bam_bathroom;
        //    prop.prp_bed = prpView.bem_bedroom;
        //    prop.prp_buildingsize = Convert.ToInt64(prpView.prp_buildingsize);
        //    prop.prp_description = prpView.prp_description;
        //    prop.prp_isact = prpView.Status;
        //    prop.prp_landsize = Convert.ToInt64(prpView.prp_landsize);
        //    prop.prp_latitude = prpView.prp_latitude;
        //    prop.prp_longitude = prpView.prp_longitude;
        //    prop.prp_name = prpView.prp_name;
        //    prop.prp_otherFeatures = prpView.prp_otherFeatures;
        //    prop.prp_price = Convert.ToDouble(prpView.prp_price);
        //    prop.prp_storey = prpView.prp_storey;
        //    prop.prp_typeId = prpView.prp_typeid;
        //    var updated = Connection.Update("tbl_property", "prp_id", prop);
        //    if (Convert.ToInt32(updated) > 0)
        //    {
        //        blnRslt = true;
        //    }
        //    else
        //    {
        //        GetSetError = "Error in Update Property";
        //        blnRslt = false;
        //    }
        //    return blnRslt;
        //}

        public Boolean UpdatePropertyImage(string flag, string imageid, Int64 productid)
        {
            Boolean isUpdt = false;
            Sql sql = Sql.Builder.Append("update tbl_property set " + flag + " = " + imageid + " where prp_id=" + productid);
            var res = Connection.Execute(sql);
            isUpdt = true;
            return isUpdt;
        }
        //public List<PropetyViewModel> ReadallProperties()
        //{
        //    List<PropetyViewModel> proplist = new List<PropetyViewModel>();
        //    Sql query = Sql.Builder.Append("select prp_id,prp_name,prptype_name,bem_bedroom,bam_bathroom,prp_price,prp_description,prp_landsize,prp_buildingsize,prp_otherFeatures,prp_address,prp_latitude,prp_longitude,prp_isact");
        //    query.Append("from tbl_property left join tbl_prpTypeMaster on prp_typeid=prptype_id");
        //    query.Append("left join tbl_prpBathMaster on prp_bath=bam_id");
        //    query.Append("left join tbl_prpBedMaster on prp_bed=bem_id");
        //    var data = Connection.Query<PropetyViewModel>(query);
        //    proplist = data.ToList<PropetyViewModel>();
        //    return proplist;
        //}

        public Boolean SaveProductImage(propimageModel prpimgmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_propimages", "pri_id", prpimgmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Image";
                blnRslt = false;
            }
            return blnRslt;
        }

        public List<ImageViewModel> ListPropertyImages(Int64 propid)
        {
            List<ImageViewModel> list = new List<ImageViewModel>();
            var data = Connection.Query<ImageViewModel>("select pri_id ,img_altText as AltText,img_savedname as ImagePath,pri_displayorder as displayOrder , pri_imageid As ImageId from tbl_propimages join tbl_imageMaster on pri_imageid=img_id where pri_propid=@0 order by pri_displayorder", propid);
            list = data.ToList<ImageViewModel>();
            return list;
        }

        public ImageViewModel GetPropertyThumb(int propid)
        {
            ImageViewModel img = new ImageViewModel();
            var data = Connection.Single<ImageViewModel>("select pri_id As ImageId ,img_altText as AltText,img_savedname as ImagePath,pri_displayorder as displayOrder,pri_imagetype as UseAs from tbl_propimages join tbl_imageMaster on pri_imageid=img_id where pri_imagetype='Thumbnail' and pri_propid=@0", propid);
            img = (ImageViewModel)data;
            return img;
        }

        public Boolean DeletePropertyImage(Int64 subid)
        {
            Boolean IsDel = false;
            int dltid = Connection.Delete<propimageModel>("Where pri_imageid=@0", subid);
            if (dltid > 0)
            {
                IsDel = true;
            }
            else
            {
                IsDel = false;
                GetSetError = "Error Occured In Image Delete";
            }
            return IsDel;
        }

        //public PropetyViewModel ReadProperties(Int64 id)
        //{
        //    PropetyViewModel prpmdl = new PropetyViewModel();
        //    Sql query = Sql.Builder.Append("select prp_id,prp_name,prptype_name,bem_bedroom,bam_bathroom,prp_price,prp_description,prp_landsize,prp_buildingsize,prp_otherFeatures,prp_address,prp_latitude,prp_longitude,prp_defaultimage,prp_smallmage,prp_mdmimage,prp_bigimage,prp_isact");
        //    query.Append("from tbl_property left join tbl_prpTypeMaster on prp_typeid=prptype_id");
        //    query.Append("left join tbl_prpBathMaster on prp_bath=bam_id");
        //    query.Append("left join tbl_prpBedMaster on prp_bed=bem_id");
        //    query.Append("where prp_id=@0", id);
        //    prpmdl = Connection.Single<PropetyViewModel>(query);
        //    return prpmdl;
        //}

        public Boolean Arrange(Int32 cdo, Int32 ndo, Int64 Imageid, Int64 pradid)
        {
            Boolean BlnRslt = false;
            Sql query = Sql.Builder.Append("update tbl_propimages set pri_displayorder=" + ndo + " where pri_imageid=" + Imageid + " and pri_propid=" + pradid);
            var rslt = Connection.Execute(query);
            query = Sql.Builder.Append("update tbl_propimages set pri_displayorder=" + cdo + " where pri_imageid !=" + Imageid + " and pri_displayorder=" + ndo + " and pri_propid=" + pradid);
            rslt = Connection.Execute(query);
            BlnRslt = true;
            return BlnRslt;
        }
        //public List<PropetyViewModel> SearchProperties(string Fq)
        //{
        //    List<PropetyViewModel> proplist = new List<PropetyViewModel>();
        //    Sql query = Sql.Builder.Append("select img_savedname,prp_id,prp_name,prptype_name,bem_bedroom,bam_bathroom,prp_price,prp_description,prp_landsize,prp_buildingsize,prp_otherFeatures,prp_address,prp_latitude,prp_longitude");
        //    query.Append("from tbl_property left join tbl_prpTypeMaster on prp_typeid=prptype_id");
        //    query.Append("left join tbl_prpBathMaster on prp_bath=bam_id");
        //    query.Append("left join tbl_prpBedMaster on prp_bed=bem_id");
        //    query.Append("left join tbl_propimages on prp_id=pri_propid");
        //    query.Append("join tbl_imageMaster on pri_imageid=img_id where pri_imagetype='Thumbnail'");
        //    if(Fq != string.Empty)
        //    {
        //        query.Append("and " + Fq);
        //    }
        //    var data = Connection.Query<PropetyViewModel>(query);
        //    proplist = data.ToList<PropetyViewModel>();
        //    return proplist;
        //}
    }
}