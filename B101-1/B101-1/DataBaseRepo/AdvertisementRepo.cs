﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using B101_1.Models;
using PetaPoco;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class AdvertisementRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveAd(advertisementModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_advertisementMaster", "adv_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(inserted);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Advertisement";
                blnRslt = false;
            }
            return blnRslt;
        }

        public List<DataBindModel> BindAdvertisement()
        {
            List<DataBindModel> items = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select adv_id as datavalue,adv_name as datatext from tbl_advertisementMaster");
            items = data.ToList<DataBindModel>();
            return items;
        }

        public Boolean EditAd(advertisementModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_advertisementMaster", "adv_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                MySession.Current.AddEditId = Convert.ToInt64(inserted);
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updating Advertisement";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<advertisementModel> ReadallAd()
        {
            List<advertisementModel> advlist = new List<advertisementModel>();
            Sql query = Sql.Builder.Append("select tbl_advertisementMaster.*,tbl_imageMaster.img_savedname as advimagenm from tbl_advertisementMaster");
            query.Append("inner join tbl_imageMaster on adv_image=img_id");
            advlist = Connection.Fetch<advertisementModel>(query);
            return advlist;
        }

        public List<advertisementModel> ReadDaytoDayAdv()
        {
            List<advertisementModel> advlist = new List<advertisementModel>();
            Sql query = Sql.Builder.Append("select tbl_advertisementMaster.*,tbl_imageMaster.img_savedname as advimagenm from tbl_advertisementMaster");
            query.Append("inner join tbl_imageMaster on adv_image=img_id");
            advlist = Connection.Fetch<advertisementModel>(query);
            return advlist;
        }

        public advertisementModel ReadaAdbyId(Int64 adid)
        {
            advertisementModel adv = new advertisementModel();
            Sql query = Sql.Builder.Append("select tbl_advertisementMaster.*,tbl_imageMaster.img_savedname as advimagenm from tbl_advertisementMaster");
            query.Append("inner join tbl_imageMaster on adv_image=img_id");
            query.Append("where adv_id=@0",adid);
            adv = Connection.SingleOrDefault<advertisementModel>(query);
            return adv;
        }
        public Boolean DeleteAd(Int64 id)
        {
            Int64 imgid = Connection.ExecuteScalar<Int64>("select adv_image from tbl_advertisementMaster where adv_id=@0",id);
            Boolean isdel = false;
            int dltid = Connection.Delete<advertisementModel>("Where adv_id=@0", id);
            if (dltid > 0)
            {
                GetSetError = imgid.ToString();
                isdel = true;
            }
            else
            {
                isdel = false;
                GetSetError = "Error Occured In Advertisemet Delete";
            }
            return isdel;
        }
    }
}