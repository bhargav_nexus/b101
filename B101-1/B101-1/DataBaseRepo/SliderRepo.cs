﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class SliderRepo : BaseConnection
    {
        public string GetSetError { get; set; }
       

        public Boolean SaveSlider(sliderModel mdlsld)
        {
            Boolean Issaved = false;
            var inserted = Connection.Insert("tbl_slider", "sld_id", mdlsld);
            if (Convert.ToInt32(inserted) > 0)
            {
                Issaved = true;
            }
            else
            {
                GetSetError = "Error in Insert Image";
                Issaved = false;
            }
            return Issaved;
        }

        public List<SliderViewModel> GetSliders()
        {
            List<SliderViewModel> Sliders = new List<SliderViewModel>();
            var data = Connection.Query<SliderViewModel>("select sld_id,img_altText,img_savedname,sld_dsplorder from tbl_slider join tbl_imageMaster on sld_imageid=img_id");
            Sliders = data.ToList<SliderViewModel>();
            return Sliders;
        }

        public Boolean DeleteSlider(int sliderId)
        {
            Boolean BlnRslt = false;
            int imageid = Connection.Single<Int32>("select sld_imageid from tbl_slider where sld_id=@0",sliderId);

            int dltid = Connection.Delete("tbl_slider", "sld_id", null, sliderId);
            if (dltid > 0)
            {
                GetSetError = imageid.ToString();
                BlnRslt = true;
            }
            else
            {
                GetSetError = "Error in Insert Image";
                BlnRslt = false;
            }
            return BlnRslt;
        }
    }
}