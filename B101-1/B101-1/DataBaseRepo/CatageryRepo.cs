﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class CatageryRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public List<DataBindModel> BindCatagery()
        {
            List<DataBindModel> catlist = new List<DataBindModel>();
            var data = Connection.Query<DataBindModel>("select prdct_catageryid as datavalue,prdct_fulcatagery as datatext from tbl_productcatagery");
            catlist = data.ToList<DataBindModel>();
            return catlist;
        }

        //public List<DataBindModel> BindCatageryByParent(String ParentCat)
        //{
        //    Int64 parentcatid = GetCatageryId(ParentCat);
        //    List<DataBindModel> catlist = new List<DataBindModel>();
        //    var data = Connection.Query<DataBindModel>("select prdct_catageryid as datavalue,prdct_catagery as datatext from tbl_productcatagery where prdct_parentid=@0", parentcatid);
        //    catlist = data.ToList<DataBindModel>();
        //    return catlist;
        //}

        public List<catageryModel> ReadCatagery()
        {
            List<catageryModel> catlist = new List<catageryModel>();
            var data = Connection.Query<catageryModel>("select * from tbl_productcatagery");
            catlist = data.ToList<catageryModel>();
            return catlist;
        }

        public Boolean SaveCatagery(catageryModel catmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_productcatagery", "prdct_catageryid", catmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Add Attribute";
                blnRslt = false;
            }
            return blnRslt;
        }

        public Int64 GetParetCatID(Int64 catId)
        {
            Int64 catid = Connection.Single<Int64>("select prdct_parentid from tbl_productcatagery where prdct_catageryid=@0", catId);
            return catid; 
        }
    }
}