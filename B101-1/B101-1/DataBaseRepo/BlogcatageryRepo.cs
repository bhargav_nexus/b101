﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;
using B101_1.ViewModels;

namespace B101_1.DataBaseRepo
{
    public class BlogcatageryRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public List<DataBindModel> Bindblgcatagery()
        {
            List<DataBindModel> catlist = new List<DataBindModel>();
            try
            {
                var data = Connection.Query<DataBindModel>("select bc_id as datavalue,bc_catagery as datatext from tbl_blogcatageryMaster");
                catlist = data.ToList<DataBindModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                catlist = null;
            }
            return catlist;
        }
        public Boolean SaveCatagery(blogcatageryModel catmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_blogcatageryMaster", "bc_id", catmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                GetSetError = inserted.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<blogcatageryModel> ReadCatagery()
        {
            List<blogcatageryModel> catlst = new List<blogcatageryModel>();
            try
            {
                var data = Connection.Query<blogcatageryModel>("select tbl_blogcatageryMaster.*,tbl_imageMaster.img_savedname as mainimagenm,tbl_blogcatageryMaster.bc_isstatic from tbl_blogcatageryMaster inner join tbl_imageMaster on bc_mainimage=img_id where bc_isstatic='false'");
                catlst = data.ToList<blogcatageryModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                catlst = null;
            }
            return catlst;
        }
        public Boolean DeleteByCatagery(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<blogcatageryModel>("Where bc_id = @0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Delete Catagery";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}