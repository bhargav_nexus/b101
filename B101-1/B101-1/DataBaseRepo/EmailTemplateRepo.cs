﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class EmailTemplateRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public emailtemplateModel gettemplatebyname(String templatename)
        {
            emailtemplateModel template = new emailtemplateModel();
            Sql query = Sql.Builder.Append("select * from tbl_emailtemplateMaster where et_name=@0", templatename);
            template = Connection.SingleOrDefault<emailtemplateModel>(query);
            return template;
        }

        public emailtemplateModel gettemplatebyid(Int64 id)
        {
            emailtemplateModel template = new emailtemplateModel();
            Sql query = Sql.Builder.Append("select * from tbl_emailtemplateMaster where et_id=@0", id);
            template = Connection.SingleOrDefault<emailtemplateModel>(query);
            return template;
        }
        public List<emailtemplateModel> TemplateList()
        {
            List<emailtemplateModel> templates = new List<emailtemplateModel>();
            Sql qery = Sql.Builder.Append("Select * from tbl_emailtemplateMaster");
            templates = Connection.Fetch<emailtemplateModel>(qery);
            return templates;
        }

        public Boolean EditTemplate(emailtemplateModel mdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_emailtemplateMaster", "et_id", mdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Updtae Template";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}