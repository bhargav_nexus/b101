﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using B101_1.Models;

namespace B101_1.DataBaseRepo
{
    public class BlogArticlesRepo : BaseConnection
    {
        public string GetSetError { get; set; }
        public Boolean SaveArticle(blogArticlesModel artmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Insert("tbl_blogarticales", "ba_id", artmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                GetSetError = inserted.ToString();
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Articles";
                blnRslt = false;
            }
            return blnRslt;
        }
        public Boolean UpdateArticle(blogArticlesModel artmdl)
        {
            Boolean blnRslt = false;
            var inserted = Connection.Update("tbl_blogarticales", "ba_id", artmdl);
            if (Convert.ToInt32(inserted) > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Adding Articles";
                blnRslt = false;
            }
            return blnRslt;
        }
        public List<blogArticlesModel> ReadAllArticles()
        {
            List<blogArticlesModel> attrlst = new List<blogArticlesModel>();
            try
            {
                var data = Connection.Query<blogArticlesModel>("select tbl_blogarticales.*,users.UserName from tbl_blogarticales inner join Users on ba_author=UserId");
                attrlst = data.ToList<blogArticlesModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                attrlst = null;
            }
            return attrlst;
        }
        public List<blogArticlesModel> ReadCurrentArticles()
        {
            List<blogArticlesModel> attrlst = new List<blogArticlesModel>();
            try
            {
                Sql sql = Sql.Builder.Append("select top 9  tbl_blogarticales.*,users.UserName,tbl_imageMaster.img_savedname as image from tbl_blogarticales");
                sql.Append("inner join Users on ba_author=UserId");
                sql.Append("inner join tbl_imageMaster on ba_mainimage=img_id");
                var data = Connection.Query<blogArticlesModel>(sql);
                attrlst = data.ToList<blogArticlesModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                attrlst = null;
            }
            return attrlst;
        }
        public List<blogArticlesModel> ReadArticlesByCatagory(String cat)
        {
            List<blogArticlesModel> attrlst = new List<blogArticlesModel>();
            try
            {
                Sql sql = Sql.Builder.Append("select tbl_blogarticales.*,users.UserName,tbl_imageMaster.img_savedname as image from tbl_blogarticales");
                sql.Append("inner join Users on ba_author=UserId");
                sql.Append("inner join tbl_imageMaster on ba_mainimage=img_id");
                sql.Append("inner join tbl_blogcatageryMaster on ba_catagery=tbl_blogcatageryMaster.bc_id");
                sql.Append("inner join tbl_blogsubcatageryMaster on tbl_blogarticales.ba_subcatagery=tbl_blogsubcatageryMaster.bsc_id");
                sql.Append("where tbl_blogcatageryMaster.bc_catagery=@0", cat);
                sql.Append("order by ba_id desc");
                var data = Connection.Query<blogArticlesModel>(sql);
                attrlst = data.ToList<blogArticlesModel>();
            }   
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                attrlst = null;
            }
            return attrlst;
        }
        public List<blogArticlesModel> ReadArticlesBySubCatagory(String subcat)
        {
            List<blogArticlesModel> attrlst = new List<blogArticlesModel>();
            try
            {
                Sql sql = Sql.Builder.Append("select tbl_blogarticales.*,users.UserName,tbl_imageMaster.img_savedname as image from tbl_blogarticales");
                sql.Append("inner join Users on ba_author=UserId");
                sql.Append("inner join tbl_imageMaster on ba_mainimage=img_id");
                sql.Append("inner join tbl_blogcatageryMaster on ba_catagery=tbl_blogcatageryMaster.bc_id");
                sql.Append("inner join tbl_blogsubcatageryMaster on tbl_blogarticales.ba_subcatagery=tbl_blogsubcatageryMaster.bsc_id");
                sql.Append("where tbl_blogsubcatageryMaster.bsc_subcatname=@0", subcat);
                sql.Append("order by ba_id desc");
                var data = Connection.Query<blogArticlesModel>(sql);
                attrlst = data.ToList<blogArticlesModel>();
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                attrlst = null;
            }
            return attrlst;
        }
        public blogArticlesModel ReadArticle(Int64 artid)
        {
            blogArticlesModel artcl = new blogArticlesModel();
            try
            {
                Sql query = Sql.Builder.Append("select tbl_blogarticales.*,img_savedname as image,users.UserName from tbl_blogarticales");
                query.Append("left join tbl_imageMaster on ba_mainimage=img_id");
                query.Append("left join Users on tbl_blogarticales.ba_author=Users.UserId");
                query.Append("where ba_id=@0", artid);
                artcl = Connection.SingleOrDefault<blogArticlesModel>(query);
            }
            catch (Exception ex)
            {
                GetSetError = ex.Message;
                artcl = null;
            }
            return artcl;
        }
        public Boolean DeleteArticle(Int64 id)
        {
            Boolean blnRslt = false;
            var Id = Connection.Delete<blogArticlesModel>("Where ba_id=@0", id);
            if (id > 0)
            {
                blnRslt = true;
            }
            else
            {
                GetSetError = "Error in Deleting Article";
                blnRslt = false;
            }
            return blnRslt;
        }
    }
}